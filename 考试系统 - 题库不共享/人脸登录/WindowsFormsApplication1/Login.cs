﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net.Http;
using System.IO;
using System.Net;
using AForge.Video.DirectShow;
using System.Drawing.Imaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using System.Data;



namespace WindowsFormsApplication1
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
            this.skinEngine1.SkinFile = Application.StartupPath + "//PageColor2.ssk";
        }
        FilterInfoCollection videoDevices;
        VideoCaptureDevice videoSource;
        public int selectedDeviceIndex = 0;
        public int selectedPICIndex = 0;

        //连接/开启摄像头
        private void Form1_Load(object sender, EventArgs e)
        {
            combDengLuShenFen.SelectedIndex = 0;
            comZcShenFen.SelectedIndex = 0;
            //隐藏注册界面
            panZhuCe.Visible = false;

            //打开登录界面摄像头
            videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            selectedDeviceIndex = 0;
            videoSource = new VideoCaptureDevice(videoDevices[selectedDeviceIndex].MonikerString);//连接摄像头。
            videoSource.VideoResolution = videoSource.VideoCapabilities[selectedDeviceIndex];
            videoSourcePlayer1.VideoSource = videoSource;
            videoSourcePlayer1.Start();
        }

        //v3接口

        //access_token属性的Json格式

        //获取包含access_token属性的Json格式的返回值
        public static class AccessToken
        {
            // 调用getAccessToken()获取的 access_token建议根据expires_in 时间 设置缓存
            // 返回token示例
            public static String TOKEN = "24.adda70c11b9786206253ddb70affdc46.2592000.1493524354.282335-1234567";

            // 百度云中开通对应服务应用的 API Key 建议开通应用的时候多选服务
            private static String clientId = "rkNnHqk367cRvOMrn4FEZteT";
            // 百度云中开通对应服务应用的 Secret Key
            private static String clientSecret = "mpTRmGXfO6ojr1NOB75VkD5Rgz86dZuO";

            public static String getAccessToken()
            {
                string authHost = "https://aip.baidubce.com/oauth/2.0/token";
                HttpClient client = new HttpClient();
                List<KeyValuePair<String, String>> paraList = new List<KeyValuePair<string, string>>();
                paraList.Add(new KeyValuePair<string, string>("grant_type", "client_credentials"));
                paraList.Add(new KeyValuePair<string, string>("client_id", clientId));
                paraList.Add(new KeyValuePair<string, string>("client_secret", clientSecret));
                HttpResponseMessage response = client.PostAsync(authHost, new FormUrlEncodedContent(paraList)).Result;
                string result = response.Content.ReadAsStringAsync().Result;
                Console.WriteLine(result);
                return result;
            }
        }

        private void ZhuCe_Click(object sender, EventArgs e)
        {

            //显示注册界面
            panZhuCe.Visible = true;
            //隐藏登录界面
            panDengLu.Visible = false;

            //关闭登录界面的摄像头
            videoSourcePlayer1.SignalToStop();
            videoSourcePlayer1.WaitForStop();

            //打开注册界面
            videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            selectedDeviceIndex = 0;
            videoSource = new VideoCaptureDevice(videoDevices[selectedDeviceIndex].MonikerString);//连接摄像头。
            videoSource.VideoResolution = videoSource.VideoCapabilities[selectedDeviceIndex];
            videoSourcePlayer2.VideoSource = videoSource;
            videoSourcePlayer2.Start();

            //判断是否检测到脸部信息
            //ShiFouYouLian(FaceDetect.detect(pictrue));
        }

        //保存照片到本地
        private void DengLu_Click(object sender, EventArgs e)
        {
            Bitmap bitmap = videoSourcePlayer1.GetCurrentVideoFrame();

            //添加照片
            string pictrue = AppDomain.CurrentDomain.BaseDirectory + "\\登录\\" + DateTime.Now.ToString("yyyyMMddHHmmssff") + ".jpg";
            try
            {
                bitmap.Save(pictrue, ImageFormat.Jpeg);
            }
            catch
            {
                return;
            }
            bitmap.Dispose();

            //将路径传给人脸检测函数，同时将返回值给textBox2：靓丽度：beauty
            //textBox2.Text = FaceDetect.detect(pictrue);

            string ChaSql = string.Format("select * from RenLianUser where UserLeiXin='{0}'", combDengLuShenFen.Text);
            DataTable Ta = DBHepler.Cha(ChaSql);
            string LuJing = null;
            if (Ta.Rows.Count > 0)
            {
                LuJing = Ta.Rows[0]["RenLianLuJing"].ToString();
            }
            else
            {
                MessageBox.Show("请先注册");
                return;
            }

            var jsonstring = AccessToken.getAccessToken();

            for (int i = 0; i < Ta.Rows.Count; i++)
            {

                var jObject = JObject.Parse(jsonstring);
                string token = jObject["access_token"].ToString();
                string host = "https://aip.baidubce.com/rest/2.0/face/v3/match?access_token=" + token;
                Encoding encoding = Encoding.Default;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(host);
                request.Method = "post";
                request.KeepAlive = true;
                string image_IdCard = null;
                string image_Live = null;

                //查询存在数据库中的人脸信息
                //ChaSql = "select * from RenLianUser";
                //Ta = DBHepler.Cha(ChaSql);
                LuJing = Ta.Rows[i]["RenLianLuJing"].ToString();
                try
                {
                    //用注册在数据库中的人脸信息与当前登录的人脸信息对比
                    image_IdCard = Convert.ToBase64String(System.IO.File.ReadAllBytes(LuJing));
                    image_Live = Convert.ToBase64String(System.IO.File.ReadAllBytes(pictrue));
                }
                catch
                {
                    //如果数据库中没有存在当前登录的人脸信息那么就是没有注册
                    if (i == Ta.Rows.Count - 1)
                    {
                        MessageBox.Show("未注册人脸信息");
                        return;
                    }
                    continue;//否则退回去重新查询
                }


                string str = "[{\"image\":\"" + image_IdCard + "\",\"image_type\":\"BASE64\",\"face_type\":\"IDCARD\",\"quality_control\":\"LOW\",\"liveness_control\":\"HIGH\"},{\"image\":\"" + image_Live + "\",\"image_type\":\"BASE64\",\"face_type\":\"LIVE\",\"quality_control\":\"LOW\",\"liveness_control\":\"HIGH\"}]";
                byte[] buffer = encoding.GetBytes(str);
                request.ContentLength = buffer.Length;
                request.GetRequestStream().Write(buffer, 0, buffer.Length);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.Default);
                string result = reader.ReadToEnd();
                //Console.WriteLine("人脸对比:");
                //Console.WriteLine(result);
                //MessageBox.Show(result);
                JObject returnJson = (JObject)JsonConvert.DeserializeObject(result);
                JObject resultJson = null;
                try
                {
                    //如果resultJson为空那么就没有查询到人脸信息
                    resultJson = (JObject)JsonConvert.DeserializeObject(returnJson["result"].ToString());
                    if (resultJson==null) {
                        MessageBox.Show(returnJson["error_msg"].ToString()+"未检测到人脸信息！");
                        return;
                    }
                   
                }
                catch (Exception)
                {
                    MessageBox.Show("未能检测到人脸信息,请到光亮的地方进行检测");
                    return;
                }

                try
                {
                    if (Convert.ToDecimal(resultJson["score"].ToString()) > 90)
                    {
                        MessageBox.Show("登录成功!");

                        string ChaIDName = string.Format(@"select * from RenLianUser where RenLianLuJing='{0}' and UserLeiXin='{1}'", LuJing, combDengLuShenFen.Text);
                        DataTable IDNameTa = DBHepler.Cha(ChaIDName);
                        KaoShiXiTong.UserIDandUserName.UserID = (int)IDNameTa.Rows[0]["ID"];//用户ID
                        KaoShiXiTong.UserIDandUserName.UserName = IDNameTa.Rows[0]["Name"].ToString();//用户姓名
                        KaoShiXiTong.UserIDandUserName.strUserLeiXin = combDengLuShenFen.Text;
                        try
                        {
                            KaoShiXiTong.ZhuYeMian Mainf = new KaoShiXiTong.ZhuYeMian();
                            Mainf.StartPosition = FormStartPosition.CenterScreen;
                            Mainf.Show();
                            videoSourcePlayer1.SignalToStop();
                            videoSourcePlayer1.WaitForStop();
                            this.Visible = false;
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("异常：" + ex);
                        }
                        break;
                    }
                    else
                    {
                        if (i == Ta.Rows.Count - 1)
                        {
                            //result = "不是同一人，请视情况报警！";
                            MessageBox.Show("登录失败!");
                            return;
                        }
                    }
                }
                catch
                {
                    MessageBox.Show("可能当前光线不好，请把脸对准摄像头!");
                    break;
                }
            }
        }

        private void Login_FormClosed(object sender, FormClosedEventArgs e)
        {

            Application.Exit();
            Environment.Exit(0);
            //Application.Exit();//结束所有线程

        }

        private void ButQuXiao_Click(object sender, EventArgs e)
        {
            //隐藏注册界面
            panZhuCe.Visible = false;
            //关闭注册界面摄像头
            videoSourcePlayer2.SignalToStop();
            videoSourcePlayer2.WaitForStop();

            //显示登录界面
            panDengLu.Visible = true;
            //打开登录界面摄像头
            videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            selectedDeviceIndex = 0;
            videoSource = new VideoCaptureDevice(videoDevices[selectedDeviceIndex].MonikerString);//连接摄像头。
            videoSource.VideoResolution = videoSource.VideoCapabilities[selectedDeviceIndex];
            videoSourcePlayer1.VideoSource = videoSource;
            videoSourcePlayer1.Start();
        }

        /// <summary>
        /// 验证手机号码
        /// </summary>
        /// <param name="StringGeShi"></param>
        /// <returns></returns>
        public static bool GeShi(string StringGeShi)
        {
            try
            {
                double SFZ = Convert.ToDouble(StringGeShi);
            }
            catch
            {
                return false;
            }
            if (StringGeShi.Length == 11 && StringGeShi.Substring(0, 1) == "1")
            {
                return true;
            }
            return false;
        }

        private void ButZhuCe_Click(object sender, EventArgs e)
        {
            string zhuCeLeiXin = comZcShenFen.Text;

            string XueShengName = txtName.Text;
            string Phone = txtPhone.Text;

            string UserCountSql = string.Format(@"select * from RenLianUser where Name='{0}' and Phone='{1}' and UserLeiXin='{2}'",
                XueShengName, Phone, comZcShenFen.Text);
            DataTable UserCountTa = DBHepler.Cha(UserCountSql);

            //拍照
            Bitmap bitmap = videoSourcePlayer2.GetCurrentVideoFrame();
            //添加照片
            string pictrue = AppDomain.CurrentDomain.BaseDirectory + "\\注册\\" + DateTime.Now.ToString("yyyyMMddHHmmssff") + ".jpg";
            bitmap.Save(pictrue, ImageFormat.Jpeg);
            bitmap.Dispose();

            if (XueShengName == "")
            {
                MessageBox.Show("请输入姓名");
                txtName.Focus();
                return;
            }
            else if (Phone == "")
            {
                MessageBox.Show("请输入号码");
                txtPhone.Focus();
                return;
            }
            else if (GeShi(Phone) == false)
            {
                MessageBox.Show("手机号码格式有误!");
                txtPhone.Focus();
                return;
            }
            //else if (txtYanZhengMa.Text == "")
            //{
            //    MessageBox.Show("请输入短信验证码!");
            //    txtYanZhengMa.Focus();
            //    return;
            //}
            //else if (txtYanZhengMa.Text != Sum)
            //{
            //    MessageBox.Show("验证码有误!");
            //    txtYanZhengMa.Focus();
            //    return;
            //}
            //else if (YZMJiShi == 0)
            //{
            //    MessageBox.Show("验证码超时!");
            //    txtYanZhengMa.Focus();
            //    YZMDaoJiShi.Stop();
            //    return;
            //}
            else
            {
                if (match(pictrue, pictrue) != 3)//判断是否获取到脸部信息
                {
                    //判断是否注册过
                    if (/*match(UserCountTa.Rows[0]["RenLianLuJing"].ToString(), pictrue) == 2 && */UserCountTa.Rows.Count == 0)
                    {
                        if (match(pictrue, pictrue) == 1)
                        {
                            string SqlInsert = string.Format(@"insert into RenLianUser Values ('{0}','{1}','{2}','{3}')", XueShengName, Phone, pictrue, zhuCeLeiXin);
                            if (DBHepler.ZSG(SqlInsert))
                            {
                                MessageBox.Show("注册成功");
                            }
                            else
                            {
                                MessageBox.Show("注册失败!");
                            }
                        }
                        else
                        {
                            MessageBox.Show("可能当前光线不好，请把脸对准摄像头!!！");
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("不需要重复注册!");
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("可能当前光线不好，请把脸对准摄像头!");
                    return;
                }
            }
        }

        /// <summary>
        /// 人脸注册 与人脸对比，1 代表同一张脸 ，2 代表不是同一张脸
        /// </summary>
        /// <returns></returns>
        private int match(string basePath1, string basePath2)
        {
            var jsonstring = AccessToken.getAccessToken();
            var jObject = JObject.Parse(jsonstring);
            string token = jObject["access_token"].ToString();
            string host = "https://aip.baidubce.com/rest/2.0/face/v3/match?access_token=" + token;
            Encoding encoding = Encoding.Default;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(host);
            request.Method = "post";
            request.KeepAlive = true;
            string image_IdCard = Convert.ToBase64String(System.IO.File.ReadAllBytes(basePath1));
            string image_Live = Convert.ToBase64String(System.IO.File.ReadAllBytes(basePath2));

            string str = "[{\"image\":\"" + image_IdCard + "\",\"image_type\":\"BASE64\",\"face_type\":\"IDCARD\",\"quality_control\":\"LOW\",\"liveness_control\":\"HIGH\"},{\"image\":\"" + image_Live + "\",\"image_type\":\"BASE64\",\"face_type\":\"LIVE\",\"quality_control\":\"LOW\",\"liveness_control\":\"HIGH\"}]";
            byte[] buffer = encoding.GetBytes(str);
            request.ContentLength = buffer.Length;
            request.GetRequestStream().Write(buffer, 0, buffer.Length);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.Default);
            string result = reader.ReadToEnd();

            //Console.WriteLine("人脸对比:");
            //Console.WriteLine(result);
            //MessageBox.Show(result);

            JObject returnJson = (JObject)JsonConvert.DeserializeObject(result);
            if ("0".Equals(returnJson["error_code"].ToString()) && "SUCCESS".Equals(returnJson["error_msg"].ToString()))
            {
                JObject resultJson = (JObject)JsonConvert.DeserializeObject(returnJson["result"].ToString());
                if (Convert.ToDecimal(resultJson["score"].ToString()) > 90)
                {
                    return 1;
                }
                else
                {
                    return 2;
                }
            }
            else
            {
                return 3;
            }
        }

        string Sum;
        private void ButYanZM_Click(object sender, EventArgs e)
        {
            if (YZMJiShi == 40)
            {
                Random R = new Random();
                Sum = null;
                for (int i = 0; i < 6; i++)
                {
                    int SuiJiShu = R.Next(0, 10);
                    Sum += SuiJiShu;
                }
                string number = txtPhone.Text;//手机号码
                string smsText = Sum;//需要发送的验证码
                try
                {
                    string PostUrl = YanZhengMa.GetPostUrl(number, smsText);
                    string result = YanZhengMa.PostSmsInfo(PostUrl);
                    string t = YanZhengMa.GetResult(result);
                    MessageBox.Show("发送成功");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

                YZMDaoJiShi.Enabled = true;
                YZMDaoJiShi.Start();
                a = 0;
            }
            else
            {
                MessageBox.Show("请在计时完成后发送!");
            }
        }

        int YZMJiShi = 40;
        int a = 0;
        /// <summary>
        /// 验证码倒计时
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void YZMDaoJiShi_Tick(object sender, EventArgs e)
        {
            if (YZMJiShi != 0 && a == 0)
            {
                YZMJiShi--;
                labYZMDaoJiShi.Text = "(" + YZMJiShi + ")";
            }
            if (YZMJiShi == 0)//等于0之后重新给时间赋值
            {
                YZMJiShi = 40;
                labYZMDaoJiShi.Text = "(0)";
                a = 1;
            }
        }
    }
}
