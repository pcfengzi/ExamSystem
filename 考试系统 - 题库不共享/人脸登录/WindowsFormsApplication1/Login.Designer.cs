﻿namespace WindowsFormsApplication1
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.button2 = new System.Windows.Forms.Button();
            this.ZhuCe = new System.Windows.Forms.Button();
            this.videoSourcePlayer1 = new AForge.Controls.VideoSourcePlayer();
            this.panDengLu = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.combDengLuShenFen = new System.Windows.Forms.ComboBox();
            this.DengLu = new System.Windows.Forms.Button();
            this.panZhuCe = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.comZcShenFen = new System.Windows.Forms.ComboBox();
            this.butYanZM = new System.Windows.Forms.Button();
            this.txtYanZhengMa = new System.Windows.Forms.TextBox();
            this.labYZMDaoJiShi = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.butQuXiao = new System.Windows.Forms.Button();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.butZhuCe = new System.Windows.Forms.Button();
            this.videoSourcePlayer2 = new AForge.Controls.VideoSourcePlayer();
            this.YZMDaoJiShi = new System.Windows.Forms.Timer(this.components);
            this.skinEngine1 = new Sunisoft.IrisSkin.SkinEngine(((System.ComponentModel.Component)(this)));
            this.panDengLu.SuspendLayout();
            this.panZhuCe.SuspendLayout();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(-139, 538);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 54;
            this.button2.Text = "登录";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // ZhuCe
            // 
            this.ZhuCe.Location = new System.Drawing.Point(386, 530);
            this.ZhuCe.Name = "ZhuCe";
            this.ZhuCe.Size = new System.Drawing.Size(75, 23);
            this.ZhuCe.TabIndex = 55;
            this.ZhuCe.Text = "注册";
            this.ZhuCe.UseVisualStyleBackColor = true;
            this.ZhuCe.Click += new System.EventHandler(this.ZhuCe_Click);
            // 
            // videoSourcePlayer1
            // 
            this.videoSourcePlayer1.Location = new System.Drawing.Point(3, 9);
            this.videoSourcePlayer1.Name = "videoSourcePlayer1";
            this.videoSourcePlayer1.Size = new System.Drawing.Size(491, 500);
            this.videoSourcePlayer1.TabIndex = 50;
            this.videoSourcePlayer1.Text = "videoSourcePlayer1";
            this.videoSourcePlayer1.VideoSource = null;
            // 
            // panDengLu
            // 
            this.panDengLu.BackColor = System.Drawing.Color.Transparent;
            this.panDengLu.Controls.Add(this.label5);
            this.panDengLu.Controls.Add(this.combDengLuShenFen);
            this.panDengLu.Controls.Add(this.DengLu);
            this.panDengLu.Controls.Add(this.ZhuCe);
            this.panDengLu.Controls.Add(this.button2);
            this.panDengLu.Controls.Add(this.videoSourcePlayer1);
            this.panDengLu.Location = new System.Drawing.Point(12, 12);
            this.panDengLu.Name = "panDengLu";
            this.panDengLu.Size = new System.Drawing.Size(505, 603);
            this.panDengLu.TabIndex = 56;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(178, 536);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 58;
            this.label5.Text = "登录身份";
            // 
            // combDengLuShenFen
            // 
            this.combDengLuShenFen.FormattingEnabled = true;
            this.combDengLuShenFen.Items.AddRange(new object[] {
            "管理员",
            "学生"});
            this.combDengLuShenFen.Location = new System.Drawing.Point(237, 534);
            this.combDengLuShenFen.Name = "combDengLuShenFen";
            this.combDengLuShenFen.Size = new System.Drawing.Size(85, 20);
            this.combDengLuShenFen.TabIndex = 57;
            // 
            // DengLu
            // 
            this.DengLu.Location = new System.Drawing.Point(48, 530);
            this.DengLu.Name = "DengLu";
            this.DengLu.Size = new System.Drawing.Size(75, 23);
            this.DengLu.TabIndex = 56;
            this.DengLu.Text = "登录";
            this.DengLu.UseVisualStyleBackColor = true;
            this.DengLu.Click += new System.EventHandler(this.DengLu_Click);
            // 
            // panZhuCe
            // 
            this.panZhuCe.BackColor = System.Drawing.Color.Transparent;
            this.panZhuCe.Controls.Add(this.label4);
            this.panZhuCe.Controls.Add(this.comZcShenFen);
            this.panZhuCe.Controls.Add(this.butYanZM);
            this.panZhuCe.Controls.Add(this.txtYanZhengMa);
            this.panZhuCe.Controls.Add(this.labYZMDaoJiShi);
            this.panZhuCe.Controls.Add(this.label3);
            this.panZhuCe.Controls.Add(this.butQuXiao);
            this.panZhuCe.Controls.Add(this.txtPhone);
            this.panZhuCe.Controls.Add(this.label2);
            this.panZhuCe.Controls.Add(this.txtName);
            this.panZhuCe.Controls.Add(this.label1);
            this.panZhuCe.Controls.Add(this.butZhuCe);
            this.panZhuCe.Controls.Add(this.videoSourcePlayer2);
            this.panZhuCe.Location = new System.Drawing.Point(9, 12);
            this.panZhuCe.Name = "panZhuCe";
            this.panZhuCe.Size = new System.Drawing.Size(508, 603);
            this.panZhuCe.TabIndex = 57;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(291, 491);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 67;
            this.label4.Text = "注册身份";
            // 
            // comZcShenFen
            // 
            this.comZcShenFen.FormattingEnabled = true;
            this.comZcShenFen.Items.AddRange(new object[] {
            "管理员",
            "学生"});
            this.comZcShenFen.Location = new System.Drawing.Point(350, 488);
            this.comZcShenFen.Name = "comZcShenFen";
            this.comZcShenFen.Size = new System.Drawing.Size(97, 20);
            this.comZcShenFen.TabIndex = 66;
            // 
            // butYanZM
            // 
            this.butYanZM.Location = new System.Drawing.Point(230, 570);
            this.butYanZM.Name = "butYanZM";
            this.butYanZM.Size = new System.Drawing.Size(75, 23);
            this.butYanZM.TabIndex = 65;
            this.butYanZM.Text = "获取验证码";
            this.butYanZM.UseVisualStyleBackColor = true;
            this.butYanZM.Click += new System.EventHandler(this.ButYanZM_Click);
            // 
            // txtYanZhengMa
            // 
            this.txtYanZhengMa.Location = new System.Drawing.Point(350, 532);
            this.txtYanZhengMa.Name = "txtYanZhengMa";
            this.txtYanZhengMa.Size = new System.Drawing.Size(97, 21);
            this.txtYanZhengMa.TabIndex = 64;
            // 
            // labYZMDaoJiShi
            // 
            this.labYZMDaoJiShi.AutoSize = true;
            this.labYZMDaoJiShi.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labYZMDaoJiShi.ForeColor = System.Drawing.Color.Red;
            this.labYZMDaoJiShi.Location = new System.Drawing.Point(452, 530);
            this.labYZMDaoJiShi.Name = "labYZMDaoJiShi";
            this.labYZMDaoJiShi.Size = new System.Drawing.Size(29, 21);
            this.labYZMDaoJiShi.TabIndex = 63;
            this.labYZMDaoJiShi.Text = "(0)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(279, 537);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 63;
            this.label3.Text = "手机验证码";
            // 
            // butQuXiao
            // 
            this.butQuXiao.Location = new System.Drawing.Point(372, 570);
            this.butQuXiao.Name = "butQuXiao";
            this.butQuXiao.Size = new System.Drawing.Size(75, 23);
            this.butQuXiao.TabIndex = 62;
            this.butQuXiao.Text = "返回";
            this.butQuXiao.UseVisualStyleBackColor = true;
            this.butQuXiao.Click += new System.EventHandler(this.ButQuXiao_Click);
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(97, 524);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(100, 21);
            this.txtPhone.TabIndex = 60;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 532);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 58;
            this.label2.Text = "手机号码";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(97, 488);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(100, 21);
            this.txtName.TabIndex = 61;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(55, 491);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 59;
            this.label1.Text = "姓名";
            // 
            // butZhuCe
            // 
            this.butZhuCe.Location = new System.Drawing.Point(97, 570);
            this.butZhuCe.Name = "butZhuCe";
            this.butZhuCe.Size = new System.Drawing.Size(75, 23);
            this.butZhuCe.TabIndex = 57;
            this.butZhuCe.Text = "注册";
            this.butZhuCe.UseVisualStyleBackColor = true;
            this.butZhuCe.Click += new System.EventHandler(this.ButZhuCe_Click);
            // 
            // videoSourcePlayer2
            // 
            this.videoSourcePlayer2.Location = new System.Drawing.Point(7, 6);
            this.videoSourcePlayer2.Name = "videoSourcePlayer2";
            this.videoSourcePlayer2.Size = new System.Drawing.Size(490, 467);
            this.videoSourcePlayer2.TabIndex = 56;
            this.videoSourcePlayer2.Text = "videoSourcePlayer2";
            this.videoSourcePlayer2.VideoSource = null;
            // 
            // YZMDaoJiShi
            // 
            this.YZMDaoJiShi.Interval = 1000;
            this.YZMDaoJiShi.Tick += new System.EventHandler(this.YZMDaoJiShi_Tick);
            // 
            // skinEngine1
            // 
            this.skinEngine1.SerialNumber = "";
            this.skinEngine1.SkinFile = null;
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(523, 620);
            this.Controls.Add(this.panZhuCe);
            this.Controls.Add(this.panDengLu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "登录/注册";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Login_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panDengLu.ResumeLayout(false);
            this.panDengLu.PerformLayout();
            this.panZhuCe.ResumeLayout(false);
            this.panZhuCe.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button ZhuCe;
        private AForge.Controls.VideoSourcePlayer videoSourcePlayer1;
        private System.Windows.Forms.Panel panDengLu;
        private System.Windows.Forms.Button DengLu;
        private System.Windows.Forms.Panel panZhuCe;
        private System.Windows.Forms.Button butQuXiao;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button butZhuCe;
        private AForge.Controls.VideoSourcePlayer videoSourcePlayer2;
        private System.Windows.Forms.Button butYanZM;
        private System.Windows.Forms.TextBox txtYanZhengMa;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labYZMDaoJiShi;
        private System.Windows.Forms.Timer YZMDaoJiShi;
        private Sunisoft.IrisSkin.SkinEngine skinEngine1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comZcShenFen;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox combDengLuShenFen;
    }
}