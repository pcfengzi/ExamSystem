﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CCWin;

namespace KaoShiXiTong
{
    public partial class ZhuYeMian : Form
    {
        public ZhuYeMian()
        {
            InitializeComponent();
            //this.skinEngine1 = new Sunisoft.IrisSkin.SkinEngine(((System.ComponentModel.Component)(this)));
            this.skinEngine1.SkinFile = Application.StartupPath + "//PageColor2.ssk";
        }

        [DllImport("user32")]

        public static extern int SetParent(int children, int parent);
        private void ZhuYeMian_Load(object sender, EventArgs e)
        {
            if (UserIDandUserName.strUserLeiXin == "管理员")
            {
                labUser.Text = "管理员姓名：";
                ZhuCaiDan.Visible = true;
                ZiCaiDan.Visible = false;
            }
            if (UserIDandUserName.strUserLeiXin == "学生")
            {
                labUser.Text = "学生姓名：";
                ZhuCaiDan.Visible = false;
                ZiCaiDan.Visible = true;
            }

            //comboBox1.DataSource = new DirectoryInfo("Skins").GetFiles();
            //comboBox1.DisplayMember = "Name";

            labUserName.Text = UserIDandUserName.UserName;
            //设置透明度
            labUserName.Parent = pictureBox1;
            labTuiChuXT.Parent = pictureBox1;
            labUser.Parent = pictureBox1;
            pictureUserTouXiang.Parent = pictureBox1;
        }

        TianJiaTiMu TianJiaTiMu = new TianJiaTiMu();
        ChaKanTiMu ChaKanTiMu = new ChaKanTiMu();
        ShengChengShiJuan SCShiJuan = new ShengChengShiJuan();
        KaiShiKaoShi KaoShi = new KaiShiKaoShi();
        ChaXunCJ CJ = new ChaXunCJ();
        ChaKanDaAn DaAn = new ChaKanDaAn();
        TianJiaKeCheng TJCK = new TianJiaKeCheng();
        ChaKanKeCheng KeCheng = new ChaKanKeCheng();
        随机点名.SJ2 Sj = new 随机点名.SJ2();

        private void ZhuCaiDan_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            string Name = e.Node.ToString();
            if (Name == "TreeNode: 添加题目" && UserIDandUserName.strUserLeiXin == "管理员")
            {
                if (TianJiaTiMu.IsDisposed)//如果当前窗体被关闭后，重新点击按钮重新把窗体show出来
                {
                    TianJiaTiMu = new TianJiaTiMu();
                    TianJiaTiMu.Show();
                    TianJiaTiMu.Location = new Point(330, 90);
                }

                TianJiaTiMu.Show();
                TianJiaTiMu.Location = new Point(330, 90);
                SetParent((int)TianJiaTiMu.Handle, (int)this.Handle);//把当前show出来的窗体放到父窗体前面
                                                                     //TianJiaTiMu.Location = new Point(250, 75);
                Sj.Visible = false;
                ChaKanTiMu.Visible = false;
                SCShiJuan.Visible = false;
                KaoShi.Visible = false;
                CJ.Visible = false;
                DaAn.Visible = false;
                TJCK.Visible = false;
                KeCheng.Visible = false;

            }
            else if (Name == "TreeNode: 查看题目" && UserIDandUserName.strUserLeiXin == "管理员")
            {
                ChaKanTiMu.Show();
                ChaKanTiMu.Location = new Point(300, 85);
                SetParent((int)ChaKanTiMu.Handle, (int)this.Handle);//把当前show出来的窗体放到父窗体前面

                TianJiaTiMu.Visible = false;
                Sj.Visible = false;
                SCShiJuan.Visible = false;
                KaoShi.Visible = false;
                CJ.Visible = false;
                DaAn.Visible = false;
                TJCK.Visible = false;
                KeCheng.Visible = false;

            }
            else if (Name == "TreeNode: 试卷生成" && UserIDandUserName.strUserLeiXin == "管理员")
            {
                SCShiJuan.Show();
                SCShiJuan.Location = new Point(330, 120);
                SetParent((int)SCShiJuan.Handle, (int)this.Handle);//把当前show出来的窗体放到父窗体前面

                TianJiaTiMu.Visible = false;
                ChaKanTiMu.Visible = false;
                Sj.Visible = false;
                KaoShi.Visible = false;
                CJ.Visible = false;
                DaAn.Visible = false;
                TJCK.Visible = false;
                KeCheng.Visible = false;

            }
            else if (Name == "TreeNode: 添加课程" && UserIDandUserName.strUserLeiXin == "管理员")
            {
                TJCK.Show();
                TJCK.Location = new Point(280, 85);
                SetParent((int)TJCK.Handle, (int)this.Handle);

                TianJiaTiMu.Visible = false;
                ChaKanTiMu.Visible = false;
                SCShiJuan.Visible = false;
                KaoShi.Visible = false;
                CJ.Visible = false;
                DaAn.Visible = false;
                KeCheng.Visible = false;
                Sj.Visible = false;
            }
            else if (Name == "TreeNode: 查看课程" && UserIDandUserName.strUserLeiXin == "管理员")
            {
                KeCheng.Show();
                KeCheng.Location = new Point(400, 150);
                SetParent((int)KeCheng.Handle, (int)this.Handle);

                TianJiaTiMu.Visible = false;
                ChaKanTiMu.Visible = false;
                SCShiJuan.Visible = false;
                KaoShi.Visible = false;
                CJ.Visible = false;
                DaAn.Visible = false;
                TJCK.Visible = false;
                Sj.Visible = false;

            }
            else if (Name == "TreeNode: 随机点名" && UserIDandUserName.strUserLeiXin == "管理员")
            {
                Sj.Show();
                Sj.Location = new Point(320, 120);
                SetParent((int)Sj.Handle, (int)this.Handle);

                TianJiaTiMu.Visible = false;
                ChaKanTiMu.Visible = false;
                SCShiJuan.Visible = false;
                KaoShi.Visible = false;
                CJ.Visible = false;
                DaAn.Visible = false;
                TJCK.Visible = false;
                KeCheng.Visible = false;
            }
        }

        private void LabTuiChuXT_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("确定选择退出？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void ZhuYeMian_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
            Environment.Exit(0);
        }

        private void ZiCaiDan_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            string ziName = e.Node.ToString();
            if (ziName == "TreeNode: 在线考试" && UserIDandUserName.strUserLeiXin == "学生")
            {
                if (KaoShi.IsDisposed)//如果当前窗体被关闭后，重新点击按钮重新把窗体show出来
                {
                    KaoShi = new KaiShiKaoShi();
                    KaoShi.Show();
                    KaoShi.Location = new Point(150, 85);
                }

                KaoShi.Show();
                KaoShi.Location = new Point(150, 85);
                SetParent((int)KaoShi.Handle, (int)this.Handle);//把当前show出来的窗体放到父窗体前面

                TianJiaTiMu.Visible = false;
                ChaKanTiMu.Visible = false;
                SCShiJuan.Visible = false;
                Sj.Visible = false;
                CJ.Visible = false;
                DaAn.Visible = false;
                TJCK.Visible = false;
                KeCheng.Visible = false;
            }
            else if (ziName == "TreeNode: 查询成绩" && UserIDandUserName.strUserLeiXin == "学生")
            {
                CJ.Show();
                CJ.Location = new Point(350, 120);
                SetParent((int)CJ.Handle, (int)this.Handle);

                TianJiaTiMu.Visible = false;
                ChaKanTiMu.Visible = false;
                SCShiJuan.Visible = false;
                KaoShi.Visible = false;
                Sj.Visible = false;
                DaAn.Visible = false;
                TJCK.Visible = false;
                KeCheng.Visible = false;
            }
            else if (ziName == "TreeNode: 错题分析" && UserIDandUserName.strUserLeiXin == "学生")
            {
                DaAn.Show();
                DaAn.Location = new Point(310, 120);
                SetParent((int)DaAn.Handle, (int)this.Handle);

                TianJiaTiMu.Visible = false;
                ChaKanTiMu.Visible = false;
                SCShiJuan.Visible = false;
                KaoShi.Visible = false;
                CJ.Visible = false;
                Sj.Visible = false;
                TJCK.Visible = false;
                KeCheng.Visible = false;
            }
        }
    }
}
