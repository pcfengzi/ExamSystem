﻿namespace KaoShiXiTong
{
    partial class ZhuYeMian
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("添加课程");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("查看课程");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("课程管理", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2});
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("添加题目");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("查看题目");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("题库管理", 1, 0, new System.Windows.Forms.TreeNode[] {
            treeNode4,
            treeNode5});
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("试卷生成");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("试卷管理", 1, 0, new System.Windows.Forms.TreeNode[] {
            treeNode7});
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("随机点名");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("小工具", new System.Windows.Forms.TreeNode[] {
            treeNode9});
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ZhuYeMian));
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("在线考试");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("考试管理", new System.Windows.Forms.TreeNode[] {
            treeNode11});
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("查询成绩");
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("错题分析");
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("成绩管理", 1, 0, new System.Windows.Forms.TreeNode[] {
            treeNode13,
            treeNode14});
            this.ZhuCaiDan = new System.Windows.Forms.TreeView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureUserTouXiang = new System.Windows.Forms.PictureBox();
            this.labUser = new System.Windows.Forms.Label();
            this.labUserName = new System.Windows.Forms.Label();
            this.labTuiChuXT = new System.Windows.Forms.Label();
            this.skinEngine1 = new Sunisoft.IrisSkin.SkinEngine(((System.ComponentModel.Component)(this)));
            this.ZiCaiDan = new System.Windows.Forms.TreeView();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureUserTouXiang)).BeginInit();
            this.SuspendLayout();
            // 
            // ZhuCaiDan
            // 
            this.ZhuCaiDan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(236)))), ((int)(((byte)(236)))));
            this.ZhuCaiDan.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ZhuCaiDan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ZhuCaiDan.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ZhuCaiDan.ForeColor = System.Drawing.Color.Black;
            this.ZhuCaiDan.ImageIndex = 1;
            this.ZhuCaiDan.ImageList = this.imageList1;
            this.ZhuCaiDan.Indent = 25;
            this.ZhuCaiDan.ItemHeight = 40;
            this.ZhuCaiDan.Location = new System.Drawing.Point(23, 104);
            this.ZhuCaiDan.Name = "ZhuCaiDan";
            treeNode1.Name = "节点2";
            treeNode1.Text = "添加课程";
            treeNode2.Name = "节点3";
            treeNode2.Text = "查看课程";
            treeNode3.ImageIndex = 1;
            treeNode3.Name = "节点1";
            treeNode3.SelectedImageKey = "1181963.gif";
            treeNode3.Text = "课程管理";
            treeNode4.Name = "节点32";
            treeNode4.Text = "添加题目";
            treeNode5.Name = "节点0";
            treeNode5.Text = "查看题目";
            treeNode6.ImageIndex = 1;
            treeNode6.Name = "节点26";
            treeNode6.SelectedImageIndex = 0;
            treeNode6.Text = "题库管理";
            treeNode7.Name = "节点35";
            treeNode7.Text = "试卷生成";
            treeNode8.ImageIndex = 1;
            treeNode8.Name = "节点33";
            treeNode8.SelectedImageIndex = 0;
            treeNode8.Text = "试卷管理";
            treeNode9.Name = "节点1";
            treeNode9.Text = "随机点名";
            treeNode10.Name = "节点0";
            treeNode10.Text = "小工具";
            this.ZhuCaiDan.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode3,
            treeNode6,
            treeNode8,
            treeNode10});
            this.ZhuCaiDan.SelectedImageIndex = 0;
            this.ZhuCaiDan.ShowLines = false;
            this.ZhuCaiDan.ShowPlusMinus = false;
            this.ZhuCaiDan.ShowRootLines = false;
            this.ZhuCaiDan.Size = new System.Drawing.Size(134, 599);
            this.ZhuCaiDan.TabIndex = 0;
            this.ZhuCaiDan.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.ZhuCaiDan_NodeMouseClick);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "1181963.gif");
            this.imageList1.Images.SetKeyName(1, "1181965.gif");
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1366, 82);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // pictureUserTouXiang
            // 
            this.pictureUserTouXiang.BackColor = System.Drawing.Color.Transparent;
            this.pictureUserTouXiang.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureUserTouXiang.BackgroundImage")));
            this.pictureUserTouXiang.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureUserTouXiang.Location = new System.Drawing.Point(1244, 7);
            this.pictureUserTouXiang.Name = "pictureUserTouXiang";
            this.pictureUserTouXiang.Size = new System.Drawing.Size(60, 60);
            this.pictureUserTouXiang.TabIndex = 3;
            this.pictureUserTouXiang.TabStop = false;
            // 
            // labUser
            // 
            this.labUser.AutoSize = true;
            this.labUser.BackColor = System.Drawing.Color.Transparent;
            this.labUser.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labUser.ForeColor = System.Drawing.Color.White;
            this.labUser.Location = new System.Drawing.Point(1079, 27);
            this.labUser.Name = "labUser";
            this.labUser.Size = new System.Drawing.Size(79, 19);
            this.labUser.TabIndex = 4;
            this.labUser.Text = "用户姓名：";
            // 
            // labUserName
            // 
            this.labUserName.AutoSize = true;
            this.labUserName.BackColor = System.Drawing.Color.Transparent;
            this.labUserName.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labUserName.ForeColor = System.Drawing.Color.White;
            this.labUserName.Location = new System.Drawing.Point(1164, 27);
            this.labUserName.Name = "labUserName";
            this.labUserName.Size = new System.Drawing.Size(0, 19);
            this.labUserName.TabIndex = 5;
            // 
            // labTuiChuXT
            // 
            this.labTuiChuXT.AutoSize = true;
            this.labTuiChuXT.BackColor = System.Drawing.Color.Transparent;
            this.labTuiChuXT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labTuiChuXT.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labTuiChuXT.ForeColor = System.Drawing.Color.White;
            this.labTuiChuXT.Location = new System.Drawing.Point(977, 27);
            this.labTuiChuXT.Name = "labTuiChuXT";
            this.labTuiChuXT.Size = new System.Drawing.Size(65, 19);
            this.labTuiChuXT.TabIndex = 6;
            this.labTuiChuXT.Text = "退出系统";
            this.labTuiChuXT.Click += new System.EventHandler(this.LabTuiChuXT_Click);
            // 
            // skinEngine1
            // 
            this.skinEngine1.SerialNumber = "";
            this.skinEngine1.SkinFile = null;
            // 
            // ZiCaiDan
            // 
            this.ZiCaiDan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(236)))), ((int)(((byte)(236)))));
            this.ZiCaiDan.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ZiCaiDan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ZiCaiDan.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ZiCaiDan.ImageIndex = 1;
            this.ZiCaiDan.ImageList = this.imageList1;
            this.ZiCaiDan.ItemHeight = 40;
            this.ZiCaiDan.Location = new System.Drawing.Point(23, 128);
            this.ZiCaiDan.Name = "ZiCaiDan";
            treeNode11.Name = "节点1";
            treeNode11.Text = "在线考试";
            treeNode12.ImageKey = "1181965.gif";
            treeNode12.Name = "节点0";
            treeNode12.SelectedImageIndex = 0;
            treeNode12.Text = "考试管理";
            treeNode13.Name = "节点3";
            treeNode13.Text = "查询成绩";
            treeNode14.Name = "节点5";
            treeNode14.Text = "错题分析";
            treeNode15.ImageIndex = 1;
            treeNode15.Name = "节点2";
            treeNode15.SelectedImageIndex = 0;
            treeNode15.Text = "成绩管理";
            this.ZiCaiDan.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode12,
            treeNode15});
            this.ZiCaiDan.SelectedImageIndex = 0;
            this.ZiCaiDan.ShowLines = false;
            this.ZiCaiDan.ShowPlusMinus = false;
            this.ZiCaiDan.ShowRootLines = false;
            this.ZiCaiDan.Size = new System.Drawing.Size(134, 384);
            this.ZiCaiDan.TabIndex = 7;
            this.ZiCaiDan.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.ZiCaiDan_NodeMouseClick);
            // 
            // ZhuYeMian
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1366, 700);
            this.Controls.Add(this.labTuiChuXT);
            this.Controls.Add(this.labUserName);
            this.Controls.Add(this.labUser);
            this.Controls.Add(this.pictureUserTouXiang);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.ZhuCaiDan);
            this.Controls.Add(this.ZiCaiDan);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ZhuYeMian";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ZhuYeMian_FormClosed);
            this.Load += new System.EventHandler(this.ZhuYeMian_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureUserTouXiang)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView ZhuCaiDan;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.PictureBox pictureUserTouXiang;
        private System.Windows.Forms.Label labUser;
        private System.Windows.Forms.Label labTuiChuXT;
        private Sunisoft.IrisSkin.SkinEngine skinEngine1;
        private System.Windows.Forms.Label labUserName;
        private System.Windows.Forms.TreeView ZiCaiDan;
    }
}

