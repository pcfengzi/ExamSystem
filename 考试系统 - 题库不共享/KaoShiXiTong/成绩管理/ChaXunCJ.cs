﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KaoShiXiTong
{
    public partial class ChaXunCJ : Form
    {
        public ChaXunCJ()
        {
            InitializeComponent();
        }

        private void ChaXunCJ_Load(object sender, EventArgs e)
        {
            ChaData();
        }

        void ChaData()
        {
            string ChengJiSql = string.Format(@"select * from ChengJi where UserID={0}", UserIDandUserName.UserID);
            DataTable ChengJiTa = DBHepler.Cha(ChengJiSql);
            dataVChengJi.AutoGenerateColumns = false;
            dataVChengJi.DataSource = ChengJiTa;
            for (int i = 0; i < ChengJiTa.Rows.Count; i++)
            {
                if (int.Parse(dataVChengJi.Rows[i].Cells[6].Value.ToString()) < 60)
                {
                    dataVChengJi.Rows[i].DefaultCellStyle.BackColor = Color.Red;
                }
                if (int.Parse(dataVChengJi.Rows[i].Cells[6].Value.ToString()) >= 60 && int.Parse(dataVChengJi.Rows[i].Cells[6].Value.ToString()) <= 70)
                {
                    dataVChengJi.Rows[i].DefaultCellStyle.BackColor = Color.Green;
                }
                if (int.Parse(dataVChengJi.Rows[i].Cells[6].Value.ToString()) >= 80)
                {
                    dataVChengJi.Rows[i].DefaultCellStyle.BackColor = Color.Blue;
                }
            }

            //取消列排序
            for (int i = 0; i < this.dataVChengJi.Columns.Count; i++)
            {
                this.dataVChengJi.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            }
        }

        private void ButDaoChuCJ_Click(object sender, EventArgs e)
        {
            //必须需要把每个字段取个别名，在excel中才会显示别名
            string ChaChengJiSQL = string.Format(@"select ShiJuanName as 试卷名称,KeChengName as 课程名称,XueShengName as 用户名称,
            KaiShiShiJian as 开始时间,JieShuShiJian as 结束时间,ZongChengJi as 总分数,ChengJi as 最终得分  from ChengJi where UserID={0}", UserIDandUserName.UserID);
            DataTable ChengJiTabel = DBHepler.Cha(ChaChengJiSQL);

            System.Diagnostics.Stopwatch timeWatch = System.Diagnostics.Stopwatch.StartNew();
            NPOIExcelHelper.DataTableToExcel(ChengJiTabel, "成绩", "D:/考试成绩.xls");
            MessageBox.Show("导出成功");
        }

        private void DataVChengJi_MouseEnter(object sender, EventArgs e)
        {
            ChaData();
        }

        private void DataVChengJi_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            this.dataVChengJi.ClearSelection();//取消默认选中的行
        }
    }
}
