﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KaoShiXiTong
{
    public partial class ChaKanDaAn : Form
    {
        public ChaKanDaAn()
        {
            InitializeComponent();
        }

        private void ChaKanDaAn_Load(object sender, EventArgs e)
        {
            XuanZeTiData();
            PanDuanTiData();
            TianKongTiData();
        }
        void XuanZeTiData()
        {
            string XuanZeTiSQL = string.Format(@"select ShiJuanName,KeCheng.KeChengName,TiMu,XuanXiangYi,XuanXiangEr,XuanXiangSan,
                                                XuanXiangSi,ZhengQueDaAn,YongHuDaAn
                                                 from ShiJuanDanXuanTi,KeCheng 
                                                 where KeCheng.KeChengID=ShiJuanDanXuanTi.KeChengID 
                                                 and ShiJuanName='{0}'  and TrueFale={1} and KeCheng.KeChengID={2}",
                                                 UserIDandUserName.ShiJuanName/*, UserIDandUserName.UserID*/, 2, UserIDandUserName.GetKeChengID);
            DataTable XuanZeTiTable = DBHepler.Cha(XuanZeTiSQL);
            dataXuanZeTi.AutoGenerateColumns = false;
            dataXuanZeTi.DataSource = XuanZeTiTable;
            dataXuanZeTi.RowTemplate.Height = 40;//值行高的设置
        }

        void PanDuanTiData()
        {
            string XuanZeTiSQL = string.Format(@"select ShiJuanName,KeCheng.KeChengName,TiMu,
                                                 ZhengQueDaAn,YongHuDaAn
                                                 from ShiJuanPanDuanTi,KeCheng 
                                                 where KeCheng.KeChengID=ShiJuanPanDuanTi.KeChengID 
                                                 and ShiJuanName='{0}' and TrueFale={1} and KeCheng.KeChengID={2}",
                                                 UserIDandUserName.ShiJuanName/*, UserIDandUserName.UserID*/, 2, UserIDandUserName.GetKeChengID);
            DataTable XuanZeTiTable = DBHepler.Cha(XuanZeTiSQL);
            dataPanDuanTi.AutoGenerateColumns = false;
            dataPanDuanTi.DataSource = XuanZeTiTable;
            dataPanDuanTi.RowTemplate.Height = 40;//值行高的设置
        }

        void TianKongTiData()
        {
            string XuanZeTiSQL = string.Format(@"select ShiJuanName,KeCheng.KeChengName,TiMu,
                                                 ZhengQueDaAn,YongHuDaAn
                                                 from ShiJuanTianKongTi,KeCheng 
                                                 where KeCheng.KeChengID=ShiJuanTianKongTi.KeChengID 
                                                 and ShiJuanName='{0}'  and TrueFale={1} and KeCheng.KeChengID={2}",
                                                 UserIDandUserName.ShiJuanName/*, UserIDandUserName.UserID*/, 2, UserIDandUserName.GetKeChengID);
            DataTable XuanZeTiTable = DBHepler.Cha(XuanZeTiSQL);
            dataTianKongTi.AutoGenerateColumns = false;
            dataTianKongTi.DataSource = XuanZeTiTable;
            dataTianKongTi.RowTemplate.Height = 40;//值行高的设置
        }

        private void TabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int Index = tabControl1.SelectedIndex;
            if (Index == 0)
            {
                XuanZeTiData();
            }
            else if (Index == 1)
            {
                PanDuanTiData();
            }
            else if (Index == 2)
            {
                TianKongTiData();
            }
        }

        private void ChaKanDaAn_MouseEnter(object sender, EventArgs e)
        {
            XuanZeTiData();
            PanDuanTiData();
            TianKongTiData();
        }
    }
}
