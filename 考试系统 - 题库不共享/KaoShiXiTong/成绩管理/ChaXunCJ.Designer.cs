﻿namespace KaoShiXiTong
{
    partial class ChaXunCJ
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataVChengJi = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.butDaoChuCJ = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataVChengJi)).BeginInit();
            this.SuspendLayout();
            // 
            // dataVChengJi
            // 
            this.dataVChengJi.AllowUserToAddRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            this.dataVChengJi.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataVChengJi.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataVChengJi.BackgroundColor = System.Drawing.Color.White;
            this.dataVChengJi.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column4,
            this.Column3,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column2});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataVChengJi.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataVChengJi.Location = new System.Drawing.Point(25, 26);
            this.dataVChengJi.Name = "dataVChengJi";
            this.dataVChengJi.RowHeadersVisible = false;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            this.dataVChengJi.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dataVChengJi.RowTemplate.Height = 23;
            this.dataVChengJi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataVChengJi.Size = new System.Drawing.Size(723, 354);
            this.dataVChengJi.TabIndex = 0;
            this.dataVChengJi.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.DataVChengJi_DataBindingComplete);
            this.dataVChengJi.MouseEnter += new System.EventHandler(this.DataVChengJi_MouseEnter);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "ShiJuanName";
            this.Column1.HeaderText = "试卷名称";
            this.Column1.Name = "Column1";
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "KeChengName";
            this.Column4.HeaderText = "课程名称";
            this.Column4.Name = "Column4";
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "XueShengName";
            this.Column3.HeaderText = "用户名称";
            this.Column3.Name = "Column3";
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "KaiShiShiJian";
            this.Column5.HeaderText = "开始时间";
            this.Column5.Name = "Column5";
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "JieShuShiJian";
            this.Column6.HeaderText = "结束时间";
            this.Column6.Name = "Column6";
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "ZongChengJi";
            this.Column7.HeaderText = "总分数";
            this.Column7.Name = "Column7";
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "ChengJi";
            this.Column2.HeaderText = "最终得分";
            this.Column2.Name = "Column2";
            // 
            // butDaoChuCJ
            // 
            this.butDaoChuCJ.BackColor = System.Drawing.Color.Gainsboro;
            this.butDaoChuCJ.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butDaoChuCJ.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butDaoChuCJ.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.butDaoChuCJ.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.butDaoChuCJ.Location = new System.Drawing.Point(660, 415);
            this.butDaoChuCJ.Name = "butDaoChuCJ";
            this.butDaoChuCJ.Size = new System.Drawing.Size(75, 23);
            this.butDaoChuCJ.TabIndex = 1;
            this.butDaoChuCJ.Text = "导出成绩";
            this.butDaoChuCJ.UseVisualStyleBackColor = false;
            this.butDaoChuCJ.Click += new System.EventHandler(this.ButDaoChuCJ_Click);
            // 
            // ChaXunCJ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(236)))), ((int)(((byte)(236)))));
            this.ClientSize = new System.Drawing.Size(771, 453);
            this.Controls.Add(this.butDaoChuCJ);
            this.Controls.Add(this.dataVChengJi);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ChaXunCJ";
            this.Text = "成绩查询";
            this.Load += new System.EventHandler(this.ChaXunCJ_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataVChengJi)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataVChengJi;
        private System.Windows.Forms.Button butDaoChuCJ;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
    }
}