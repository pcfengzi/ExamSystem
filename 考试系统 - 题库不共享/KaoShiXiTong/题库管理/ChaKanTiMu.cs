﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KaoShiXiTong
{
    public partial class ChaKanTiMu : Form
    {
        public ChaKanTiMu()
        {
            InitializeComponent();
        }

        private void ChaKanTiMu_Load(object sender, EventArgs e)
        {

            PanTianKeCheng();
            ComTiXingID();
            DanDuoKeCheng();

            ChaDanXuanTi();
            ChaPanDuanTi();
            ChaTianKongTi();

            panelDanDuo.Visible = true;
            panelPanTian.Visible = false;
        }

        /// <summary>
        /// 窗体加载时把题型下拉框添加题型
        /// </summary>
        void ComTiXingID()
        {
            string SqlTiXing = "select * from TiXing";
            DataTable TiXingTa = DBHepler.Cha(SqlTiXing);
            comTiXing.DataSource = TiXingTa;
            comTiXing.ValueMember = "TiXingID";
            comTiXing.DisplayMember = "TiXingName";
        }

        /// <summary>
        /// 给单选题与多选题课程下拉框添加课程
        /// </summary>
        void DanDuoKeCheng()
        {
            string SqlKeCheng = string.Format(@"select * from KeCheng where UserID={0}", UserIDandUserName.UserID);
            DataTable KeChengTa = DBHepler.Cha(SqlKeCheng);
            comDanKeCheng.DataSource = KeChengTa;
            comDanKeCheng.ValueMember = "KeChengID";
            comDanKeCheng.DisplayMember = "KeChengName";

        }

        /// <summary>
        /// 给判断题题与填空题课程下拉框添加课程
        /// </summary>
        void PanTianKeCheng()
        {
            string SqlKeCheng_2 = string.Format(@"select * from KeCheng where UserID={0}", UserIDandUserName.UserID);
            DataTable KeChengTa_2 = DBHepler.Cha(SqlKeCheng_2);
            comPanTian.DataSource = KeChengTa_2;
            comPanTian.ValueMember = "KeChengID";
            comPanTian.DisplayMember = "KeChengName";
        }
        /// <summary>
        /// 给不同的容器中的下拉框加载课程，并显示索引对应的界面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComTiXing_SelectedIndexChanged(object sender, EventArgs e)
        {
            int TiXingIndex = comTiXing.SelectedIndex;

            if (TiXingIndex == 0)
            {
                //显示单选题界面
                DanDuoKeCheng();

                panelDanDuo.Visible = true;
                panelPanTian.Visible = false;

            }
            else if (TiXingIndex == 1)
            {
                //显示判断题界面
                PanTianKeCheng();

                panelDanDuo.Visible = false;
                panelPanTian.Visible = true;
            }
            else if (TiXingIndex == 2)
            {
                //显示填空题界面
                PanTianKeCheng();

                panelDanDuo.Visible = false;
                panelPanTian.Visible = true;
            }
        }

        double FYe_XuanZeTiCount = 0;
        /// <summary>
        /// 显示单选题表数据
        /// </summary>
        void ChaDanXuanTi()
        {
            try
            {
                int TiXingIndex = (int)comTiXing.SelectedValue;
                int IndexKeChengID = (int)comDanKeCheng.SelectedValue;

                //string KeChengTa = string.Format(@"select * from DanXuanTi where KeChengID={0} and TiXingID={1} and UserID={2}",
                //    IndexKeChengID, TiXingIndex, UserIDandUserName.UserID);
                //DataTable KCTa = DBHepler.Cha(KeChengTa);

                dataVDanAndDuo.AutoGenerateColumns = false;
                dataVDanAndDuo.DataSource = XuanZeTiFenYe(20, 1, UserIDandUserName.UserID);
                dataVDanAndDuo.RowTemplate.Height = 40;//值行高的设置

                string KeChengTa = string.Format(@"select count(*) from DanXuanTi where KeChengID={0} and TiXingID={1} and UserID={2}",
                    IndexKeChengID, TiXingIndex, UserIDandUserName.UserID);
                FYe_XuanZeTiCount = (int)DBHepler.JuHe(KeChengTa);
                labXuanFenZong.Text = Math.Ceiling(FYe_XuanZeTiCount / 20).ToString();//显示总页码
                labXuanZeTiSumHangShu.Text = FYe_XuanZeTiCount.ToString();//总记录行数
            }
            catch
            {
            }
        }

        /// <summary>
        /// 通过对应的索引获取对应的表数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComDanKeCheng_SelectedIndexChanged(object sender, EventArgs e)
        {
            int TiXingIndex = comTiXing.SelectedIndex;
            if (TiXingIndex == 0)
            {
                ChaDanXuanTi();
            }

        }

        /// <summary>
        /// 更新单选题数据
        /// </summary>
        /// <returns></returns>
        private bool UpdateDanXuan()
        {
            DataRow rowDelete = (dataVDanAndDuo.CurrentRow.DataBoundItem as DataRowView).Row;
            int DWeID = (int)rowDelete["ID"];//删除,更新 where   ID

            string UpdateSql = string.Format(@"update DanXuanTi set 
                                        TiMu='{0}',XuanXiangYi='{1}',XuanXiangEr='{2}',XuanXiangSan='{3}',XuanXiangSi='{4}',ZhengQueDaAn='{5}' where ID={6}",
                                        strTiMu, strXuanXiangYi, strXuanXiangEr, strXuanXiangSan, strXuanXiangSi, strZhengQueDaAn, DWeID);
            if (DBHepler.ZSG(UpdateSql))
            {
                return true;
            }
            return false;
        }

        string strTiMu = "";
        string strXuanXiangYi = "";
        string strXuanXiangEr = "";
        string strXuanXiangSan = "";
        string strXuanXiangSi = "";
        string strZhengQueDaAn = "";
        private void DataVDanAndDuo_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataRow rowDelete = (dataVDanAndDuo.CurrentRow.DataBoundItem as DataRowView).Row;
            int DWeID = (int)rowDelete["ID"];//删除,更新 where   ID

            int TiXingIndex = comTiXing.SelectedIndex;

            int Index = e.ColumnIndex;//获取按钮索引
            if (Index == 6)
            {
                UpDateRow();//获取字段进行更改
                //更新
                if (strTiMu != "")
                {
                    if (strXuanXiangYi != "")
                    {
                        if (strXuanXiangEr != "")
                        {
                            if (strXuanXiangSan != "")
                            {
                                if (strXuanXiangSi != "")
                                {
                                    if (strZhengQueDaAn != "")
                                    {
                                        if (TiXingIndex == 0)//单选题
                                        {
                                            if (UpdateDanXuan())
                                            {
                                                MessageBox.Show("更新成功");
                                                ChaDanXuanTi();
                                            }
                                            else
                                            {
                                                MessageBox.Show("更新失败");
                                                ChaDanXuanTi();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("答案不能为空");

                                    }
                                }
                                else
                                {
                                    MessageBox.Show("选项D不能为空");

                                }
                            }
                            else
                            {
                                MessageBox.Show("选项C不能为空");

                            }
                        }
                        else
                        {
                            MessageBox.Show("选项B不能为空");

                        }
                    }
                    else
                    {
                        MessageBox.Show("选项A不能为空");

                    }
                }
                else
                {
                    MessageBox.Show("题目不能为空");

                }
            }



            else if (Index == 7)
            {
                //删除
                if (MessageBox.Show("确定删除吗？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) == DialogResult.OK)
                {
                    if (TiXingIndex == 0)
                    {
                        string DeleteSql = string.Format(@"delete  from DanXuanTi where ID={0}", DWeID);
                        if (DBHepler.ZSG(DeleteSql))
                        {
                            MessageBox.Show("删除成功");
                            ChaDanXuanTi();
                        }
                        else
                        {
                            MessageBox.Show("删除失败");
                            ChaDanXuanTi();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 获取更改后的字段
        /// </summary>
        void UpDateRow()
        {
            DataRow UpdateRow = (dataVDanAndDuo.CurrentRow.DataBoundItem as DataRowView).Row;
            strTiMu = UpdateRow["TiMu"].ToString();
            strXuanXiangYi = UpdateRow["XuanXiangYi"].ToString();
            strXuanXiangEr = UpdateRow["XuanXiangEr"].ToString();
            strXuanXiangSan = UpdateRow["XuanXiangSan"].ToString();
            strXuanXiangSi = UpdateRow["XuanXiangSi"].ToString();
            strZhengQueDaAn = UpdateRow["ZhengQueDaAn"].ToString();
        }

        private void DataVDanAndDuo_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            UpDateRow();
        }

        double FYe_PanDuanTiCount = 0;
        /// <summary>
        /// 显示判断题表数据
        /// </summary>
        void ChaPanDuanTi()
        {
            labDangQianYeMa.Text = 1.ToString();
            try
            {
                int TiXingIndex = (int)comTiXing.SelectedValue;

                int IndexKeChengID = (int)comPanTian.SelectedValue;

                //string KeChengTa = string.Format(@"select * from PanDuanTi where KeChengID={0} and TiXingID={1} and UserID={2}",
                //    IndexKeChengID, TiXingIndex, UserIDandUserName.UserID);
                //DataTable KCTa = DBHepler.Cha(KeChengTa);

                dataVPanTian.AutoGenerateColumns = false;
                dataVPanTian.DataSource = FenYe(20, 1, UserIDandUserName.UserID);
                dataVPanTian.RowTemplate.Height = 40;//值行高的设置

                string CountKeChengTa = string.Format(@"select count(*) from PanDuanTi where KeChengID={0} and TiXingID={1} and UserID={2}",
                    IndexKeChengID, TiXingIndex, UserIDandUserName.UserID);
                FYe_PanDuanTiCount = (int)DBHepler.JuHe(CountKeChengTa);
                labZongYeMa.Text = Math.Ceiling(FYe_PanDuanTiCount / 20).ToString();//显示总页码
                labPAndTSumHangShu.Text = FYe_PanDuanTiCount.ToString();//显示总记录行数
            }
            catch
            {
            }
        }

        double FYe_TianKongTiCount = 0;
        /// <summary>
        /// 显示填空题表数据
        /// </summary>
        void ChaTianKongTi()
        {
            labDangQianYeMa.Text = 1.ToString();
            try
            {
                int TiXingIndex = (int)comTiXing.SelectedValue;

                int IndexKeChengID = (int)comPanTian.SelectedValue;

                //string KeChengTa = string.Format(@"select * from TianKongTi where KeChengID={0} and TiXingID={1} and UserID={2}",
                //    IndexKeChengID, TiXingIndex, UserIDandUserName.UserID);
                //DataTable KCTa = DBHepler.Cha(KeChengTa);
                dataVPanTian.AutoGenerateColumns = false;
                dataVPanTian.DataSource = TianKongTiFenYe(20, 1, UserIDandUserName.UserID);
                dataVPanTian.RowTemplate.Height = 40;//值行高的设置

                string CountKeChengTa = string.Format(@"select count(*) from TianKongTi where KeChengID={0} and TiXingID={1} and UserID={2}",
                    IndexKeChengID, TiXingIndex, UserIDandUserName.UserID);
                FYe_TianKongTiCount = (int)DBHepler.JuHe(CountKeChengTa);
                labZongYeMa.Text = Math.Ceiling(FYe_TianKongTiCount / 20).ToString();//显示总页码
                labPAndTSumHangShu.Text = FYe_TianKongTiCount.ToString();//显示总记录行数
            }
            catch
            {
            }
        }

        private void ComPanTian_SelectedIndexChanged(object sender, EventArgs e)
        {
            int TiXingIndex = comTiXing.SelectedIndex;
            if (TiXingIndex == 1)
            {
                ChaPanDuanTi();
            }
            else if (TiXingIndex == 2)
            {
                ChaTianKongTi();
            }
        }

        string PTTiMu = "";
        string PTDaAn = "";

        void UDPanTian()
        {
            DataRow PTrow = (dataVPanTian.CurrentRow.DataBoundItem as DataRowView).Row;
            PTTiMu = PTrow["TiMu"].ToString();
            PTDaAn = PTrow["ZhengQueDaAn"].ToString();
        }
        private void DataVPanTian_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            UDPanTian();
        }



        private void DataVPanTian_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            UDPanTian();

            DataRow RowID = (dataVPanTian.CurrentRow.DataBoundItem as DataRowView).Row;
            int UDWherID = (int)RowID["ID"];

            int TiXingIndex = comTiXing.SelectedIndex;//题型下拉框索引

            int PTIndex = e.ColumnIndex;

            if (PTIndex == 2)
            {
                if (PTTiMu != "")
                {
                    if (PTDaAn != "")
                    {
                        //更新
                        if (TiXingIndex == 1)
                        {
                            //判断题
                            string SqlUpdate = string.Format(@"update PanDuanTi set TiMu='{0}',ZhengQueDaAn='{1}' where ID={2}",
                                PTTiMu, PTDaAn, UDWherID);
                            if (DBHepler.ZSG(SqlUpdate))
                            {
                                MessageBox.Show("更新成功");
                                ChaPanDuanTi();
                            }
                            else
                            {
                                MessageBox.Show("更新失败");
                                ChaPanDuanTi();
                            }

                        }
                        else if (TiXingIndex == 2)
                        {
                            //填空题
                            string SqlUpdate = string.Format(@"update TianKongTi set TiMu='{0}',ZhengQueDaAn='{1}'  where ID={2}",
                               PTTiMu, PTDaAn, UDWherID);
                            if (DBHepler.ZSG(SqlUpdate))
                            {
                                MessageBox.Show("更新成功");
                                ChaTianKongTi();
                            }
                            else
                            {
                                MessageBox.Show("更新失败");
                                ChaTianKongTi();
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("答案不能为空");

                    }
                }
                else
                {
                    MessageBox.Show("题目不能为空");

                }
            }
            else if (PTIndex == 3)
            {
                if (MessageBox.Show("确定删除吗？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) == DialogResult.OK)
                {
                    //删除
                    if (TiXingIndex == 1)
                    {
                        //判断题
                        string SqlDelete = string.Format(@"delete from PanDuanTi where ID={0}", UDWherID);
                        if (DBHepler.ZSG(SqlDelete))
                        {
                            MessageBox.Show("删除成功");
                            ChaPanDuanTi();
                        }
                        else
                        {
                            MessageBox.Show("删除失败");
                            ChaPanDuanTi();
                        }
                    }

                    else if (TiXingIndex == 2)
                    {
                        //填空题
                        string SqlDelete = string.Format(@"delete from TianKongTi where ID={0}", UDWherID);
                        if (DBHepler.ZSG(SqlDelete))
                        {
                            MessageBox.Show("删除成功");
                            ChaTianKongTi();
                        }
                        else
                        {
                            MessageBox.Show("删除失败");
                            ChaTianKongTi();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 判断题
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButFShangYiYe_Click(object sender, EventArgs e)
        {
            int TiXingIndex = (int)comTiXing.SelectedValue;//题型ID
            if (TiXingIndex == 3)//判断题
            {
                if (i > 1)
                {
                    labDangQianYeMa.Text = 1.ToString();
                    dataVPanTian.DataSource = FenYe(20, --i, UserIDandUserName.UserID);
                    labDangQianYeMa.Text = i.ToString();
                }
            }
            if (TiXingIndex == 4)//填空题
            {
                if (i2 > 1)
                {
                    labDangQianYeMa.Text = 1.ToString();
                    dataVPanTian.DataSource = TianKongTiFenYe(20, --i2, UserIDandUserName.UserID);
                    labDangQianYeMa.Text = i2.ToString();
                }
            }
        }

        //@pageSize int,--每页数据条数
        //@pageIndex int,--当前页数（页码）
        //@TiXingID int,--题型ID
        //@KeChengID int,--课程ID
        //@UserID int,--用户ID

        /// <summary>
        /// 判断题的分页
        /// </summary>
        /// <param name="MeiYeShuJuHuangShu">每页数据条数</param>
        /// <param name="TiaoZhuanYeShu">当前页数</param>
        /// <param name="FYe_TiXingID">题型ID</param>
        /// <param name="FYe_KeChengID">课程ID</param>
        /// <param name="FYe_UserID">用户ID</param>
        /// <returns></returns>
        private DataTable FenYe(int MeiYeShuJuHuangShu, int TiaoZhuanYeShu, int FYe_UserID)
        {
            int TiXingIndex = (int)comTiXing.SelectedValue;//题型ID
            int IndexKeChengID = (int)comPanTian.SelectedValue;//课程ID

            string FYe_Sql = string.Format(@"declare @num int exec UP_DanXuanTi {0},{1},{2},{3},{4},@num output",
                MeiYeShuJuHuangShu, TiaoZhuanYeShu, TiXingIndex, IndexKeChengID, FYe_UserID);
            DataTable Ta = DBHepler.Cha(FYe_Sql);
            return Ta;
        }
        /// <summary>
        /// 填空题分页
        /// </summary>
        /// <param name="MeiYeShuJuHuangShu"></param>
        /// <param name="TiaoZhuanYeShu"></param>
        /// <param name="FYe_UserID"></param>
        /// <returns></returns>
        private DataTable TianKongTiFenYe(int MeiYeShuJuHuangShu, int TiaoZhuanYeShu, int FYe_UserID)
        {
            int TiXingIndex = (int)comTiXing.SelectedValue;//题型ID
            int IndexKeChengID = (int)comPanTian.SelectedValue;//课程ID

            string FYe_Sql = string.Format(@"declare @num int exec UP_TianKongTi {0},{1},{2},{3},{4},@num output",
                MeiYeShuJuHuangShu, TiaoZhuanYeShu, TiXingIndex, IndexKeChengID, FYe_UserID);
            DataTable Ta = DBHepler.Cha(FYe_Sql);
            return Ta;
        }
        /// <summary>
        /// 选择题分页
        /// </summary>
        /// <param name="MeiYeShuJuHuangShu"></param>
        /// <param name="TiaoZhuanYeShu"></param>
        /// <param name="FYe_UserID"></param>
        /// <returns></returns>
        private DataTable XuanZeTiFenYe(int MeiYeShuJuHuangShu, int TiaoZhuanYeShu, int FYe_UserID)
        {
            try
            {
                int TiXingIndex = (int)comTiXing.SelectedValue;//题型ID
                int IndexKeChengID = (int)comPanTian.SelectedValue;//课程ID

                string FYe_Sql = string.Format(@"declare @num int exec UP_XuanZeTi {0},{1},{2},{3},{4},@num output",
                    MeiYeShuJuHuangShu, TiaoZhuanYeShu, TiXingIndex, IndexKeChengID, FYe_UserID);
                DataTable Ta = DBHepler.Cha(FYe_Sql);
                return Ta;
            }
            catch (Exception)
            {

                throw;
            }
        }


        int i = 1;
        int i2 = 1;
        /// <summary>
        /// 判断题
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButFXiaYiYe_Click(object sender, EventArgs e)
        {
            int TiXingIndex = (int)comTiXing.SelectedValue;//题型ID
            if (TiXingIndex == 3)
            {
                double FYe_ZongYeShu = Math.Ceiling(FYe_PanDuanTiCount / 20);
                if (i < FYe_ZongYeShu)
                {
                    labDangQianYeMa.Text = 1.ToString();
                    dataVPanTian.DataSource = FenYe(20, ++i, UserIDandUserName.UserID);
                    labDangQianYeMa.Text = i.ToString();
                }
            }
            if (TiXingIndex == 4)
            {
                double FYe_ZongYeShu = Math.Ceiling(FYe_TianKongTiCount / 20);
                if (i2 < FYe_ZongYeShu)
                {
                    labDangQianYeMa.Text = 1.ToString();
                    dataVPanTian.DataSource = TianKongTiFenYe(20, ++i2, UserIDandUserName.UserID);
                    labDangQianYeMa.Text = i2.ToString();
                }
            }
        }

        /// <summary>
        /// 判断题的跳转
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButFTiaoZhuan_Click(object sender, EventArgs e)
        {
            int TiXingIndex = (int)comTiXing.SelectedValue;//题型ID
            if (TiXingIndex == 3)
            {
                try
                {
                    i = int.Parse(txtFYe_TiaoZhuan.Text);
                }
                catch
                {
                }
                if (i <= int.Parse(labZongYeMa.Text) && i > 0)
                {
                    labDangQianYeMa.Text = 1.ToString();
                    dataVPanTian.DataSource = FenYe(20, i, UserIDandUserName.UserID);
                    labDangQianYeMa.Text = i.ToString();
                }
            }
            if (TiXingIndex == 4)
            {
                try
                {
                    i2 = int.Parse(txtFYe_TiaoZhuan.Text);
                }
                catch
                {
                }
                if (i2 <= int.Parse(labZongYeMa.Text) && i2 > 0)
                {
                    labDangQianYeMa.Text = 1.ToString();
                    dataVPanTian.DataSource = TianKongTiFenYe(20, i2, UserIDandUserName.UserID);
                    labDangQianYeMa.Text = i2.ToString();
                }
            }
            txtFYe_TiaoZhuan.Text = "";
        }

        int i3 = 0;
        private void ButXuanFenShang_Click(object sender, EventArgs e)
        {
            int TiXingIndex = (int)comTiXing.SelectedValue;//题型ID
            if (TiXingIndex == 1)//选择题
            {
                if (i3 > 1)
                {
                    labXuanFenIndex.Text = 1.ToString();
                    dataVDanAndDuo.DataSource = XuanZeTiFenYe(20, --i3, UserIDandUserName.UserID);
                    labXuanFenIndex.Text = i3.ToString();
                }
            }
        }

        private void ButXuanFenXiaYi_Click(object sender, EventArgs e)
        {
            int TiXingIndex = (int)comTiXing.SelectedValue;//题型ID
            if (TiXingIndex == 1)
            {
                double FYe_ZongYeShu = Math.Ceiling(FYe_XuanZeTiCount / 20);
                if (i3 < FYe_ZongYeShu)
                {
                    labXuanFenIndex.Text = 1.ToString();
                    dataVDanAndDuo.DataSource = XuanZeTiFenYe(20, ++i3, UserIDandUserName.UserID);
                    labXuanFenIndex.Text = i3.ToString();
                }
            }
        }

        private void ButXuanTiaoZhuan_Click(object sender, EventArgs e)
        {
            int TiXingIndex = (int)comTiXing.SelectedValue;//题型ID
            if (TiXingIndex == 1)
            {
                try
                {
                    i3 = int.Parse(txtXuanFenTiao.Text);
                }
                catch
                {
                }
                if (i3 <= int.Parse(labXuanFenZong.Text) && i3 > 0)
                {
                    labXuanFenIndex.Text = 1.ToString();
                    dataVDanAndDuo.DataSource = XuanZeTiFenYe(20, i3, UserIDandUserName.UserID);
                    labXuanFenIndex.Text = i3.ToString();
                }
            }
            txtXuanFenTiao.Text = "";
        }
    }
}
