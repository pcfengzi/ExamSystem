﻿namespace KaoShiXiTong
{
    partial class ChaKanTiMu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.comTiXing = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panelDanDuo = new System.Windows.Forms.Panel();
            this.labXuanZeTiSumHangShu = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labXuanFenZong = new System.Windows.Forms.Label();
            this.labXuanFenIndex = new System.Windows.Forms.Label();
            this.txtXuanFenTiao = new System.Windows.Forms.TextBox();
            this.butXuanTiaoZhuan = new System.Windows.Forms.Button();
            this.butXuanFenXiaYi = new System.Windows.Forms.Button();
            this.butXuanFenShang = new System.Windows.Forms.Button();
            this.dataVDanAndDuo = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.comDanKeCheng = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panelPanTian = new System.Windows.Forms.Panel();
            this.labPAndTSumHangShu = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFYe_TiaoZhuan = new System.Windows.Forms.TextBox();
            this.labZongYeMa = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labDangQianYeMa = new System.Windows.Forms.Label();
            this.butFTiaoZhuan = new System.Windows.Forms.Button();
            this.butFXiaYiYe = new System.Windows.Forms.Button();
            this.butFShangYiYe = new System.Windows.Forms.Button();
            this.dataVPanTian = new System.Windows.Forms.DataGridView();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.comPanTian = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panelDanDuo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataVDanAndDuo)).BeginInit();
            this.panelPanTian.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataVPanTian)).BeginInit();
            this.SuspendLayout();
            // 
            // comTiXing
            // 
            this.comTiXing.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comTiXing.FormattingEnabled = true;
            this.comTiXing.Location = new System.Drawing.Point(424, 18);
            this.comTiXing.Name = "comTiXing";
            this.comTiXing.Size = new System.Drawing.Size(121, 25);
            this.comTiXing.TabIndex = 1;
            this.comTiXing.SelectedIndexChanged += new System.EventHandler(this.ComTiXing_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(328, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "选择题型查看";
            // 
            // panelDanDuo
            // 
            this.panelDanDuo.BackColor = System.Drawing.Color.Transparent;
            this.panelDanDuo.Controls.Add(this.labXuanZeTiSumHangShu);
            this.panelDanDuo.Controls.Add(this.label6);
            this.panelDanDuo.Controls.Add(this.label7);
            this.panelDanDuo.Controls.Add(this.labXuanFenZong);
            this.panelDanDuo.Controls.Add(this.labXuanFenIndex);
            this.panelDanDuo.Controls.Add(this.txtXuanFenTiao);
            this.panelDanDuo.Controls.Add(this.butXuanTiaoZhuan);
            this.panelDanDuo.Controls.Add(this.butXuanFenXiaYi);
            this.panelDanDuo.Controls.Add(this.butXuanFenShang);
            this.panelDanDuo.Controls.Add(this.dataVDanAndDuo);
            this.panelDanDuo.Controls.Add(this.comDanKeCheng);
            this.panelDanDuo.Controls.Add(this.label4);
            this.panelDanDuo.Location = new System.Drawing.Point(22, 59);
            this.panelDanDuo.Name = "panelDanDuo";
            this.panelDanDuo.Size = new System.Drawing.Size(879, 441);
            this.panelDanDuo.TabIndex = 2;
            // 
            // labXuanZeTiSumHangShu
            // 
            this.labXuanZeTiSumHangShu.AutoSize = true;
            this.labXuanZeTiSumHangShu.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labXuanZeTiSumHangShu.Location = new System.Drawing.Point(766, 408);
            this.labXuanZeTiSumHangShu.Name = "labXuanZeTiSumHangShu";
            this.labXuanZeTiSumHangShu.Size = new System.Drawing.Size(15, 17);
            this.labXuanZeTiSumHangShu.TabIndex = 6;
            this.labXuanZeTiSumHangShu.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(261, 404);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "/";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(687, 409);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 17);
            this.label7.TabIndex = 6;
            this.label7.Text = "总记录行数：";
            // 
            // labXuanFenZong
            // 
            this.labXuanFenZong.AutoSize = true;
            this.labXuanFenZong.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labXuanFenZong.Location = new System.Drawing.Point(277, 404);
            this.labXuanFenZong.Name = "labXuanFenZong";
            this.labXuanFenZong.Size = new System.Drawing.Size(15, 17);
            this.labXuanFenZong.TabIndex = 5;
            this.labXuanFenZong.Text = "0";
            // 
            // labXuanFenIndex
            // 
            this.labXuanFenIndex.AutoSize = true;
            this.labXuanFenIndex.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labXuanFenIndex.Location = new System.Drawing.Point(243, 404);
            this.labXuanFenIndex.Name = "labXuanFenIndex";
            this.labXuanFenIndex.Size = new System.Drawing.Size(15, 17);
            this.labXuanFenIndex.TabIndex = 5;
            this.labXuanFenIndex.Text = "1";
            // 
            // txtXuanFenTiao
            // 
            this.txtXuanFenTiao.Location = new System.Drawing.Point(472, 405);
            this.txtXuanFenTiao.Name = "txtXuanFenTiao";
            this.txtXuanFenTiao.Size = new System.Drawing.Size(68, 21);
            this.txtXuanFenTiao.TabIndex = 4;
            // 
            // butXuanTiaoZhuan
            // 
            this.butXuanTiaoZhuan.Location = new System.Drawing.Point(590, 405);
            this.butXuanTiaoZhuan.Name = "butXuanTiaoZhuan";
            this.butXuanTiaoZhuan.Size = new System.Drawing.Size(75, 23);
            this.butXuanTiaoZhuan.TabIndex = 3;
            this.butXuanTiaoZhuan.Text = "跳转";
            this.butXuanTiaoZhuan.UseVisualStyleBackColor = true;
            this.butXuanTiaoZhuan.Click += new System.EventHandler(this.ButXuanTiaoZhuan_Click);
            // 
            // butXuanFenXiaYi
            // 
            this.butXuanFenXiaYi.Location = new System.Drawing.Point(355, 405);
            this.butXuanFenXiaYi.Name = "butXuanFenXiaYi";
            this.butXuanFenXiaYi.Size = new System.Drawing.Size(75, 23);
            this.butXuanFenXiaYi.TabIndex = 3;
            this.butXuanFenXiaYi.Text = "下一页";
            this.butXuanFenXiaYi.UseVisualStyleBackColor = true;
            this.butXuanFenXiaYi.Click += new System.EventHandler(this.ButXuanFenXiaYi_Click);
            // 
            // butXuanFenShang
            // 
            this.butXuanFenShang.Location = new System.Drawing.Point(111, 405);
            this.butXuanFenShang.Name = "butXuanFenShang";
            this.butXuanFenShang.Size = new System.Drawing.Size(75, 23);
            this.butXuanFenShang.TabIndex = 3;
            this.butXuanFenShang.Text = "上一页";
            this.butXuanFenShang.UseVisualStyleBackColor = true;
            this.butXuanFenShang.Click += new System.EventHandler(this.ButXuanFenShang_Click);
            // 
            // dataVDanAndDuo
            // 
            this.dataVDanAndDuo.AllowUserToAddRows = false;
            this.dataVDanAndDuo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataVDanAndDuo.BackgroundColor = System.Drawing.Color.White;
            this.dataVDanAndDuo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataVDanAndDuo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8});
            this.dataVDanAndDuo.Location = new System.Drawing.Point(15, 59);
            this.dataVDanAndDuo.Name = "dataVDanAndDuo";
            this.dataVDanAndDuo.RowHeadersVisible = false;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dataVDanAndDuo.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dataVDanAndDuo.RowTemplate.Height = 23;
            this.dataVDanAndDuo.Size = new System.Drawing.Size(850, 329);
            this.dataVDanAndDuo.TabIndex = 2;
            this.dataVDanAndDuo.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataVDanAndDuo_CellContentClick);
            this.dataVDanAndDuo.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataVDanAndDuo_CellEndEdit);
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column1.DataPropertyName = "TiMu";
            this.Column1.HeaderText = "题目";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "XuanXiangYi";
            this.Column2.HeaderText = "选项A";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "XuanXiangEr";
            this.Column3.HeaderText = "选项B";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "XuanXiangSan";
            this.Column4.HeaderText = "选项C";
            this.Column4.Name = "Column4";
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "XuanXiangSi";
            this.Column5.HeaderText = "选项D";
            this.Column5.Name = "Column5";
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "ZhengQueDaAn";
            this.Column6.HeaderText = "正确答案";
            this.Column6.Name = "Column6";
            // 
            // Column7
            // 
            this.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.NullValue = "更新";
            this.Column7.DefaultCellStyle = dataGridViewCellStyle1;
            this.Column7.HeaderText = "更新";
            this.Column7.Name = "Column7";
            this.Column7.Width = 50;
            // 
            // Column8
            // 
            this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.NullValue = "删除";
            this.Column8.DefaultCellStyle = dataGridViewCellStyle2;
            this.Column8.HeaderText = "删除";
            this.Column8.Name = "Column8";
            this.Column8.Width = 50;
            // 
            // comDanKeCheng
            // 
            this.comDanKeCheng.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comDanKeCheng.FormattingEnabled = true;
            this.comDanKeCheng.Location = new System.Drawing.Point(404, 27);
            this.comDanKeCheng.Name = "comDanKeCheng";
            this.comDanKeCheng.Size = new System.Drawing.Size(121, 25);
            this.comDanKeCheng.TabIndex = 1;
            this.comDanKeCheng.SelectedIndexChanged += new System.EventHandler(this.ComDanKeCheng_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(308, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "选择课程查看";
            // 
            // panelPanTian
            // 
            this.panelPanTian.BackColor = System.Drawing.Color.Transparent;
            this.panelPanTian.Controls.Add(this.labPAndTSumHangShu);
            this.panelPanTian.Controls.Add(this.label1);
            this.panelPanTian.Controls.Add(this.txtFYe_TiaoZhuan);
            this.panelPanTian.Controls.Add(this.labZongYeMa);
            this.panelPanTian.Controls.Add(this.label3);
            this.panelPanTian.Controls.Add(this.labDangQianYeMa);
            this.panelPanTian.Controls.Add(this.butFTiaoZhuan);
            this.panelPanTian.Controls.Add(this.butFXiaYiYe);
            this.panelPanTian.Controls.Add(this.butFShangYiYe);
            this.panelPanTian.Controls.Add(this.dataVPanTian);
            this.panelPanTian.Controls.Add(this.comPanTian);
            this.panelPanTian.Controls.Add(this.label5);
            this.panelPanTian.Location = new System.Drawing.Point(21, 59);
            this.panelPanTian.Name = "panelPanTian";
            this.panelPanTian.Size = new System.Drawing.Size(880, 441);
            this.panelPanTian.TabIndex = 2;
            // 
            // labPAndTSumHangShu
            // 
            this.labPAndTSumHangShu.AutoSize = true;
            this.labPAndTSumHangShu.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labPAndTSumHangShu.Location = new System.Drawing.Point(764, 411);
            this.labPAndTSumHangShu.Name = "labPAndTSumHangShu";
            this.labPAndTSumHangShu.Size = new System.Drawing.Size(15, 17);
            this.labPAndTSumHangShu.TabIndex = 6;
            this.labPAndTSumHangShu.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(686, 412);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "总记录行数：";
            // 
            // txtFYe_TiaoZhuan
            // 
            this.txtFYe_TiaoZhuan.Location = new System.Drawing.Point(474, 410);
            this.txtFYe_TiaoZhuan.Name = "txtFYe_TiaoZhuan";
            this.txtFYe_TiaoZhuan.Size = new System.Drawing.Size(68, 21);
            this.txtFYe_TiaoZhuan.TabIndex = 5;
            // 
            // labZongYeMa
            // 
            this.labZongYeMa.AutoSize = true;
            this.labZongYeMa.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labZongYeMa.Location = new System.Drawing.Point(279, 414);
            this.labZongYeMa.Name = "labZongYeMa";
            this.labZongYeMa.Size = new System.Drawing.Size(15, 17);
            this.labZongYeMa.TabIndex = 4;
            this.labZongYeMa.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(262, 414);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "/";
            // 
            // labDangQianYeMa
            // 
            this.labDangQianYeMa.AutoSize = true;
            this.labDangQianYeMa.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labDangQianYeMa.Location = new System.Drawing.Point(245, 414);
            this.labDangQianYeMa.Name = "labDangQianYeMa";
            this.labDangQianYeMa.Size = new System.Drawing.Size(15, 17);
            this.labDangQianYeMa.TabIndex = 4;
            this.labDangQianYeMa.Text = "0";
            // 
            // butFTiaoZhuan
            // 
            this.butFTiaoZhuan.Location = new System.Drawing.Point(592, 409);
            this.butFTiaoZhuan.Name = "butFTiaoZhuan";
            this.butFTiaoZhuan.Size = new System.Drawing.Size(75, 23);
            this.butFTiaoZhuan.TabIndex = 3;
            this.butFTiaoZhuan.Text = "跳转";
            this.butFTiaoZhuan.UseVisualStyleBackColor = true;
            this.butFTiaoZhuan.Click += new System.EventHandler(this.ButFTiaoZhuan_Click);
            // 
            // butFXiaYiYe
            // 
            this.butFXiaYiYe.Location = new System.Drawing.Point(357, 409);
            this.butFXiaYiYe.Name = "butFXiaYiYe";
            this.butFXiaYiYe.Size = new System.Drawing.Size(75, 23);
            this.butFXiaYiYe.TabIndex = 3;
            this.butFXiaYiYe.Text = "下一页";
            this.butFXiaYiYe.UseVisualStyleBackColor = true;
            this.butFXiaYiYe.Click += new System.EventHandler(this.ButFXiaYiYe_Click);
            // 
            // butFShangYiYe
            // 
            this.butFShangYiYe.Location = new System.Drawing.Point(113, 409);
            this.butFShangYiYe.Name = "butFShangYiYe";
            this.butFShangYiYe.Size = new System.Drawing.Size(75, 23);
            this.butFShangYiYe.TabIndex = 3;
            this.butFShangYiYe.Text = "上一页";
            this.butFShangYiYe.UseVisualStyleBackColor = true;
            this.butFShangYiYe.Click += new System.EventHandler(this.ButFShangYiYe_Click);
            // 
            // dataVPanTian
            // 
            this.dataVPanTian.AllowUserToAddRows = false;
            this.dataVPanTian.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataVPanTian.BackgroundColor = System.Drawing.Color.White;
            this.dataVPanTian.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataVPanTian.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column12});
            this.dataVPanTian.Location = new System.Drawing.Point(25, 59);
            this.dataVPanTian.Name = "dataVPanTian";
            this.dataVPanTian.RowHeadersVisible = false;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dataVPanTian.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dataVPanTian.RowTemplate.Height = 23;
            this.dataVPanTian.Size = new System.Drawing.Size(828, 336);
            this.dataVPanTian.TabIndex = 2;
            this.dataVPanTian.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataVPanTian_CellContentClick);
            this.dataVPanTian.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataVPanTian_CellEndEdit);
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "TiMu";
            this.Column9.FillWeight = 99.49239F;
            this.Column9.HeaderText = "题目";
            this.Column9.Name = "Column9";
            // 
            // Column10
            // 
            this.Column10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column10.DataPropertyName = "ZhengQueDaAn";
            this.Column10.FillWeight = 99.49239F;
            this.Column10.HeaderText = "答案";
            this.Column10.Name = "Column10";
            this.Column10.Width = 150;
            // 
            // Column11
            // 
            this.Column11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.NullValue = "更新";
            this.Column11.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column11.FillWeight = 101.5228F;
            this.Column11.HeaderText = "更新";
            this.Column11.Name = "Column11";
            this.Column11.Width = 50;
            // 
            // Column12
            // 
            this.Column12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.NullValue = "删除";
            this.Column12.DefaultCellStyle = dataGridViewCellStyle5;
            this.Column12.FillWeight = 99.49239F;
            this.Column12.HeaderText = "删除";
            this.Column12.Name = "Column12";
            this.Column12.Width = 50;
            // 
            // comPanTian
            // 
            this.comPanTian.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comPanTian.FormattingEnabled = true;
            this.comPanTian.Location = new System.Drawing.Point(404, 22);
            this.comPanTian.Name = "comPanTian";
            this.comPanTian.Size = new System.Drawing.Size(121, 25);
            this.comPanTian.TabIndex = 1;
            this.comPanTian.SelectedIndexChanged += new System.EventHandler(this.ComPanTian_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(307, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "选择课程查看";
            // 
            // ChaKanTiMu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(236)))), ((int)(((byte)(236)))));
            this.ClientSize = new System.Drawing.Size(930, 514);
            this.Controls.Add(this.comTiXing);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panelPanTian);
            this.Controls.Add(this.panelDanDuo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ChaKanTiMu";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.ChaKanTiMu_Load);
            this.panelDanDuo.ResumeLayout(false);
            this.panelDanDuo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataVDanAndDuo)).EndInit();
            this.panelPanTian.ResumeLayout(false);
            this.panelPanTian.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataVPanTian)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox comTiXing;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panelDanDuo;
        private System.Windows.Forms.DataGridView dataVDanAndDuo;
        private System.Windows.Forms.ComboBox comDanKeCheng;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panelPanTian;
        private System.Windows.Forms.DataGridView dataVPanTian;
        private System.Windows.Forms.ComboBox comPanTian;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button butFTiaoZhuan;
        private System.Windows.Forms.Button butFXiaYiYe;
        private System.Windows.Forms.Button butFShangYiYe;
        private System.Windows.Forms.TextBox txtFYe_TiaoZhuan;
        private System.Windows.Forms.Label labZongYeMa;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labDangQianYeMa;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labXuanFenZong;
        private System.Windows.Forms.Label labXuanFenIndex;
        private System.Windows.Forms.TextBox txtXuanFenTiao;
        private System.Windows.Forms.Button butXuanTiaoZhuan;
        private System.Windows.Forms.Button butXuanFenXiaYi;
        private System.Windows.Forms.Button butXuanFenShang;
        private System.Windows.Forms.Label labXuanZeTiSumHangShu;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labPAndTSumHangShu;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewButtonColumn Column7;
        private System.Windows.Forms.DataGridViewButtonColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewButtonColumn Column11;
        private System.Windows.Forms.DataGridViewButtonColumn Column12;
    }
}