﻿namespace KaoShiXiTong
{
    partial class TianJiaTiMu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.combTiXing = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.radDanDaAnA = new System.Windows.Forms.RadioButton();
            this.radDanDaAnD = new System.Windows.Forms.RadioButton();
            this.richtxtDanXuanTiMu = new System.Windows.Forms.RichTextBox();
            this.radDanDaAnB = new System.Windows.Forms.RadioButton();
            this.label15 = new System.Windows.Forms.Label();
            this.radDanDaAnC = new System.Windows.Forms.RadioButton();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.richtxtDuoXD = new System.Windows.Forms.RichTextBox();
            this.richtxtDuoXC = new System.Windows.Forms.RichTextBox();
            this.richtxtDuoXB = new System.Windows.Forms.RichTextBox();
            this.richtxtDuoXA = new System.Windows.Forms.RichTextBox();
            this.butQingKongDan = new System.Windows.Forms.Button();
            this.butTianJiaDan = new System.Windows.Forms.Button();
            this.comKeChengDanXuan = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.butPanDuanQingKong = new System.Windows.Forms.Button();
            this.butPanDuanTianJia = new System.Windows.Forms.Button();
            this.radPanDuanCuo = new System.Windows.Forms.RadioButton();
            this.radPanDuanDui = new System.Windows.Forms.RadioButton();
            this.richtxtPanDuanTiMu = new System.Windows.Forms.RichTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comKeChengPanDuan = new System.Windows.Forms.ComboBox();
            this.comTianKongKeCheng = new System.Windows.Forms.ComboBox();
            this.richtxtTianKongDaAn = new System.Windows.Forms.RichTextBox();
            this.butTianKongDelete = new System.Windows.Forms.Button();
            this.butTianKongInsert = new System.Windows.Forms.Button();
            this.richtxtTianKongTiMu = new System.Windows.Forms.RichTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.butKuaisuDaoRu1 = new System.Windows.Forms.Button();
            this.tabControlDanPanTian = new System.Windows.Forms.TabControl();
            this.tabDanXuan = new System.Windows.Forms.TabPage();
            this.tabTianKong = new System.Windows.Forms.TabPage();
            this.butTabDaoRuTian = new System.Windows.Forms.Button();
            this.tabPanDuan = new System.Windows.Forms.TabPage();
            this.butTabDaoRuPan = new System.Windows.Forms.Button();
            this.tabControlDanPanTian.SuspendLayout();
            this.tabDanXuan.SuspendLayout();
            this.tabTianKong.SuspendLayout();
            this.tabPanDuan.SuspendLayout();
            this.SuspendLayout();
            // 
            // combTiXing
            // 
            this.combTiXing.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.combTiXing.FormattingEnabled = true;
            this.combTiXing.Location = new System.Drawing.Point(323, 12);
            this.combTiXing.Name = "combTiXing";
            this.combTiXing.Size = new System.Drawing.Size(121, 25);
            this.combTiXing.TabIndex = 1;
            this.combTiXing.SelectedIndexChanged += new System.EventHandler(this.CombTiXing_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(249, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "选择题型";
            // 
            // radDanDaAnA
            // 
            this.radDanDaAnA.AutoSize = true;
            this.radDanDaAnA.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.radDanDaAnA.Location = new System.Drawing.Point(507, 218);
            this.radDanDaAnA.Name = "radDanDaAnA";
            this.radDanDaAnA.Size = new System.Drawing.Size(74, 21);
            this.radDanDaAnA.TabIndex = 27;
            this.radDanDaAnA.TabStop = true;
            this.radDanDaAnA.Text = "正确答案";
            this.radDanDaAnA.UseVisualStyleBackColor = true;
            // 
            // radDanDaAnD
            // 
            this.radDanDaAnD.AutoSize = true;
            this.radDanDaAnD.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.radDanDaAnD.Location = new System.Drawing.Point(507, 359);
            this.radDanDaAnD.Name = "radDanDaAnD";
            this.radDanDaAnD.Size = new System.Drawing.Size(74, 21);
            this.radDanDaAnD.TabIndex = 24;
            this.radDanDaAnD.TabStop = true;
            this.radDanDaAnD.Text = "正确答案";
            this.radDanDaAnD.UseVisualStyleBackColor = true;
            // 
            // richtxtDanXuanTiMu
            // 
            this.richtxtDanXuanTiMu.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.richtxtDanXuanTiMu.Location = new System.Drawing.Point(229, 47);
            this.richtxtDanXuanTiMu.Name = "richtxtDanXuanTiMu";
            this.richtxtDanXuanTiMu.Size = new System.Drawing.Size(261, 150);
            this.richtxtDanXuanTiMu.TabIndex = 31;
            this.richtxtDanXuanTiMu.Text = "";
            // 
            // radDanDaAnB
            // 
            this.radDanDaAnB.AutoSize = true;
            this.radDanDaAnB.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.radDanDaAnB.Location = new System.Drawing.Point(507, 262);
            this.radDanDaAnB.Name = "radDanDaAnB";
            this.radDanDaAnB.Size = new System.Drawing.Size(74, 21);
            this.radDanDaAnB.TabIndex = 26;
            this.radDanDaAnB.TabStop = true;
            this.radDanDaAnB.Text = "正确答案";
            this.radDanDaAnB.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label15.Location = new System.Drawing.Point(171, 47);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(44, 17);
            this.label15.TabIndex = 30;
            this.label15.Text = "题目：";
            // 
            // radDanDaAnC
            // 
            this.radDanDaAnC.AutoSize = true;
            this.radDanDaAnC.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.radDanDaAnC.Location = new System.Drawing.Point(507, 309);
            this.radDanDaAnC.Name = "radDanDaAnC";
            this.radDanDaAnC.Size = new System.Drawing.Size(74, 21);
            this.radDanDaAnC.TabIndex = 25;
            this.radDanDaAnC.TabStop = true;
            this.radDanDaAnC.Text = "正确答案";
            this.radDanDaAnC.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label14.Location = new System.Drawing.Point(189, 360);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 17);
            this.label14.TabIndex = 7;
            this.label14.Text = "D";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label13.Location = new System.Drawing.Point(189, 311);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(16, 17);
            this.label13.TabIndex = 7;
            this.label13.Text = "C";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label12.Location = new System.Drawing.Point(189, 265);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(16, 17);
            this.label12.TabIndex = 7;
            this.label12.Text = "B";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label11.Location = new System.Drawing.Point(189, 221);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(16, 17);
            this.label11.TabIndex = 7;
            this.label11.Text = "A";
            // 
            // richtxtDuoXD
            // 
            this.richtxtDuoXD.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.richtxtDuoXD.Location = new System.Drawing.Point(229, 352);
            this.richtxtDuoXD.Name = "richtxtDuoXD";
            this.richtxtDuoXD.Size = new System.Drawing.Size(261, 35);
            this.richtxtDuoXD.TabIndex = 6;
            this.richtxtDuoXD.Text = "";
            // 
            // richtxtDuoXC
            // 
            this.richtxtDuoXC.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.richtxtDuoXC.Location = new System.Drawing.Point(229, 303);
            this.richtxtDuoXC.Name = "richtxtDuoXC";
            this.richtxtDuoXC.Size = new System.Drawing.Size(261, 35);
            this.richtxtDuoXC.TabIndex = 6;
            this.richtxtDuoXC.Text = "";
            // 
            // richtxtDuoXB
            // 
            this.richtxtDuoXB.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.richtxtDuoXB.Location = new System.Drawing.Point(229, 256);
            this.richtxtDuoXB.Name = "richtxtDuoXB";
            this.richtxtDuoXB.Size = new System.Drawing.Size(261, 35);
            this.richtxtDuoXB.TabIndex = 6;
            this.richtxtDuoXB.Text = "";
            // 
            // richtxtDuoXA
            // 
            this.richtxtDuoXA.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.richtxtDuoXA.Location = new System.Drawing.Point(229, 212);
            this.richtxtDuoXA.Name = "richtxtDuoXA";
            this.richtxtDuoXA.Size = new System.Drawing.Size(261, 35);
            this.richtxtDuoXA.TabIndex = 6;
            this.richtxtDuoXA.Text = "";
            // 
            // butQingKongDan
            // 
            this.butQingKongDan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butQingKongDan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butQingKongDan.FlatAppearance.BorderSize = 0;
            this.butQingKongDan.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.butQingKongDan.Location = new System.Drawing.Point(506, 423);
            this.butQingKongDan.Name = "butQingKongDan";
            this.butQingKongDan.Size = new System.Drawing.Size(75, 23);
            this.butQingKongDan.TabIndex = 5;
            this.butQingKongDan.Text = "清空";
            this.butQingKongDan.UseVisualStyleBackColor = true;
            this.butQingKongDan.Click += new System.EventHandler(this.ButQingKongDan_Click);
            // 
            // butTianJiaDan
            // 
            this.butTianJiaDan.BackColor = System.Drawing.Color.Gainsboro;
            this.butTianJiaDan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butTianJiaDan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butTianJiaDan.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.butTianJiaDan.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.butTianJiaDan.Location = new System.Drawing.Point(229, 423);
            this.butTianJiaDan.Name = "butTianJiaDan";
            this.butTianJiaDan.Size = new System.Drawing.Size(75, 23);
            this.butTianJiaDan.TabIndex = 5;
            this.butTianJiaDan.Text = "添加";
            this.butTianJiaDan.UseVisualStyleBackColor = false;
            this.butTianJiaDan.Click += new System.EventHandler(this.ButTianJiaDan_Click);
            // 
            // comKeChengDanXuan
            // 
            this.comKeChengDanXuan.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comKeChengDanXuan.FormattingEnabled = true;
            this.comKeChengDanXuan.Location = new System.Drawing.Point(301, 6);
            this.comKeChengDanXuan.Name = "comKeChengDanXuan";
            this.comKeChengDanXuan.Size = new System.Drawing.Size(121, 25);
            this.comKeChengDanXuan.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(167, 198);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 17);
            this.label4.TabIndex = 2;
            this.label4.Text = "答案：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(230, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "选择课程";
            // 
            // butPanDuanQingKong
            // 
            this.butPanDuanQingKong.BackColor = System.Drawing.Color.Gainsboro;
            this.butPanDuanQingKong.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butPanDuanQingKong.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butPanDuanQingKong.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.butPanDuanQingKong.Location = new System.Drawing.Point(434, 266);
            this.butPanDuanQingKong.Name = "butPanDuanQingKong";
            this.butPanDuanQingKong.Size = new System.Drawing.Size(75, 23);
            this.butPanDuanQingKong.TabIndex = 4;
            this.butPanDuanQingKong.Text = "清空";
            this.butPanDuanQingKong.UseVisualStyleBackColor = false;
            this.butPanDuanQingKong.Click += new System.EventHandler(this.ButPanDuanQingKong_Click);
            // 
            // butPanDuanTianJia
            // 
            this.butPanDuanTianJia.BackColor = System.Drawing.Color.Gainsboro;
            this.butPanDuanTianJia.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butPanDuanTianJia.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butPanDuanTianJia.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.butPanDuanTianJia.Location = new System.Drawing.Point(203, 266);
            this.butPanDuanTianJia.Name = "butPanDuanTianJia";
            this.butPanDuanTianJia.Size = new System.Drawing.Size(75, 23);
            this.butPanDuanTianJia.TabIndex = 4;
            this.butPanDuanTianJia.Text = "添加";
            this.butPanDuanTianJia.UseVisualStyleBackColor = false;
            this.butPanDuanTianJia.Click += new System.EventHandler(this.ButPanDuanTianJia_Click);
            // 
            // radPanDuanCuo
            // 
            this.radPanDuanCuo.AutoSize = true;
            this.radPanDuanCuo.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.radPanDuanCuo.Location = new System.Drawing.Point(302, 199);
            this.radPanDuanCuo.Name = "radPanDuanCuo";
            this.radPanDuanCuo.Size = new System.Drawing.Size(38, 21);
            this.radPanDuanCuo.TabIndex = 3;
            this.radPanDuanCuo.TabStop = true;
            this.radPanDuanCuo.Text = "错";
            this.radPanDuanCuo.UseVisualStyleBackColor = true;
            // 
            // radPanDuanDui
            // 
            this.radPanDuanDui.AutoSize = true;
            this.radPanDuanDui.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.radPanDuanDui.Location = new System.Drawing.Point(213, 199);
            this.radPanDuanDui.Name = "radPanDuanDui";
            this.radPanDuanDui.Size = new System.Drawing.Size(38, 21);
            this.radPanDuanDui.TabIndex = 3;
            this.radPanDuanDui.TabStop = true;
            this.radPanDuanDui.Text = "对";
            this.radPanDuanDui.UseVisualStyleBackColor = true;
            // 
            // richtxtPanDuanTiMu
            // 
            this.richtxtPanDuanTiMu.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.richtxtPanDuanTiMu.Location = new System.Drawing.Point(203, 53);
            this.richtxtPanDuanTiMu.Name = "richtxtPanDuanTiMu";
            this.richtxtPanDuanTiMu.Size = new System.Drawing.Size(306, 120);
            this.richtxtPanDuanTiMu.TabIndex = 2;
            this.richtxtPanDuanTiMu.Text = "";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(147, 199);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 17);
            this.label7.TabIndex = 0;
            this.label7.Text = "答案：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(147, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "题目：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(228, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "选择课程";
            // 
            // comKeChengPanDuan
            // 
            this.comKeChengPanDuan.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comKeChengPanDuan.FormattingEnabled = true;
            this.comKeChengPanDuan.Location = new System.Drawing.Point(303, 6);
            this.comKeChengPanDuan.Name = "comKeChengPanDuan";
            this.comKeChengPanDuan.Size = new System.Drawing.Size(121, 25);
            this.comKeChengPanDuan.TabIndex = 1;
            // 
            // comTianKongKeCheng
            // 
            this.comTianKongKeCheng.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comTianKongKeCheng.FormattingEnabled = true;
            this.comTianKongKeCheng.Location = new System.Drawing.Point(302, 6);
            this.comTianKongKeCheng.Name = "comTianKongKeCheng";
            this.comTianKongKeCheng.Size = new System.Drawing.Size(121, 25);
            this.comTianKongKeCheng.TabIndex = 7;
            // 
            // richtxtTianKongDaAn
            // 
            this.richtxtTianKongDaAn.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.richtxtTianKongDaAn.Location = new System.Drawing.Point(203, 205);
            this.richtxtTianKongDaAn.Name = "richtxtTianKongDaAn";
            this.richtxtTianKongDaAn.Size = new System.Drawing.Size(306, 28);
            this.richtxtTianKongDaAn.TabIndex = 11;
            this.richtxtTianKongDaAn.Text = "";
            // 
            // butTianKongDelete
            // 
            this.butTianKongDelete.BackColor = System.Drawing.Color.Gainsboro;
            this.butTianKongDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butTianKongDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butTianKongDelete.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.butTianKongDelete.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.butTianKongDelete.Location = new System.Drawing.Point(434, 270);
            this.butTianKongDelete.Name = "butTianKongDelete";
            this.butTianKongDelete.Size = new System.Drawing.Size(75, 23);
            this.butTianKongDelete.TabIndex = 10;
            this.butTianKongDelete.Text = "清空";
            this.butTianKongDelete.UseVisualStyleBackColor = false;
            this.butTianKongDelete.Click += new System.EventHandler(this.ButTianKongDelete_Click);
            // 
            // butTianKongInsert
            // 
            this.butTianKongInsert.BackColor = System.Drawing.Color.Gainsboro;
            this.butTianKongInsert.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butTianKongInsert.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butTianKongInsert.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.butTianKongInsert.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.butTianKongInsert.Location = new System.Drawing.Point(203, 270);
            this.butTianKongInsert.Name = "butTianKongInsert";
            this.butTianKongInsert.Size = new System.Drawing.Size(75, 23);
            this.butTianKongInsert.TabIndex = 10;
            this.butTianKongInsert.Text = "添加";
            this.butTianKongInsert.UseVisualStyleBackColor = false;
            this.butTianKongInsert.Click += new System.EventHandler(this.ButTianKongInsert_Click);
            // 
            // richtxtTianKongTiMu
            // 
            this.richtxtTianKongTiMu.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.richtxtTianKongTiMu.Location = new System.Drawing.Point(203, 50);
            this.richtxtTianKongTiMu.Name = "richtxtTianKongTiMu";
            this.richtxtTianKongTiMu.Size = new System.Drawing.Size(306, 120);
            this.richtxtTianKongTiMu.TabIndex = 8;
            this.richtxtTianKongTiMu.Text = "";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.Location = new System.Drawing.Point(145, 209);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 17);
            this.label10.TabIndex = 2;
            this.label10.Text = "答案：";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.Location = new System.Drawing.Point(145, 50);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 17);
            this.label9.TabIndex = 2;
            this.label9.Text = "题目：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(228, 11);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 17);
            this.label8.TabIndex = 2;
            this.label8.Text = "选择课程";
            // 
            // butKuaisuDaoRu1
            // 
            this.butKuaisuDaoRu1.BackColor = System.Drawing.Color.Gainsboro;
            this.butKuaisuDaoRu1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butKuaisuDaoRu1.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.butKuaisuDaoRu1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.butKuaisuDaoRu1.Location = new System.Drawing.Point(348, 423);
            this.butKuaisuDaoRu1.Name = "butKuaisuDaoRu1";
            this.butKuaisuDaoRu1.Size = new System.Drawing.Size(95, 23);
            this.butKuaisuDaoRu1.TabIndex = 9;
            this.butKuaisuDaoRu1.Text = "快速导入试题";
            this.butKuaisuDaoRu1.UseVisualStyleBackColor = false;
            this.butKuaisuDaoRu1.Click += new System.EventHandler(this.ButKuaisuDaoRu1_Click_1);
            // 
            // tabControlDanPanTian
            // 
            this.tabControlDanPanTian.Controls.Add(this.tabDanXuan);
            this.tabControlDanPanTian.Controls.Add(this.tabTianKong);
            this.tabControlDanPanTian.Controls.Add(this.tabPanDuan);
            this.tabControlDanPanTian.Location = new System.Drawing.Point(19, 41);
            this.tabControlDanPanTian.Name = "tabControlDanPanTian";
            this.tabControlDanPanTian.SelectedIndex = 0;
            this.tabControlDanPanTian.Size = new System.Drawing.Size(701, 510);
            this.tabControlDanPanTian.TabIndex = 10;
            this.tabControlDanPanTian.SelectedIndexChanged += new System.EventHandler(this.TabControlDanPanTian_SelectedIndexChanged);
            // 
            // tabDanXuan
            // 
            this.tabDanXuan.BackColor = System.Drawing.Color.Transparent;
            this.tabDanXuan.Controls.Add(this.radDanDaAnA);
            this.tabDanXuan.Controls.Add(this.butKuaisuDaoRu1);
            this.tabDanXuan.Controls.Add(this.radDanDaAnD);
            this.tabDanXuan.Controls.Add(this.richtxtDuoXB);
            this.tabDanXuan.Controls.Add(this.richtxtDanXuanTiMu);
            this.tabDanXuan.Controls.Add(this.label3);
            this.tabDanXuan.Controls.Add(this.radDanDaAnB);
            this.tabDanXuan.Controls.Add(this.label4);
            this.tabDanXuan.Controls.Add(this.label15);
            this.tabDanXuan.Controls.Add(this.comKeChengDanXuan);
            this.tabDanXuan.Controls.Add(this.radDanDaAnC);
            this.tabDanXuan.Controls.Add(this.butTianJiaDan);
            this.tabDanXuan.Controls.Add(this.label14);
            this.tabDanXuan.Controls.Add(this.butQingKongDan);
            this.tabDanXuan.Controls.Add(this.label13);
            this.tabDanXuan.Controls.Add(this.richtxtDuoXA);
            this.tabDanXuan.Controls.Add(this.label12);
            this.tabDanXuan.Controls.Add(this.richtxtDuoXC);
            this.tabDanXuan.Controls.Add(this.label11);
            this.tabDanXuan.Controls.Add(this.richtxtDuoXD);
            this.tabDanXuan.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabDanXuan.Location = new System.Drawing.Point(4, 22);
            this.tabDanXuan.Name = "tabDanXuan";
            this.tabDanXuan.Padding = new System.Windows.Forms.Padding(3);
            this.tabDanXuan.Size = new System.Drawing.Size(693, 484);
            this.tabDanXuan.TabIndex = 0;
            this.tabDanXuan.Text = "单选题";
            // 
            // tabTianKong
            // 
            this.tabTianKong.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(228)))), ((int)(((byte)(246)))));
            this.tabTianKong.Controls.Add(this.butTabDaoRuTian);
            this.tabTianKong.Controls.Add(this.richtxtTianKongDaAn);
            this.tabTianKong.Controls.Add(this.butTianKongDelete);
            this.tabTianKong.Controls.Add(this.butTianKongInsert);
            this.tabTianKong.Controls.Add(this.label8);
            this.tabTianKong.Controls.Add(this.richtxtTianKongTiMu);
            this.tabTianKong.Controls.Add(this.label9);
            this.tabTianKong.Controls.Add(this.comTianKongKeCheng);
            this.tabTianKong.Controls.Add(this.label10);
            this.tabTianKong.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabTianKong.Location = new System.Drawing.Point(4, 22);
            this.tabTianKong.Name = "tabTianKong";
            this.tabTianKong.Padding = new System.Windows.Forms.Padding(3);
            this.tabTianKong.Size = new System.Drawing.Size(693, 484);
            this.tabTianKong.TabIndex = 1;
            this.tabTianKong.Text = "填空题";
            // 
            // butTabDaoRuTian
            // 
            this.butTabDaoRuTian.BackColor = System.Drawing.Color.Gainsboro;
            this.butTabDaoRuTian.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butTabDaoRuTian.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butTabDaoRuTian.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.butTabDaoRuTian.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.butTabDaoRuTian.Location = new System.Drawing.Point(313, 270);
            this.butTabDaoRuTian.Name = "butTabDaoRuTian";
            this.butTabDaoRuTian.Size = new System.Drawing.Size(88, 23);
            this.butTabDaoRuTian.TabIndex = 12;
            this.butTabDaoRuTian.Text = "快速导入试题";
            this.butTabDaoRuTian.UseVisualStyleBackColor = false;
            this.butTabDaoRuTian.Click += new System.EventHandler(this.ButTabDaoRuTian_Click);
            // 
            // tabPanDuan
            // 
            this.tabPanDuan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(228)))), ((int)(((byte)(246)))));
            this.tabPanDuan.Controls.Add(this.butTabDaoRuPan);
            this.tabPanDuan.Controls.Add(this.butPanDuanQingKong);
            this.tabPanDuan.Controls.Add(this.butPanDuanTianJia);
            this.tabPanDuan.Controls.Add(this.comKeChengPanDuan);
            this.tabPanDuan.Controls.Add(this.radPanDuanCuo);
            this.tabPanDuan.Controls.Add(this.label5);
            this.tabPanDuan.Controls.Add(this.radPanDuanDui);
            this.tabPanDuan.Controls.Add(this.label6);
            this.tabPanDuan.Controls.Add(this.richtxtPanDuanTiMu);
            this.tabPanDuan.Controls.Add(this.label7);
            this.tabPanDuan.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabPanDuan.Location = new System.Drawing.Point(4, 22);
            this.tabPanDuan.Name = "tabPanDuan";
            this.tabPanDuan.Padding = new System.Windows.Forms.Padding(3);
            this.tabPanDuan.Size = new System.Drawing.Size(693, 484);
            this.tabPanDuan.TabIndex = 2;
            this.tabPanDuan.Text = "判断题";
            // 
            // butTabDaoRuPan
            // 
            this.butTabDaoRuPan.BackColor = System.Drawing.Color.Gainsboro;
            this.butTabDaoRuPan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butTabDaoRuPan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butTabDaoRuPan.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.butTabDaoRuPan.Location = new System.Drawing.Point(312, 266);
            this.butTabDaoRuPan.Name = "butTabDaoRuPan";
            this.butTabDaoRuPan.Size = new System.Drawing.Size(90, 23);
            this.butTabDaoRuPan.TabIndex = 5;
            this.butTabDaoRuPan.Text = "快速导入试题";
            this.butTabDaoRuPan.UseVisualStyleBackColor = false;
            this.butTabDaoRuPan.Click += new System.EventHandler(this.ButTabDaoRuPan_Click);
            // 
            // TianJiaTiMu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(236)))), ((int)(((byte)(236)))));
            this.ClientSize = new System.Drawing.Size(734, 558);
            this.Controls.Add(this.tabControlDanPanTian);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.combTiXing);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "TianJiaTiMu";
            this.Text = "TianJiaZhuanYeXinXi";
            this.Load += new System.EventHandler(this.TianJiaTiMu_Load);
            this.tabControlDanPanTian.ResumeLayout(false);
            this.tabDanXuan.ResumeLayout(false);
            this.tabDanXuan.PerformLayout();
            this.tabTianKong.ResumeLayout(false);
            this.tabTianKong.PerformLayout();
            this.tabPanDuan.ResumeLayout(false);
            this.tabPanDuan.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox combTiXing;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comKeChengDanXuan;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button butQingKongDan;
        private System.Windows.Forms.Button butTianJiaDan;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RichTextBox richtxtPanDuanTiMu;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comKeChengPanDuan;
        private System.Windows.Forms.RadioButton radPanDuanCuo;
        private System.Windows.Forms.RadioButton radPanDuanDui;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comTianKongKeCheng;
        private System.Windows.Forms.RichTextBox richtxtTianKongTiMu;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button butTianKongDelete;
        private System.Windows.Forms.Button butTianKongInsert;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.RichTextBox richtxtDuoXD;
        private System.Windows.Forms.RichTextBox richtxtDuoXC;
        private System.Windows.Forms.RichTextBox richtxtDuoXB;
        private System.Windows.Forms.RichTextBox richtxtDuoXA;
        private System.Windows.Forms.RadioButton radDanDaAnA;
        private System.Windows.Forms.RadioButton radDanDaAnD;
        private System.Windows.Forms.RadioButton radDanDaAnB;
        private System.Windows.Forms.RadioButton radDanDaAnC;
        private System.Windows.Forms.RichTextBox richtxtDanXuanTiMu;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.RichTextBox richtxtTianKongDaAn;
        private System.Windows.Forms.Button butPanDuanQingKong;
        private System.Windows.Forms.Button butPanDuanTianJia;
        private System.Windows.Forms.Button butKuaisuDaoRu1;
        private System.Windows.Forms.TabControl tabControlDanPanTian;
        private System.Windows.Forms.TabPage tabDanXuan;
        private System.Windows.Forms.TabPage tabTianKong;
        private System.Windows.Forms.TabPage tabPanDuan;
        private System.Windows.Forms.Button butTabDaoRuTian;
        private System.Windows.Forms.Button butTabDaoRuPan;
    }
}