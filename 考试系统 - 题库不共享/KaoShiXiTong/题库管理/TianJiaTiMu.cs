﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;
using System.Data.SqlClient;
using CCWin;

namespace KaoShiXiTong
{
    public partial class TianJiaTiMu : Form
    {
        public TianJiaTiMu()
        {
            InitializeComponent();
        }

        private void TianJiaTiMu_Load(object sender, EventArgs e)
        {
            string SqlTiXing = "select * from TiXing";
            DataTable TiXingTa = DBHepler.Cha(SqlTiXing);
            combTiXing.DataSource = TiXingTa;
            combTiXing.ValueMember = "TiXingID";
            combTiXing.DisplayMember = "TiXingName";


            ComKeChengDanXuan();

            ComKeChengTianKong();

            ComKeChengPanDuan();
        }
        private int TiXingID()
        {
            int ComTiXingID = (int)combTiXing.SelectedValue;
            return ComTiXingID;
        }

        /// <summary>
        /// 添加课程到单选题下拉框中
        /// </summary>
        void ComKeChengDanXuan()
        {
            string SqlKeCheng = string.Format(@"select * from KeCheng where UserID={0}", UserIDandUserName.UserID);
            DataTable KeChengTa = DBHepler.Cha(SqlKeCheng);
            comKeChengDanXuan.DataSource = KeChengTa;
            comKeChengDanXuan.ValueMember = "KeChengID";
            comKeChengDanXuan.DisplayMember = "KeChengName";
        }
        int ComTiXingIDDan = 0;
        int ComKeChengIDDan = 0;

        void ComDanTiXingKeChengID()
        {
            ComTiXingIDDan = TiXingID();
            ComKeChengIDDan = (int)comKeChengDanXuan.SelectedValue;
        }

        private void ButTianJiaDan_Click(object sender, EventArgs e)
        {
            ComDanTiXingKeChengID();

            string DanXuanTiMu = richtxtDanXuanTiMu.Text;
            string DanXuanA = "A." + richtxtDuoXA.Text;
            string DanXuanB = "B." + richtxtDuoXB.Text;
            string DanXuanC = "C." + richtxtDuoXC.Text;
            string DanXuanD = "D." + richtxtDuoXD.Text;
            string DanXuanDaAn = "";
            if (radDanDaAnA.Checked == true)
            {
                DanXuanDaAn = "A";
            }
            if (radDanDaAnB.Checked == true)
            {
                DanXuanDaAn = "B";
            }
            if (radDanDaAnC.Checked == true)
            {
                DanXuanDaAn = "C";
            }
            if (radDanDaAnD.Checked == true)
            {
                DanXuanDaAn = "D";
            }

            if (DanXuanTiMu != "")
            {
                if (DanXuanA != "")
                {
                    if (DanXuanB != "")
                    {
                        if (DanXuanC != "")
                        {
                            if (DanXuanD != "")
                            {
                                if (DanXuanDaAn != "")
                                {
                                    string SqlInsertDanXuanTi = string.Format(@"insert into DanXuanTi 
                                    values('{0}','{1}','{2}','{3}','{4}','{5}',{6},{7},{8})",
                                    DanXuanTiMu, DanXuanA, DanXuanB, DanXuanC, DanXuanD, DanXuanDaAn,
                                    ComKeChengIDDan, ComTiXingIDDan, UserIDandUserName.UserID);
                                    if (DBHepler.ZSG(SqlInsertDanXuanTi))
                                    {
                                        MessageBox.Show("添加成功");
                                    }
                                    else
                                    {
                                        MessageBox.Show("添加失败");
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("正确答案不能为空");
                                }
                            }
                            else
                            {
                                MessageBox.Show("选项D不能为空");
                                richtxtDuoXD.Focus();
                            }
                        }
                        else
                        {
                            MessageBox.Show("选项C不能为空");
                            richtxtDuoXC.Focus();
                        }
                    }
                    else
                    {
                        MessageBox.Show("选项B不能为空");
                        richtxtDuoXB.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("选项A不能为空");
                    richtxtDuoXA.Focus();
                }
            }
            else
            {
                MessageBox.Show("题目不能为空");
                richtxtDanXuanTiMu.Focus();
            }
        }

        void QingKongDanXuan()
        {
            foreach (Control Ct in tabDanXuan.Controls)
            {
                if (Ct is RichTextBox)
                {
                    Ct.Text = "";
                }
            }
        }
        private void ButQingKongDan_Click(object sender, EventArgs e)
        {
            QingKongDanXuan();
            //使Tab单选题中的单选按钮不被选中
            foreach (var btn in tabDanXuan.Controls.OfType<RadioButton>().ToList())
            {
                if (btn.Name != (sender as Control).Name)
                {
                    btn.Checked = false;
                }
            }
        }

        /// <summary>
        /// 添加课程到填空题下拉框中
        /// </summary>
        void ComKeChengTianKong()
        {
            string SqlKeCheng = string.Format(@"select * from KeCheng where UserID={0}", UserIDandUserName.UserID);
            DataTable KeChengTa = DBHepler.Cha(SqlKeCheng);
            comTianKongKeCheng.DataSource = KeChengTa;
            comTianKongKeCheng.ValueMember = "KeChengID";
            comTianKongKeCheng.DisplayMember = "KeChengName";
        }
        int ComTiXingIDTian = 0;
        int ComKeChengIDTian = 0;
        void ComTianTiXingKeChengID()
        {
            ComTiXingIDTian = TiXingID();
            ComKeChengIDTian = (int)comTianKongKeCheng.SelectedValue;
        }
        private void ButTianKongInsert_Click(object sender, EventArgs e)
        {
            ComTianTiXingKeChengID();

            string TianKongTiMu = richtxtTianKongTiMu.Text;
            string TianKongDaAn = richtxtTianKongDaAn.Text;

            if (TianKongTiMu != "")
            {
                if (TianKongDaAn != "")
                {
                    string InsertTK = string.Format(@"insert into TianKongTi values('{0}','{1}',{2},{3},{4})",
                        TianKongTiMu, TianKongDaAn, ComKeChengIDTian, ComTiXingIDTian, UserIDandUserName.UserID);
                    if (DBHepler.ZSG(InsertTK))
                    {
                        MessageBox.Show("添加成功");
                    }
                    else
                    {
                        MessageBox.Show("添加失败");
                    }
                }
                else
                {
                    MessageBox.Show("请填写答案");
                    richtxtTianKongDaAn.Focus();
                }
            }
            else
            {
                MessageBox.Show("请填写题目");
                richtxtTianKongTiMu.Focus();
            }
        }

        void QingKongTianKong()
        {
            foreach (Control Ct in tabTianKong.Controls)
            {
                if (Ct is RichTextBox)
                {
                    Ct.Text = "";
                }
            }
        }
        private void ButTianKongDelete_Click(object sender, EventArgs e)
        {
            QingKongTianKong();
        }

        /// <summary>
        /// 添加课程到判断题下拉框中
        /// </summary>
        void ComKeChengPanDuan()
        {
            string SqlKeCheng = string.Format(@"select * from KeCheng where UserID={0}", UserIDandUserName.UserID);
            DataTable KeChengTa = DBHepler.Cha(SqlKeCheng);
            comKeChengPanDuan.DataSource = KeChengTa;
            comKeChengPanDuan.ValueMember = "KeChengID";
            comKeChengPanDuan.DisplayMember = "KeChengName";
        }

        int ComTiXingIDPan = 0;//下拉框题型id
        int ComKeChengIDPan = 0;//下拉框课程id
        void ComPanTiXingKeChengID()
        {
            ComTiXingIDPan = TiXingID();
            ComKeChengIDPan = (int)comKeChengPanDuan.SelectedValue;
        }
        private void ButPanDuanTianJia_Click(object sender, EventArgs e)
        {
            ComPanTiXingKeChengID();

            string PanDuanTiMu = richtxtPanDuanTiMu.Text;
            string PanDuanDaAn = "";
            if (radPanDuanDui.Checked == true)
            {
                PanDuanDaAn = "对";
            }
            if (radPanDuanCuo.Checked == true)
            {
                PanDuanDaAn = "错";
            }

            if (PanDuanTiMu != "")
            {
                if (PanDuanDaAn != "")
                {
                    string InsertPuanDuanTi = string.Format(@"insert into PanDuanTi 
                    values('{0}','{1}',{2},{3},{4})",
                    PanDuanTiMu, PanDuanDaAn, ComKeChengIDPan, ComTiXingIDPan, UserIDandUserName.UserID);
                    if (DBHepler.ZSG(InsertPuanDuanTi))
                    {
                        MessageBox.Show("添加成功");
                    }
                    else
                    {
                        MessageBox.Show("添加失败");
                    }
                }
                else
                {
                    MessageBox.Show("请选择答案");
                }
            }
            else
            {
                MessageBox.Show("请填写题目");
                richtxtPanDuanTiMu.Focus();
            }
        }

        private void CombTiXing_SelectedIndexChanged(object sender, EventArgs e)
        {
            int IndexId = combTiXing.SelectedIndex;//获取题型下拉框的索引

            if (IndexId == 0)
            {
                //QingKongDanXuan();
                //QingKongTianKong();
                //QingKongPanDuan();
                this.tabControlDanPanTian.SelectedTab = this.tabDanXuan;//如果下拉框的索引是单选题，那么就选中Tab的单选题

                ComKeChengDanXuan();
            }
            if (IndexId == 1)
            {
                //QingKongDanXuan();
                //QingKongTianKong();
                this.tabControlDanPanTian.SelectedTab = this.tabPanDuan;//................
                ComKeChengPanDuan();
            }
            if (IndexId == 2)
            {
                //QingKongDanXuan();
                //QingKongPanDuan();
                this.tabControlDanPanTian.SelectedTab = this.tabTianKong;//................
                ComKeChengTianKong();
            }
        }

        void QingKongPanDuan()
        {
            foreach (Control Ct in tabPanDuan.Controls)
            {
                if (Ct is RichTextBox)
                {
                    Ct.Text = "";
                }
            }
        }
        private void ButPanDuanQingKong_Click(object sender, EventArgs e)
        {
            QingKongPanDuan();
            foreach (var btn in tabPanDuan.Controls.OfType<RadioButton>().ToList())
            {
                if (btn.Name != (sender as Control).Name)
                {
                    btn.Checked = false;
                }
            }
        }

        /// <summary>
        /// 导入试题
        /// </summary>
        void DaoRuTiKu()
        {
            OpenFileDialog LuJing = new OpenFileDialog();//获取题型文件路径
            if (LuJing.ShowDialog() == DialogResult.OK)
            {
                string fileName = LuJing.FileName;
                DaoRuShuJuKu(fileName);
            }
        }
        private void ButKuaisuDaoRu1_Click_1(object sender, EventArgs e)
        {
            if (comKeChengDanXuan.Text.Length != 0)//判断是否存在课程
            {
                DaoRuTiKu();
            }
            else
            {
                MessageBox.Show("请先添加课程！");
            }
        }

        private void ButTabDaoRuTian_Click(object sender, EventArgs e)
        {
            if (comTianKongKeCheng.Text.Length != 0)
            {
                DaoRuTiKu();
            }
            else
            {
                MessageBox.Show("请先添加课程！");
            }
        }
        private void ButTabDaoRuPan_Click(object sender, EventArgs e)
        {
            if (comKeChengPanDuan.Text.Length != 0)
            {
                DaoRuTiKu();
            }
            else
            {
                MessageBox.Show("请先添加课程！");
            }
        }

        /// <summary>
        /// 连接Excel将Excel的数据循环导入数据库
        /// </summary>
        /// <param name="fileName"></param>
        private void DaoRuShuJuKu(string fileName)
        {

            //Excel连接字符串
            string strConn = "Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + fileName + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1';";
            OleDbDataAdapter da = new OleDbDataAdapter("select *  from [sheet1$]", strConn);//获取Excel里面的所有数据
            DataSet ds = new DataSet();
            try
            {
                DataTable dt = new DataTable();
                da.Fill(ds);//填入dataset
                dt = ds.Tables[0];//赋给单个表

                //导入单选题
                if (tabControlDanPanTian.SelectedIndex == 0)
                {
                    if (dt.Rows.Count > 0)//是否存在数据
                    {
                        DataRow DanRow = null;//定义数据行
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            DanRow = dt.Rows[i];
                            insertDanToSql(DanRow);//把Excel表中的每一行存入数据库
                            if (i == dt.Rows.Count - 1)
                            {
                                MessageBox.Show("导入成功！");
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("没有单选试题！");
                    }
                }
                else if (tabControlDanPanTian.SelectedIndex == 1)//导入填空题
                {
                    if (dt.Rows.Count > 0)//是否存在数据
                    {
                        DataRow PanRow = null;//定义数据行
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            PanRow = dt.Rows[i];
                            insertTianToSql(PanRow);//把Excel表中的每一行存入数据库
                            if (i == dt.Rows.Count - 1)
                            {
                                MessageBox.Show("导入成功！");
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("没有填空试题！");
                    }
                }
                else if (tabControlDanPanTian.SelectedIndex == 2)//导入判断题 
                {
                    if (dt.Rows.Count > 0)//是否存在数据
                    {
                        DataRow TianRow = null;//定义数据行
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            TianRow = dt.Rows[i];
                            insertPanToSql(TianRow);//把Excel表中的每一行存入数据库
                            if (i == dt.Rows.Count - 1)
                            {
                                MessageBox.Show("导入成功！");
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("没有判断试题！");
                    }
                }
            }
            catch (Exception err)
            {
                MessageBox.Show("操作失败！" + err.ToString());
            }
        }

        /// <summary>
        /// 把Excel所有单选题的数据导入数据库
        /// </summary>
        /// <param name="dr"></param>
        private void insertDanToSql(DataRow DanRow)
        {
            ComDanTiXingKeChengID();//获取单选题课程id月题型id
            //excel表中的列名和数据库中的列名一定要对应  
            string TiMuName = DanRow["TiMu"].ToString();
            string XuanXiangYi = DanRow["XuanXiangYi"].ToString();
            string XuanXiangEr = DanRow["XuanXiangEr"].ToString();
            string XuanXiangSan = DanRow["XuanXiangSan"].ToString();
            string XuanXiangSi = DanRow["XuanXiangSi"].ToString();
            string ZhengQueDaAn = DanRow["ZhengQueDaAn"].ToString();

            string InsertDanSQL = string.Format(@"insert into DanXuanTi values('{0}','{1}','{2}','{3}','{4}','{5}',{6},{7},{8})",
                TiMuName, XuanXiangYi, XuanXiangEr, XuanXiangSan, XuanXiangSi, ZhengQueDaAn,
                ComKeChengIDDan, ComTiXingIDDan, UserIDandUserName.UserID);
            try
            {
                DBHepler.ZSG(InsertDanSQL);//如果题目或者答案出现sql语句并且存在单引号，会抛异常那么就把单引号转换为双引号并保存
            }
            catch
            {
                string TiMuName_2 = DanRow["TiMu"].ToString().Replace("'", "\"");
                string XuanXiangYi_2 = DanRow["XuanXiangYi"].ToString().Replace("'", "\"");
                string XuanXiangEr_2 = DanRow["XuanXiangEr"].ToString().Replace("'", "\"");
                string XuanXiangSan_2 = DanRow["XuanXiangSan"].ToString().Replace("'", "\"");
                string XuanXiangSi_2 = DanRow["XuanXiangSi"].ToString().Replace("'", "\"");
                string ZhengQueDaAn_2 = DanRow["ZhengQueDaAn"].ToString().Replace("'", "\"");

                string InsertDanSQL_2 = string.Format(@"insert into DanXuanTi values('{0}','{1}','{2}','{3}','{4}','{5}',{6},{7},{8})",
                    TiMuName_2, XuanXiangYi_2, XuanXiangEr_2, XuanXiangSan_2, XuanXiangSi_2,
                    ZhengQueDaAn_2, ComKeChengIDDan, ComTiXingIDDan, UserIDandUserName.UserID);
                DBHepler.ZSG(InsertDanSQL_2);
            }
        }
        /// <summary>
        /// Excel判断题导入数据库
        /// </summary>
        /// <param name="PanRow"></param>
        private void insertPanToSql(DataRow PanRow)
        {
            ComPanTiXingKeChengID();//获取课程id，题型id
            string TiMuName = PanRow["TiMu"].ToString();
            string ZhengQueDaAn = PanRow["ZhengQueDaAn"].ToString();
            string InsertPanSQL = string.Format(@"insert into PanDuanTi values('{0}','{1}',{2},{3},{4})",
                TiMuName, ZhengQueDaAn, ComKeChengIDPan, ComTiXingIDPan, UserIDandUserName.UserID);
            DBHepler.ZSG(InsertPanSQL);
        }

        /// <summary>
        /// Excel填空题导入数据库
        /// </summary>
        /// <param name="PanRow"></param>
        private void insertTianToSql(DataRow PanRow)
        {
            ComTianTiXingKeChengID();//获取课程id，题型id
            string TiMuName = PanRow["TiMu"].ToString();
            string ZhengQueDaAn = PanRow["ZhengQueDaAn"].ToString();
            string InsertTianSQL = string.Format(@"insert into TianKongTi values('{0}','{1}',{2},{3},{4})",
                TiMuName, ZhengQueDaAn, ComKeChengIDTian, ComTiXingIDTian,UserIDandUserName.UserID);
            DBHepler.ZSG(InsertTianSQL);
        }

        private void TabControlDanPanTian_SelectedIndexChanged(object sender, EventArgs e)
        {
            int TabIndex = tabControlDanPanTian.SelectedIndex;//获取Tab控件的索引
            if (TabIndex == 0)//如果是单选题那么就选中题型下拉框的单选题
            {
                combTiXing.SelectedIndex = 0;
            }
            else if (TabIndex == 1)//如果是填空题那么就选中题型下拉框的填空题
            {
                combTiXing.SelectedIndex = 2;
            }
            else if (TabIndex == 2)//如果是判断题那么就选中题型下拉框的判断题
            {
                combTiXing.SelectedIndex = 1;
            }
        }
    }
}
