﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

   public  class ExportExcelHelper
    {
       #region 导出excel

       public static void ExportExcel(string fileName, DataGridView myDGV,bool isShowDialog)
       {
           string saveFileName = "";
           if (isShowDialog)
           {
               //bool fileSaved = false;
               SaveFileDialog saveDialog = new SaveFileDialog();
               saveDialog.DefaultExt = "xls";
               saveDialog.Filter = "Excel文件|*.xls";
               saveDialog.FileName = fileName;
               saveDialog.ShowDialog();
               saveFileName = saveDialog.FileName;
               if (saveFileName.IndexOf(":") < 0) return; //被点了取消 
           }
           else
           {
              // saveFileName = Application.StartupPath + @"\导出记录\" + fileName + ".xls";
               saveFileName = fileName;
           }
           Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
           if (xlApp == null)
           {
               MessageBox.Show("无法创建Excel对象，可能您的机子未安装Excel");
               return;
           }

           Microsoft.Office.Interop.Excel.Workbooks workbooks = xlApp.Workbooks;
           Microsoft.Office.Interop.Excel.Workbook workbook = workbooks.Add(Microsoft.Office.Interop.Excel.XlWBATemplate.xlWBATWorksheet);
           Microsoft.Office.Interop.Excel.Worksheet worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Worksheets[1];//取得sheet1 

           //写入标题
           for (int i = 0; i < myDGV.ColumnCount; i++)
           {
               worksheet.Cells[1, i + 1] = myDGV.Columns[i].HeaderText;
           }
           //写入数值
           for (int r = 0; r < myDGV.Rows.Count; r++)
           {
               for (int i = 0; i < myDGV.ColumnCount; i++)
               {
                   if (myDGV[i, r].ValueType == typeof(string)
                      || myDGV[i, r].ValueType == typeof(DateTime))//这里就是验证DataGridView单元格中的类型,如果是string或是DataTime类型,则在放入缓 存时在该内容前加入" ";
                   {
                       worksheet.Cells[r + 2, i + 1] = "'" + myDGV.Rows[r].Cells[i].Value;
                   }
                   else
                   {
                       worksheet.Cells[r + 2, i + 1] = myDGV.Rows[r].Cells[i].Value;
                   }
               }
               System.Windows.Forms.Application.DoEvents();
           }
           worksheet.Columns.EntireColumn.AutoFit();//列宽自适应
           //if (Microsoft.Office.Interop.cmbxType.Text != "Notification")
           //{
           //    Excel.Range rg = worksheet.get_Range(worksheet.Cells[2, 2], worksheet.Cells[ds.Tables[0].Rows.Count + 1, 2]);
           //    rg.NumberFormat = "00000000";
           //}

           if (saveFileName != "")
           {
               try
               {
                   workbook.Saved = true;
                   workbook.SaveCopyAs(saveFileName);
                   //fileSaved = true;
               }
               catch (Exception ex)
               {
                   //fileSaved = false;
                   MessageBox.Show("导出文件时出错,文件可能正被打开！\n" + ex.Message);
               }

           }
           //else
           //{
           //    fileSaved = false;
           //}
           xlApp.Quit();
           GC.Collect();//强行销毁 
           // if (fileSaved && System.IO.File.Exists(saveFileName)) System.Diagnostics.Process.Start(saveFileName); //打开EXCEL
           MessageBox.Show(fileName + "保存成功", "提示", MessageBoxButtons.OK);
       }

       #endregion

 
    }

