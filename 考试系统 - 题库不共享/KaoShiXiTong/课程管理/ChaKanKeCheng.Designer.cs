﻿namespace KaoShiXiTong
{
    partial class ChaKanKeCheng
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataKeCheng = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataKeCheng)).BeginInit();
            this.SuspendLayout();
            // 
            // dataKeCheng
            // 
            this.dataKeCheng.AllowUserToAddRows = false;
            this.dataKeCheng.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataKeCheng.BackgroundColor = System.Drawing.Color.White;
            this.dataKeCheng.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dataKeCheng.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataKeCheng.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.dataKeCheng.GridColor = System.Drawing.Color.White;
            this.dataKeCheng.Location = new System.Drawing.Point(12, 12);
            this.dataKeCheng.Name = "dataKeCheng";
            this.dataKeCheng.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataKeCheng.RowHeadersVisible = false;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dataKeCheng.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dataKeCheng.RowTemplate.Height = 23;
            this.dataKeCheng.Size = new System.Drawing.Size(624, 333);
            this.dataKeCheng.TabIndex = 0;
            this.dataKeCheng.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataKeCheng_CellContentClick);
            this.dataKeCheng.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataKeCheng_CellEndEdit);
            this.dataKeCheng.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.DataKeCheng_DataBindingComplete);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "KeChengName";
            this.Column1.FillWeight = 206.1124F;
            this.Column1.HeaderText = "课程名称";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.NullValue = "修改";
            this.Column2.DefaultCellStyle = dataGridViewCellStyle1;
            this.Column2.FillWeight = 45.68528F;
            this.Column2.HeaderText = "修改";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.NullValue = "删除";
            this.Column3.DefaultCellStyle = dataGridViewCellStyle2;
            this.Column3.FillWeight = 48.20235F;
            this.Column3.HeaderText = "删除";
            this.Column3.Name = "Column3";
            // 
            // ChaKanKeCheng
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(236)))), ((int)(((byte)(236)))));
            this.ClientSize = new System.Drawing.Size(649, 361);
            this.Controls.Add(this.dataKeCheng);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ChaKanKeCheng";
            this.Text = "ChaKanKeCheng";
            this.Load += new System.EventHandler(this.ChaKanKeCheng_Load);
            this.MouseEnter += new System.EventHandler(this.ChaKanKeCheng_MouseEnter);
            ((System.ComponentModel.ISupportInitialize)(this.dataKeCheng)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dataKeCheng;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewButtonColumn Column2;
        private System.Windows.Forms.DataGridViewButtonColumn Column3;
    }
}