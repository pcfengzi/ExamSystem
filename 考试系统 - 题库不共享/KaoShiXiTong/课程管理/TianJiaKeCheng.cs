﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KaoShiXiTong
{
    public partial class TianJiaKeCheng : Form
    {
        public TianJiaKeCheng()
        {
            InitializeComponent();
        }

        private void ButTianJia_Click(object sender, EventArgs e)
        {
            string KeChengName = txtKeChengName.Text;
          
            if (KeChengName != "")
            {
                if (KeCheng_T.KCNameCount(KeChengName) ==0)
                {
                    if (KeCheng_T.TianJiaKeCheng(KeChengName))
                    {
                        MessageBox.Show("添加成功");
                    }
                    else
                    {
                        MessageBox.Show("添加失败");
                    }
                }
                else
                {
                    MessageBox.Show("该课程已存在无需再次添加");
                    txtKeChengName.Focus();
                }
            }
            else
            {
                MessageBox.Show("请输入课程名称");
                txtKeChengName.Focus();
            }
        }

        private void ButQuXiao_Click(object sender, EventArgs e)
        {
            txtKeChengName.Text = "";
            txtKeChengName.Focus();
        }
    }
}
