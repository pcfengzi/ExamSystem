﻿namespace KaoShiXiTong
{
    partial class TianJiaKeCheng
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtKeChengName = new System.Windows.Forms.TextBox();
            this.butTianJia = new System.Windows.Forms.Button();
            this.butQuXiao = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(327, 124);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "填写课程名称";
            // 
            // txtKeChengName
            // 
            this.txtKeChengName.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtKeChengName.Location = new System.Drawing.Point(426, 119);
            this.txtKeChengName.Name = "txtKeChengName";
            this.txtKeChengName.Size = new System.Drawing.Size(125, 23);
            this.txtKeChengName.TabIndex = 1;
            // 
            // butTianJia
            // 
            this.butTianJia.BackColor = System.Drawing.Color.Gainsboro;
            this.butTianJia.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butTianJia.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butTianJia.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro;
            this.butTianJia.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.butTianJia.Location = new System.Drawing.Point(334, 192);
            this.butTianJia.Name = "butTianJia";
            this.butTianJia.Size = new System.Drawing.Size(75, 23);
            this.butTianJia.TabIndex = 2;
            this.butTianJia.Text = "添加";
            this.butTianJia.UseVisualStyleBackColor = false;
            this.butTianJia.Click += new System.EventHandler(this.ButTianJia_Click);
            // 
            // butQuXiao
            // 
            this.butQuXiao.BackColor = System.Drawing.Color.Gainsboro;
            this.butQuXiao.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butQuXiao.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butQuXiao.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro;
            this.butQuXiao.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.butQuXiao.Location = new System.Drawing.Point(481, 192);
            this.butQuXiao.Name = "butQuXiao";
            this.butQuXiao.Size = new System.Drawing.Size(75, 23);
            this.butQuXiao.TabIndex = 2;
            this.butQuXiao.Text = "取消";
            this.butQuXiao.UseVisualStyleBackColor = false;
            this.butQuXiao.Click += new System.EventHandler(this.ButQuXiao_Click);
            // 
            // TianJiaKeCheng
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(236)))), ((int)(((byte)(236)))));
            this.ClientSize = new System.Drawing.Size(878, 503);
            this.Controls.Add(this.butQuXiao);
            this.Controls.Add(this.butTianJia);
            this.Controls.Add(this.txtKeChengName);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "TianJiaKeCheng";
            this.Text = "TianJiaKeCheng";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtKeChengName;
        private System.Windows.Forms.Button butTianJia;
        private System.Windows.Forms.Button butQuXiao;
    }
}