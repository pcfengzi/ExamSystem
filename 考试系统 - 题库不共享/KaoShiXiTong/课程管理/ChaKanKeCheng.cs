﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KaoShiXiTong
{
    public partial class ChaKanKeCheng : Form
    {
        public ChaKanKeCheng()
        {
            InitializeComponent();
        }

        private void DataKeCheng_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataRow row = (dataKeCheng.CurrentRow.DataBoundItem as DataRowView).Row;
            KeChengName = row["KeChengName"].ToString();
            try
            {
                int KeChengID = ChaID();
                int Index = e.ColumnIndex;
                if (Index == 2)
                {
                    if (KeCheng_T.DeleteKeChengName(KeChengID))
                    {
                        MessageBox.Show("删除成功");
                        ChaData();
                    }
                    else
                    {
                        MessageBox.Show("删除失败");
                    }
                }

                if (Index == 1)
                {
                    if (KeChengName == "")
                    {
                        MessageBox.Show("课程不能为空");
                        return;
                    }
                    if (KeCheng_T.KCNameCount(KeChengName) == 0)
                    {

                        if (KeCheng_T.UpdateKeChengName(KeChengName, KeChengID))
                        {
                            MessageBox.Show("修改成功");
                        }
                        else
                        {
                            MessageBox.Show("修改失败");
                        }
                    }
                    else
                    {
                        MessageBox.Show("课程名称不允许相同");
                    }
                }
            }
            catch
            {
            }
        }

        int ChaID()
        {
            DataRow Row = (dataKeCheng.CurrentRow.DataBoundItem as DataRowView).Row;
            int KeChengID = (int)Row["KeChengID"];
            return KeChengID;
        }

        /// <summary>
        /// 查询表信息
        /// </summary>
        void ChaData()
        {
            string SqlCha = string.Format(@"select * from KeCheng where UserID={0}",UserIDandUserName.UserID);
            DataTable Ta = DBHepler.Cha(SqlCha);
            dataKeCheng.AutoGenerateColumns = false;
            dataKeCheng.DataSource = Ta;

            dataKeCheng.AllowUserToAddRows = false;//不显示空白行
        }
        private void ChaKanKeCheng_Load(object sender, EventArgs e)
        {
            ChaData();
        }

        private void DataKeCheng_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            this.dataKeCheng.ClearSelection();//取消默认选中的行
        }

        string KeChengName;
        private void DataKeCheng_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataRow row = (dataKeCheng.CurrentRow.DataBoundItem as DataRowView).Row;
            KeChengName = row["KeChengName"].ToString();
        }

        private void ChaKanKeCheng_MouseEnter(object sender, EventArgs e)
        {
            ChaData();
        }
    }
}
