﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace KaoShiXiTong
{
    public static class DBHepler
    {
        public static string SqlConn = "Data Source=.;Initial Catalog=KaoShiXiTong;Integrated Security=True";

        public static bool ZSG(string Sql)
        {
            SqlConnection Conn = new SqlConnection(SqlConn);
            Conn.Open();
            SqlCommand Cmd = new SqlCommand(Sql,Conn);
            int Count = Cmd.ExecuteNonQuery();
            Conn.Close();
            return Count > 0;
        }

        public static DataTable Cha(string Sql)
        {
            SqlConnection Conn = new SqlConnection(SqlConn);
            SqlDataAdapter Data = new SqlDataAdapter(Sql,Conn);
            DataTable Ta = new DataTable();
            Data.Fill(Ta);
            return Ta;
        }
        public static object JuHe(string Sql)
        {
            SqlConnection Conn = new SqlConnection(SqlConn);
            Conn.Open();
            SqlCommand Cmd = new SqlCommand(Sql,Conn);
            object Count = Cmd.ExecuteScalar();
            return Count;
        }
    }
}
