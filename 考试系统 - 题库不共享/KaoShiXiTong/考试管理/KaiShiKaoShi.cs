﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KaoShiXiTong
{
    public partial class KaiShiKaoShi : Form
    {
        public KaiShiKaoShi()
        {
            InitializeComponent();
        }
        private void KaiShiKaoShi_Load(object sender, EventArgs e)
        {
            panelKaoShiJieMian.Visible = false;

            ShiJuanShuaXin();//查找试卷
            KeChengShuaXin();//查找课程

        }
        void ShiJuanShuaXin()
        {
            string ShiJuanNameSql = string.Format(@"select * from ShiJuanName" /*where UserID={0}", UserIDandUserName.UserID*/);
            DataTable Ta = DBHepler.Cha(ShiJuanNameSql);
            comShiJuanName.DataSource = Ta;
            comShiJuanName.ValueMember = "ShiJuanID";
            comShiJuanName.DisplayMember = "ShiJuanName";
        }
        void KeChengShuaXin()
        {
            string ShiJuanNameSql = string.Format(@"select * from KeCheng"/*where UserID={0}", UserIDandUserName.UserID*/);
            DataTable Ta = DBHepler.Cha(ShiJuanNameSql);
            comKeCheng.DataSource = Ta;
            comKeCheng.ValueMember = "KeChengID";
            comKeCheng.DisplayMember = "KeChengName";
        }

        int DanID = 0;
        int PanID = 0;
        int TianID = 0;
        string ShiJuanName;
        string[] DanDaAn;
        private void ButXiaYiTi_Click(object sender, EventArgs e)
        {
            //如果当前下标的题目的对应的某个答案没有被选择，那么lab控件颜色变为红色
            if (intShiFouXuanZhong_XuanZeTi == 0)
            {
                labXuan[DanID].BackColor = Color.Red;
            }
            else
            {
                labXuan[DanID].BackColor = Color.LimeGreen;
            }

            txtDanIndex.Text = "";
            if (DanTiTa.Rows.Count > 0 && DanID != DanTiTa.Rows.Count - 1)//当点击下一题时如果题目数大于0并且下一题次数不等于题目总数减一
            {
                //那么题目索引加一并显示当前索引的题目
                DanID++;
                DanXuanTi();
                butShangYiTi.Enabled = true;//题目索引大于0后开启上一题按钮
                labDanTiMuIndex.Text = DanID + 1 + "/" + DanTiTa.Rows.Count;//显示单选题题号与单选题个数

                if (DanDaAn[DanID] != "A")
                {
                    radA.Checked = false;
                }
                else
                {
                    radA.Checked = true;
                }

                if (DanDaAn[DanID] != "B")
                {
                    radB.Checked = false;
                }
                else
                {
                    radB.Checked = true;
                }

                if (DanDaAn[DanID] != "C")
                {
                    radC.Checked = false;
                }
                else
                {
                    radC.Checked = true;
                }

                if (DanDaAn[DanID] != "D")
                {
                    radD.Checked = false;
                }
                else
                {
                    radD.Checked = true;
                }
               

            }
            if (DanID == DanTiTa.Rows.Count - 1)//如果点击下一题次数等于当前题目数量减一时把下一题按钮禁用
            {
                butXiaYiTi.Enabled = false;
            }
        }

        private void ButShangYiTi_Click(object sender, EventArgs e)
        {
            txtDanIndex.Text = "";//将跳转题号清空
            if (DanID > 0)
            {
                DanID--;
                DanXuanTi();
                butXiaYiTi.Enabled = true;
                labDanTiMuIndex.Text = DanID + 1 + "/" + DanTiTa.Rows.Count;

                if (DanDaAn[DanID] == "A")
                {
                    radA.Checked = true;
                }
                else
                {
                    radA.Checked = false;
                }

                if (DanDaAn[DanID] == "B")
                {
                    radB.Checked = true;
                }
                else
                {
                    radB.Checked = false;
                }

                if (DanDaAn[DanID] == "C")
                {
                    radC.Checked = true;
                }
                else
                {
                    radC.Checked = false;
                }

                if (DanDaAn[DanID] == "D")
                {
                    radD.Checked = true;
                }
                else
                {
                    radD.Checked = false;
                }
            }
            if (DanID == 0)
            {
                butShangYiTi.Enabled = false;
            }
        }

        private int[] SuiJiTiMu(int TaRowCount, int TiMuGeShu)
        {
            Random SuiJiShu = new Random();
            int sj = 0;
            int[] CFSuiJiShu = new int[TaRowCount];
            int[] ShangYiTiIndex = new int[TiMuGeShu];
            int sytIndex = 0;
            for (; ; )
            {
                //用当前随机数与当前不重复数组的下标中的数字相比较，如果相同那么就重新生成一个随机数
                //直到产生不重复随机数的个数与数据表中的行数相等时，才停止生成随机数
                if (sj != CFSuiJiShu[sj])
                {
                    CFSuiJiShu[sj] = sj;
                    ShangYiTiIndex[sytIndex] = sj;
                    sytIndex++;
                }
                else
                {
                    sj = SuiJiShu.Next(0, TaRowCount);
                }
                if (sytIndex == TiMuGeShu - 1)
                {
                    return ShangYiTiIndex;
                }
            }
        }


        private void ComShiJuanName_SelectionChangeCommitted(object sender, EventArgs e)
        {

        }


        int[] DanXuanTiDaAnID;
        /// <summary>
        /// 根据单选题索引加载单选题题目和ID
        /// </summary>
        void DanXuanTi()
        {
            DanXuanTiDaAnID[DanID] = (int)DanTiTa.Rows[DanID]["ID"];
            richTiMu.Text = DanTiTa.Rows[DanID]["TiMu"].ToString();
            radA.Text = DanTiTa.Rows[DanID]["XuanXiangYi"].ToString();
            radB.Text = DanTiTa.Rows[DanID]["XuanXiangEr"].ToString();
            radC.Text = DanTiTa.Rows[DanID]["XuanXiangSan"].ToString();
            radD.Text = DanTiTa.Rows[DanID]["XuanXiangSi"].ToString();
        }

        int[] PanDuanTiDaAnID;
        /// <summary>
        /// 根据多选题索引加载判断题题目和ID
        /// </summary>
        void PanDuanTi()
        {
            PanDuanTiDaAnID[PanID] = (int)PanTiTa.Rows[PanID]["ID"];
            richtxtPanTiMu.Text = PanTiTa.Rows[PanID]["TiMu"].ToString();
        }


        int[] TianKongTiDaAnID;
        /// <summary>
        /// 根据多选题索引加载填空题题目和ID
        /// </summary>
        void TianKongTi()
        {
            TianKongTiDaAnID[TianID] = (int)TianTiTa.Rows[TianID]["ID"];
            richtxtTianTiMu.Text = TianTiTa.Rows[TianID]["TiMu"].ToString();
        }

        void IndexJiaZaiDanXuanTi()
        {
            //如果存在题目那么就加载题目，否则禁用上下题按钮并且提示
            if (DanTiTa.Rows.Count > 0)
            {
                //如果不存在题目那么就显示0/0否则显示题号与总题数
                int TiMuIndexId = DanID;
                labDanTiMuIndex.Text = TiMuIndexId + 1 + "/" + DanTiTa.Rows.Count;
                DanXuanTi();
                if (DanTiTa.Rows.Count == 1)//如果只有一个题目那么就禁用上一题与下一题按钮
                {
                    butShangYiTi.Enabled = false;
                    butXiaYiTi.Enabled = false;
                }
                if (DanID == 0)//如果当前题目索引为0那就禁用上一题按钮
                {
                    butShangYiTi.Enabled = false;
                }
            }
            else
            {
                labDanTiMuIndex.Text = DanID + "/" + DanTiTa.Rows.Count;
                butShangYiTi.Enabled = false;
                butXiaYiTi.Enabled = false;
                labYouWuTiMu.Text = "当前试卷没有该题目";
            }
        }

        void IndexJiaZaiPanDuanTi()
        {
            if (PanTiTa.Rows.Count > 0)
            {
                int TiMuIndexId = PanID;
                labPanTiMuIndex.Text = TiMuIndexId + 1 + "/" + PanTiTa.Rows.Count;
                PanDuanTi();
                if (PanTiTa.Rows.Count == 1)
                {
                    butPanShang.Enabled = false;
                    butPanXia.Enabled = false;
                }
                if (PanID == 0)
                {
                    butPanShang.Enabled = false;
                }
            }
            else
            {
                labPanTiMuIndex.Text = PanID + "/" + PanTiTa.Rows.Count;
                butPanShang.Enabled = false;
                butPanXia.Enabled = false;
                labPanYouWu.Text = "当前试卷没有该题目";
            }
        }

        void IndexJiaZaiTianKongTi()
        {
            if (TianTiTa.Rows.Count > 0)
            {
                int TiMuIndexId = TianID;
                labTianTiMuIndex.Text = TiMuIndexId + 1 + "/" + TianTiTa.Rows.Count;
                TianKongTi();
                if (TianTiTa.Rows.Count == 1)
                {
                    butTianShang.Enabled = false;
                    butTianXia.Enabled = false;
                }
                if (TianID == 0)
                {
                    butTianShang.Enabled = false;
                }
            }
            else
            {
                labTianTiMuIndex.Text = TianID + "/" + TianTiTa.Rows.Count;
                butTianShang.Enabled = false;
                butTianXia.Enabled = false;
                labTianYouWu.Text = "当前试卷没有该题目";
            }
        }
        private void TabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int TabIndex = tabControl1.SelectedIndex;//获取当前控件中题型的索引
            if (TabIndex == 0)//单选题
            {
                IndexJiaZaiDanXuanTi();
                //如果当前下标的题目的对应的某个答案没有被选择，那么lab控件颜色变为红色
                if (intShiFouXuanZhong_XuanZeTi == 0)
                {
                    labXuan[DanID].BackColor = Color.Red;
                }
                else
                {
                    labXuan[DanID].BackColor = Color.LimeGreen;
                }
            }
            else if (TabIndex == 1)
            {
                IndexJiaZaiPanDuanTi();
                //如果当前下标的题目的对应的某个答案没有被选择，那么lab控件颜色变为红色
                if (intShiFouXuanZhong_PanDuanTi == 0)
                {
                    labPan[PanID].BackColor = Color.Red;
                }
                else
                {
                    labPan[PanID].BackColor = Color.LimeGreen;
                }
            }
            else if (TabIndex == 2)
            {
                IndexJiaZaiTianKongTi();
                //如果当前下标的题目的对应的某个答案没有被选择，那么lab控件颜色变为红色
                if (intShiFouXuanZhong_TianKongTi == 0)
                {
                    labTian[TianID].BackColor = Color.Red;
                }
                else
                {
                    labTian[TianID].BackColor = Color.LimeGreen;
                }
            }
        }

        string[] PanDaAn;

        private void ButPanXia_Click(object sender, EventArgs e)
        {
            //如果当前下标的题目的对应的某个答案没有被选择，那么lab控件颜色变为红色
            if (intShiFouXuanZhong_PanDuanTi == 0)
            {
                labPan[PanID].BackColor = Color.Red;
            }
            else
            {
                labPan[PanID].BackColor = Color.LimeGreen;
            }
            txtPanTiIndex.Text = "";
            if (PanTiTa.Rows.Count > 0 && PanID != PanTiTa.Rows.Count - 1)
            {
                PanID++;
                PanDuanTi();
                butPanShang.Enabled = true;
                labPanTiMuIndex.Text = PanID + 1 + "/" + PanTiTa.Rows.Count;

                if (PanDaAn[PanID] == radPanDui.Text)
                {
                    radPanDui.Checked = true;
                }
                else
                {
                    radPanDui.Checked = false;
                }

                if (PanDaAn[PanID] == radPanCuo.Text)
                {
                    radPanCuo.Checked = true;
                }
                else
                {
                    radPanCuo.Checked = false;
                }
              
            }
            if (PanID == PanTiTa.Rows.Count - 1)
            {
                butPanXia.Enabled = false;
            }
        }

        private void ButPanShang_Click(object sender, EventArgs e)
        {
            txtPanTiIndex.Text = "";
            if (PanID > 0)
            {
                PanID--;
                PanDuanTi();
                butPanXia.Enabled = true;
                labPanTiMuIndex.Text = PanID + 1 + "/" + PanTiTa.Rows.Count;

                if (PanDaAn[PanID] == radPanDui.Text)
                {
                    radPanDui.Checked = true;
                }
                else
                {
                    radPanDui.Checked = false;
                }
                if (PanDaAn[PanID] == radPanCuo.Text)
                {
                    radPanCuo.Checked = true;
                }
                else
                {
                    radPanCuo.Checked = false;
                }

            }
            if (PanID == 0)
            {
                butPanShang.Enabled = false;
            }
        }


        string[] TianDaAn;
        private void ButTianXia_Click(object sender, EventArgs e)
        {
            //如果当前下标的题目的对应的某个答案没有被选择，那么lab控件颜色变为红色
            if (intShiFouXuanZhong_TianKongTi == 0)
            {
                labTian[TianID].BackColor = Color.Red;
            }
            else
            {
                labTian[TianID].BackColor = Color.LimeGreen;
            }

            txtTianIndex.Text = "";
            if (TianTiTa.Rows.Count > 0 && TianID != PanTiTa.Rows.Count - 1)
            {
                TianID++;
                TianKongTi();
                butTianShang.Enabled = true;
                labTianTiMuIndex.Text = TianID + 1 + "/" + TianTiTa.Rows.Count;

                if (TianDaAn[TianID] != null)
                {
                    txtTianDaAn.Text = TianDaAn[TianID];
                }
                else
                {
                    txtTianDaAn.Text = null;
                }
            }
            if (TianID == TianTiTa.Rows.Count - 1)
            {
                butTianXia.Enabled = false;
            }
        }

        private void ButTianShang_Click(object sender, EventArgs e)
        {
            txtTianIndex.Text = "";
            if (TianID > 0)
            {
                TianID--;
                TianKongTi();
                butTianXia.Enabled = true;
                labTianTiMuIndex.Text = TianID + 1 + "/" + TianTiTa.Rows.Count;

                if (TianDaAn[TianID] != null)
                {
                    txtTianDaAn.Text = TianDaAn[TianID];
                }
                else
                {
                    txtTianDaAn.Text = null;
                }
            }
            if (TianID == 0)
            {
                butTianShang.Enabled = false;
            }
        }

        private void ButDanTiaoZhuan_Click(object sender, EventArgs e)
        {
            int TiaoZhuanIndex = 0;
            try
            {
                TiaoZhuanIndex = int.Parse(txtDanIndex.Text);//获取跳转题号进行跳转
            }
            catch
            {
                return;
            }

            DanID = --TiaoZhuanIndex;//因为题号索引从0开始所以当前获取的题号需要减一
            DanXuanTi();
            labDanTiMuIndex.Text = DanID + 1 + "/" + DanTiTa.Rows.Count;
            if (DanID == 0)//如果当前题目为第一题那么就把上一题按钮禁用，开启下一题按钮
            {
                butShangYiTi.Enabled = false;
                butXiaYiTi.Enabled = true;
            }
            else if (DanID == DanTiTa.Rows.Count - 1)//否则如果当前题目为最后一题那么就禁用下一题按钮，开启上一题按钮
            {
                butXiaYiTi.Enabled = false;
                butShangYiTi.Enabled = true;
            }
            else//上述条件都不满足那么肯定不是最后一题也不是第一题，所以上下一题按钮都开启
            {
                butXiaYiTi.Enabled = true;
                butShangYiTi.Enabled = true;
            }


            //如果跳转的题号的答案与某个单选按钮的答案一致的话就选中这个单选按钮，否则就不选中
            if (DanDaAn[DanID] != "A")
            {
                radA.Checked = false;
            }
            else
            {
                radA.Checked = true;
            }

            if (DanDaAn[DanID] != "B")
            {
                radB.Checked = false;
            }
            else
            {
                radB.Checked = true;
            }
            if (DanDaAn[DanID] != "C")
            {
                radC.Checked = false;
            }
            else
            {
                radC.Checked = true;
            }
            if (DanDaAn[DanID] != "D")
            {
                radD.Checked = false;
            }
            else
            {
                radD.Checked = true;
            }

        }
        private void ButPanTiaoZhuan_Click(object sender, EventArgs e)
        {
            int TiaoZhuanIndex = 0;
            try
            {
                TiaoZhuanIndex = int.Parse(txtPanTiIndex.Text);
            }
            catch
            {
                return;
            }

            if (TiaoZhuanIndex > PanTiTa.Rows.Count)
            {
                MessageBox.Show("不能大于题库的题目");
                return;
            }
            PanID = --TiaoZhuanIndex;
            PanDuanTi();
            labPanTiMuIndex.Text = PanID + 1 + "/" + PanTiTa.Rows.Count;
            if (PanID == 0)
            {
                butPanShang.Enabled = false;
                butPanXia.Enabled = true;
            }
            else if (PanID == PanTiTa.Rows.Count - 1)
            {
                butPanShang.Enabled = true;
                butPanXia.Enabled = false;
            }
            else
            {
                butPanXia.Enabled = true;
                butPanShang.Enabled = true;
            }



            if (PanDaAn[PanID] == radPanDui.Text)
            {
                radPanDui.Checked = true;
            }
            else
            {
                radPanDui.Checked = false;
            }
            if (PanDaAn[PanID] == radPanCuo.Text)
            {
                radPanCuo.Checked = true;
            }
            else
            {
                radPanCuo.Checked = false;
            }
        }

        private void ButTianTiaoZhuan_Click(object sender, EventArgs e)
        {
            int TiaoZhuanIndex = 0;
            try
            {
                TiaoZhuanIndex = int.Parse(txtTianIndex.Text);
            }
            catch
            {
                return;
            }

            if (TiaoZhuanIndex > TianTiTa.Rows.Count)
            {
                MessageBox.Show("不能大于题库的题目");
                return;
            }
            TianID = --TiaoZhuanIndex;//数组索引从0开始，所以需要减一
            TianKongTi();
            labTianTiMuIndex.Text = TianID + 1 + "/" + TianTiTa.Rows.Count;

            if (TianID == 0)
            {
                butTianShang.Enabled = false;
                butTianXia.Enabled = true;
            }
            else if (TianID == TianTiTa.Rows.Count - 1)
            {
                butTianShang.Enabled = true;
                butTianXia.Enabled = false;
            }
            else
            {
                butTianXia.Enabled = true;
                butTianShang.Enabled = true;
            }


            if (TianDaAn[TianID] != null)
            {
                txtTianDaAn.Text = TianDaAn[TianID];
            }
            else
            {
                txtTianDaAn.Text = null;
            }
        }

        private void TxtDanIndex_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int TiaoZhuanIndex = int.Parse(txtDanIndex.Text);
                if (TiaoZhuanIndex > DanTiTa.Rows.Count || TiaoZhuanIndex == 0)
                {
                    butDanTiaoZhuan.Enabled = false;
                }
                else
                {
                    butDanTiaoZhuan.Enabled = true;
                }
            }
            catch
            {
            }
        }

        private void TxtPanTiIndex_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int TiaoZhuanIndex = int.Parse(txtPanTiIndex.Text);
                if (TiaoZhuanIndex > PanTiTa.Rows.Count || TiaoZhuanIndex == 0)
                {
                    butPanTiaoZhuan.Enabled = false;
                }
                else
                {
                    butPanTiaoZhuan.Enabled = true;
                }
            }
            catch
            {
            }
        }

        private void TxtTianIndex_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int TiaoZhuanIndex = int.Parse(txtTianIndex.Text);
                if (TiaoZhuanIndex > TianTiTa.Rows.Count || TiaoZhuanIndex == 0)
                {
                    butTianTiaoZhuan.Enabled = false;
                }
                else
                {
                    butTianTiaoZhuan.Enabled = true;
                }
            }
            catch
            {
            }
        }
        int Shi;//时
        int Fen;//分
        int Miao = 0;
        private void TimerDoJiShi_Tick(object sender, EventArgs e)
        {
            if (Miao == 0 && Fen != 0)
            {
                Fen--;
                Miao = 59;
            }
            if (Miao == 0 && Shi != 0 && Fen == 0)
            {
                Shi--;
                Fen = 59;
                Miao = 59;
            }
            Miao--;


            labShi.Text = Shi.ToString();
            labFen.Text = Fen.ToString();
            labMiao.Text = Miao.ToString();

            //倒计时完成后自动交卷。。。。。。。
            if (Shi == 0 && Fen == 0 && Miao == 0)
            {
                timerDoJiShi.Enabled = false;//关闭计时器
                if (TiJiaoI == 1)
                {
                    MessageBox.Show("你已提交试卷不需要再次提交");
                    return;
                }
                if (DanTiTa.Rows.Count == 0 && PanTiTa.Rows.Count == 0 && TianTiTa.Rows.Count == 0)
                {
                    MessageBox.Show("该试卷没有任何题目不能进行提交");
                    return;
                }
                JiaoJuan();
            }
        }

        /// <summary>
        /// 更新选择题的正错题ID
        /// </summary>
        void DanZhengCuoTiID(int ZhengCuoID, int DaAnID)
        {
            string UpdateDanSQL = string.Format(@"update  ShiJuanDanXuanTi set TrueFale={0}
                    where ShiJuanName='{1}' and ID={2}  and KeChengID={3}",
                   ZhengCuoID, ShiJuanName, DaAnID/*, UserIDandUserName.UserID*/, ComKeChengID);
            DBHepler.ZSG(UpdateDanSQL);//添加错题ID
        }

        /// <summary>
        /// 更新判断题的正错题ID
        /// </summary>
        void PanZhengCuoTiID(int ZhengCuoID, int DaAnID)
        {
            string UpdateDanSQL = string.Format(@"update  ShiJuanPanDuanTi set TrueFale={0}
                    where ShiJuanName='{1}' and ID={2}  and KeChengID={3}",
                     ZhengCuoID, ShiJuanName, DaAnID/*, UserIDandUserName.UserID*/, ComKeChengID);
            DBHepler.ZSG(UpdateDanSQL);//添加错题ID
        }

        /// <summary>
        /// 更新填空题的正错题ID
        /// </summary>
        void TianZhengCuoTiID(int ZhengCuoID, int DaAnID)
        {
            string UpdateDanSQL = string.Format(@"update  ShiJuanTianKongTi set TrueFale={0}
                    where ShiJuanName='{1}' and ID={2}  and KeChengID={3}",
                    ZhengCuoID, ShiJuanName, DaAnID/*, UserIDandUserName.UserID*/, ComKeChengID);
            DBHepler.ZSG(UpdateDanSQL);//添加错题ID
        }

        /// <summary>
        /// 交卷
        /// </summary>
        void JiaoJuan()
        {
            if (TiJiaoI == 1)
            {
                MessageBox.Show("你已提交试卷不需要再次提交");
                return;
            }
            if (DanTiTa.Rows.Count == 0 && PanTiTa.Rows.Count == 0 && TianTiTa.Rows.Count == 0)
            {
                MessageBox.Show("该试卷没有任何题目不能进行提交");
                return;
            }

            //查找哪个题目的答案没有填写
            for (int i = 0; i < DanTiTa.Rows.Count; i++)
            {
                if (DanDaAn[i] == null)
                {
                    int a = i + 1;
                    MessageBox.Show("单选题第" + a + "题答案未填写,请填写在交卷");
                    return;
                }
            }

            for (int i = 0; i < PanTiTa.Rows.Count; i++)
            {
                if (PanDaAn[i] == null)
                {
                    int a = i + 1;
                    MessageBox.Show("判断题第" + a + "题答案未填写,请填写在交卷");
                    return;
                }
            }

            for (int i = 0; i < TianTiTa.Rows.Count; i++)
            {
                if (TianDaAn[i] == null)
                {
                    int a = i + 1;
                    MessageBox.Show("填空题第" + a + "题答案未填写,请填写在交卷");
                    return;
                }
            }


            //对单选题进行查和改，来进行计算得分
            for (int i = 0; i < DanTiTa.Rows.Count; i++)
            {
                string InsertDanSQL = string.Format(@"update  ShiJuanDanXuanTi set YongHuDaAn='{0}'
                where ShiJuanName='{1}' and ID={2}  and KeChengID={3}",
                DanDaAn[i], ShiJuanName, DanXuanTiDaAnID[i]/*, UserIDandUserName.UserID*/, ComKeChengID);
                DBHepler.ZSG(InsertDanSQL);//添加用户答案

                string ChaDanDaAnSQL = string.Format(@"select * from ShiJuanDanXuanTi 
                where ShiJuanName='{0}' and ID={1}  and KeChengID={2}",
                ShiJuanName, DanXuanTiDaAnID[i]/*, UserIDandUserName.UserID*/, ComKeChengID);
                DataTable DanDaAnTa = DBHepler.Cha(ChaDanDaAnSQL);//查找正确答案

                //如果有填写答案才去查找
                string DanZhengQuDaAn = DanDaAnTa.Rows[0]["ZhengQueDaAn"].ToString();

                if (DanZhengQuDaAn != DanDaAn[i])//正确答案与用户答案相比较
                {
                    ShiJuanZongFenShu = ShiJuanZongFenShu - (double)DanDaAnTa.Rows[0]["MeiTiFenShu"];

                    DanZhengCuoTiID(2, DanXuanTiDaAnID[i]);//2为错误
                }
                else
                {
                    DanZhengCuoTiID(1, DanXuanTiDaAnID[i]);//1为正确
                }
            }


            for (int i = 0; i < PanTiTa.Rows.Count; i++)
            {
                string InsertPanSQL = string.Format(@"update  ShiJuanPanDuanTi set YongHuDaAn='{0}'
                where ShiJuanName='{1}' and ID={2}  and KeChengID={3}",
                PanDaAn[i], ShiJuanName, PanDuanTiDaAnID[i]/*, UserIDandUserName.UserID*/, ComKeChengID);
                DBHepler.ZSG(InsertPanSQL);//添加用户答案

                string ChaPanDaAnSQL = string.Format(@"select * from ShiJuanPanDuanTi 
                where ShiJuanName='{0}' and ID={1}  and KeChengID={2}",
                ShiJuanName, PanDuanTiDaAnID[i]/*, UserIDandUserName.UserID*/, ComKeChengID);
                DataTable PanDaAnTa = DBHepler.Cha(ChaPanDaAnSQL);//查找正确答案

                string PanZhengQuDaAn = PanDaAnTa.Rows[0]["ZhengQueDaAn"].ToString();

                if (PanZhengQuDaAn != PanDaAn[i])//正确答案与用户答案相比较
                {
                    ShiJuanZongFenShu = ShiJuanZongFenShu - (double)PanDaAnTa.Rows[0]["MeiTiFenShu"];

                    PanZhengCuoTiID(2, PanDuanTiDaAnID[i]);
                }
                else
                {

                    PanZhengCuoTiID(1, PanDuanTiDaAnID[i]);
                }
            }


            for (int i = 0; i < TianTiTa.Rows.Count; i++)
            {
                string InsertTainSQL = string.Format(@"update  ShiJuanTianKongTi set YongHuDaAn='{0}'
                where ShiJuanName='{1}' and ID={2} and KeChengID={3}",
               TianDaAn[i], ShiJuanName, TianKongTiDaAnID[i]/*, UserIDandUserName.UserID*/, ComKeChengID);
                DBHepler.ZSG(InsertTainSQL);//添加用户答案

                string ChaTianDaAnSQL = string.Format(@"select * from ShiJuanTianKongTi 
                where ShiJuanName='{0}' and ID={1}  and KeChengID={2}",
                ShiJuanName, TianKongTiDaAnID[i]/*, UserIDandUserName.UserID*/, ComKeChengID);
                DataTable TianDaAnTa = DBHepler.Cha(ChaTianDaAnSQL);//查找正确答案

                string TianZhengQuDaAn = TianDaAnTa.Rows[0]["ZhengQueDaAn"].ToString();

                if (TianZhengQuDaAn != TianDaAn[i])//正确答案与用户答案相比较
                {
                    ShiJuanZongFenShu = ShiJuanZongFenShu - (double)TianDaAnTa.Rows[0]["MeiTiFenShu"];


                    TianZhengCuoTiID(2, TianKongTiDaAnID[i]);
                }
                else
                {

                    TianZhengCuoTiID(1, TianKongTiDaAnID[i]);
                }
            }


            MessageBox.Show("最终得分：" + ShiJuanZongFenShu);
            UserIDandUserName.ShiJuanName = ShiJuanName;//传试卷名称到错题解析窗体
            UserIDandUserName.GetKeChengID = ComKeChengID;//传课程id到错题解析窗体

            TiJiaoI++;//防止用户再次提交试卷
            JieShuShiJian = DateTime.Now.ToString("yyyy/MM/dd/HH:mm:ss");//结束时间
            timerDoJiShi.Enabled = false;//关闭计时器

            //查找课程名称
            string KeChengName = "";
            string DanKeChengNameSQL = string.Format(@"select KeCheng.KeChengName  from ShiJuanDanXuanTi,KeCheng where
            ShiJuanDanXuanTi.KeChengID=KeCheng.KeChengID and ShiJuanName='{0}' ", ShiJuanName/*, UserIDandUserName.UserID*/);
            DataTable DanKeChengNameTa = DBHepler.Cha(DanKeChengNameSQL);

            string PanKeChengNameSQL = string.Format(@"select KeCheng.KeChengName  from ShiJuanPanDuanTi,KeCheng where
            ShiJuanPanDuanTi.KeChengID=KeCheng.KeChengID and ShiJuanName='{0}' ", ShiJuanName/*, UserIDandUserName.UserID*/);
            DataTable PanKeChengNameTa = DBHepler.Cha(PanKeChengNameSQL);

            string TianKeChengNameSQL = string.Format(@"select KeCheng.KeChengName  from ShiJuanTianKongTi,KeCheng where
            ShiJuanTianKongTi.KeChengID=KeCheng.KeChengID and ShiJuanName='{0}' ", ShiJuanName/*, UserIDandUserName.UserID*/);
            DataTable TianKeChengNameTa = DBHepler.Cha(TianKeChengNameSQL);

            //只要某一个试卷的课程名称，然后存入成绩单中
            if (DanKeChengNameTa.Rows.Count > 0)
            {
                KeChengName = DanKeChengNameTa.Rows[0]["KeChengName"].ToString();
            }
            else if (PanKeChengNameTa.Rows.Count > 0)
            {
                KeChengName = PanKeChengNameTa.Rows[0]["KeChengName"].ToString();
            }
            else if (TianKeChengNameTa.Rows.Count > 0)
            {
                KeChengName = TianKeChengNameTa.Rows[0]["KeChengName"].ToString();
            }
            string InsertChengJiXingXi = string.Format(@"insert into ChengJi values('{0}','{1}','{2}','{3}','{4}',{5},{6},{7})",
               ShiJuanName, KeChengName, UserIDandUserName.UserName, KaiShiShiJian, JieShuShiJian, ShiJuanZongFenShu, UserIDandUserName.UserID, labZongFen.Text);
            DBHepler.ZSG(InsertChengJiXingXi);

            this.Close();//关闭当前窗体
        }

        int intShiFouXuanZhong_XuanZeTi = 0;//声明是否选择答案的索引
        private void RadA_CheckedChanged(object sender, EventArgs e)
        {
            intShiFouXuanZhong_XuanZeTi = 0;//回来重新赋值
            if (radA.Checked == true)//判断是否选择 选择就加一
            {
                DanDaAn[DanID] = "A";
                intShiFouXuanZhong_XuanZeTi++;
            }
        }

        private void RadB_CheckedChanged(object sender, EventArgs e)
        {
            intShiFouXuanZhong_XuanZeTi = 0;
            if (radB.Checked == true)
            {
                DanDaAn[DanID] = "B";
                intShiFouXuanZhong_XuanZeTi++;
            }
        }

        private void RadC_CheckedChanged(object sender, EventArgs e)
        {
            intShiFouXuanZhong_XuanZeTi = 0;
            if (radC.Checked == true)
            {
                DanDaAn[DanID] = "C";
                intShiFouXuanZhong_XuanZeTi++;
            }
        }

        private void RadD_CheckedChanged(object sender, EventArgs e)
        {
            intShiFouXuanZhong_XuanZeTi = 0;
            if (radD.Checked == true)
            {
                DanDaAn[DanID] = "D";
                intShiFouXuanZhong_XuanZeTi++;
            }
        }

        int intShiFouXuanZhong_PanDuanTi = 0;
        private void RadPanDui_CheckedChanged(object sender, EventArgs e)
        {
            intShiFouXuanZhong_PanDuanTi = 0;
            if (radPanDui.Checked == true)
            {
                PanDaAn[PanID] = radPanDui.Text;
                intShiFouXuanZhong_PanDuanTi++;
            }
        }

        private void RadPanCuo_CheckedChanged(object sender, EventArgs e)
        {
            intShiFouXuanZhong_PanDuanTi = 0;
            if (radPanCuo.Checked == true)
            {
                PanDaAn[PanID] = radPanCuo.Text;
                intShiFouXuanZhong_PanDuanTi++;
            }
        }

        int intShiFouXuanZhong_TianKongTi = 0;
        private void RichtxtTianDaAn_TextChanged(object sender, EventArgs e)
        {
            intShiFouXuanZhong_TianKongTi = 0;
            if (txtTianDaAn.Text != "")
            {
                TianDaAn[TianID] = txtTianDaAn.Text;
                intShiFouXuanZhong_TianKongTi++;
            }
        }

        private void LabFanHui_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("确定选择退出考试？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        double ShiJuanZongFenShu = 0;
        int TiJiaoI = 0;
        string KaiShiShiJian;
        string JieShuShiJian;
        private void LabTiQianJiaoJuan_Click(object sender, EventArgs e)
        {
            //对单选题进行查和改，来进行计算得分
            #region  提前交卷
            //for (int i = 0; i < DanTiTa.Rows.Count; i++)
            //{
            //    string InsertDanSQL = string.Format(@"update  ShiJuanDanXuanTi set YongHuDaAn='{0}'
            //    where ShiJuanName='{1}' and ID={2} and UserID={3}",
            //    DanDaAn[i], ShiJuanName, DanXuanTiDaAnID[i], UserIDandUserName.UserID);
            //    DBHepler.ZSG(InsertDanSQL);//添加用户答案

            //    string ChaDanDaAnSQL = string.Format(@"select * from ShiJuanDanXuanTi 
            //    where ShiJuanName='{0}' and ID={1} and UserID={2}",
            //    ShiJuanName, DanXuanTiDaAnID[i], UserIDandUserName.UserID);
            //    DataTable DanDaAnTa = DBHepler.Cha(ChaDanDaAnSQL);//查找正确答案
            //    string DanZhengQuDaAn = DanDaAnTa.Rows[0]["ZhengQueDaAn"].ToString();

            //    if (DanZhengQuDaAn != DanDaAn[i])//正确答案与用户答案相比较
            //    {
            //        ShiJuanZongFenShu = ShiJuanZongFenShu - (double)DanDaAnTa.Rows[0]["MeiTiFenShu"];

            //        string UpdateDanSQL = string.Format(@"update  ShiJuanDanXuanTi set TrueFale={0}
            //        where ShiJuanName='{1}' and ID={2} and UserID={3}",
            //        2, ShiJuanName, DanXuanTiDaAnID[i], UserIDandUserName.UserID);
            //        DBHepler.ZSG(UpdateDanSQL);//添加错题ID
            //    }
            //    else
            //    {
            //        string UpdateDanSQL = string.Format(@"update  ShiJuanDanXuanTi set TrueFale={0}
            //        where ShiJuanName='{1}' and ID={2} and UserID={3}",
            //        1, ShiJuanName, DanXuanTiDaAnID[i], UserIDandUserName.UserID);
            //        DBHepler.ZSG(UpdateDanSQL);//更改错题ID
            //    }
            //}


            //for (int i = 0; i < PanTiTa.Rows.Count; i++)
            //{
            //    string InsertPanSQL = string.Format(@"update  ShiJuanPanDuanTi set YongHuDaAn='{0}'
            //    where ShiJuanName='{1}' and ID={2} and UserID={3}",
            //    PanDaAn[i], ShiJuanName, PanDuanTiDaAnID[i], UserIDandUserName.UserID);
            //    DBHepler.ZSG(InsertPanSQL);//添加用户答案

            //    string ChaPanDaAnSQL = string.Format(@"select * from ShiJuanPanDuanTi 
            //    where ShiJuanName='{0}' and ID={1} and UserID={2}",
            //    ShiJuanName, PanDuanTiDaAnID[i], UserIDandUserName.UserID);
            //    DataTable PanDaAnTa = DBHepler.Cha(ChaPanDaAnSQL);//查找正确答案
            //    string PanZhengQuDaAn = PanDaAnTa.Rows[0]["ZhengQueDaAn"].ToString();

            //    if (PanZhengQuDaAn != PanDaAn[i])//正确答案与用户答案相比较
            //    {
            //        ShiJuanZongFenShu = ShiJuanZongFenShu - (double)PanDaAnTa.Rows[0]["MeiTiFenShu"];

            //        string UpdateDanSQL = string.Format(@"update  ShiJuanPanDuanTi set TrueFale={0}
            //        where ShiJuanName='{1}' and ID={2} and UserID={3}",
            //        2, ShiJuanName, PanDuanTiDaAnID[i], UserIDandUserName.UserID);
            //        DBHepler.ZSG(UpdateDanSQL);//添加错题ID
            //    }
            //    else
            //    {
            //        string UpdateDanSQL = string.Format(@"update  ShiJuanPanDuanTi set TrueFale={0}
            //        where ShiJuanName='{1}' and ID={2} and UserID={3}",
            //        1, ShiJuanName, PanDuanTiDaAnID[i], UserIDandUserName.UserID);
            //        DBHepler.ZSG(UpdateDanSQL);//更改错题ID
            //    }
            //}


            //for (int i = 0; i < TianTiTa.Rows.Count; i++)
            //{
            //    string InsertTainSQL = string.Format(@"update  ShiJuanTianKongTi set YongHuDaAn='{0}'
            //    where ShiJuanName='{1}' and ID={2} and UserID={3}",
            //   TianDaAn[i], ShiJuanName, TianKongTiDaAnID[i], UserIDandUserName.UserID);
            //    DBHepler.ZSG(InsertTainSQL);//添加用户答案

            //    string ChaTianDaAnSQL = string.Format(@"select * from ShiJuanTianKongTi 
            //    where ShiJuanName='{0}' and ID={1} and UserID={2}",
            //    ShiJuanName, TianKongTiDaAnID[i], UserIDandUserName.UserID);
            //    DataTable TianDaAnTa = DBHepler.Cha(ChaTianDaAnSQL);//查找正确答案
            //    string TianZhengQuDaAn = TianDaAnTa.Rows[0]["ZhengQueDaAn"].ToString();

            //    if (TianZhengQuDaAn != TianDaAn[i])//正确答案与用户答案相比较
            //    {
            //        ShiJuanZongFenShu = ShiJuanZongFenShu - (double)TianDaAnTa.Rows[0]["MeiTiFenShu"];

            //        string UpdateDanSQL = string.Format(@"update  ShiJuanTianKongTi set TrueFale={0}
            //        where ShiJuanName='{1}' and ID={2} and UserID={3}",
            //        2, ShiJuanName, TianKongTiDaAnID[i], UserIDandUserName.UserID);
            //        DBHepler.ZSG(UpdateDanSQL);//添加错题ID
            //    }
            //    else
            //    {
            //        string UpdateDanSQL = string.Format(@"update  ShiJuanTianKongTi set TrueFale={0}
            //        where ShiJuanName='{1}' and ID={2} and UserID={3}",
            //        1, ShiJuanName, TianKongTiDaAnID[i], UserIDandUserName.UserID);
            //        DBHepler.ZSG(UpdateDanSQL);//更改错题ID
            //    }
            //}


            //MessageBox.Show("最终得分：" + ShiJuanZongFenShu);
            //UserIDandUserName.ShiJuanName = ShiJuanName;
            //TiJiaoI++;//防止用户再次提交试卷
            //JieShuShiJian = DateTime.Now.ToString("yyyy/MM/dd/HH:mm:ss");//结束时间
            //timerDoJiShi.Stop();//关闭计时器

            ////查找课程名称
            //string KeChengName = "";
            //string DanKeChengNameSQL = string.Format(@"select KeCheng.KeChengName  from ShiJuanDanXuanTi,KeCheng where
            //ShiJuanDanXuanTi.KeChengID=KeCheng.KeChengID and ShiJuanName='{0}' and UserID={1}", ShiJuanName, UserIDandUserName.UserID);
            //DataTable DanKeChengNameTa = DBHepler.Cha(DanKeChengNameSQL);

            //string PanKeChengNameSQL = string.Format(@"select KeCheng.KeChengName  from ShiJuanPanDuanTi,KeCheng where
            //ShiJuanPanDuanTi.KeChengID=KeCheng.KeChengID and ShiJuanName='{0}' and UserID={1}", ShiJuanName, UserIDandUserName.UserID);
            //DataTable PanKeChengNameTa = DBHepler.Cha(PanKeChengNameSQL);

            //string TianKeChengNameSQL = string.Format(@"select KeCheng.KeChengName  from ShiJuanTianKongTi,KeCheng where
            //ShiJuanTianKongTi.KeChengID=KeCheng.KeChengID and ShiJuanName='{0}' and UserID={1}", ShiJuanName, UserIDandUserName.UserID);
            //DataTable TianKeChengNameTa = DBHepler.Cha(TianKeChengNameSQL);

            ////只要某一个试卷的课程名称，然后存入成绩单中
            //if (DanKeChengNameTa.Rows.Count > 0)
            //{
            //    KeChengName = DanKeChengNameTa.Rows[0]["KeChengName"].ToString();
            //}
            //else if (PanKeChengNameTa.Rows.Count > 0)
            //{
            //    KeChengName = DanKeChengNameTa.Rows[0]["KeChengName"].ToString();
            //}
            //else if (TianKeChengNameTa.Rows.Count > 0)
            //{
            //    KeChengName = TianKeChengNameTa.Rows[0]["KeChengName"].ToString();
            //}
            //string InsertChengJiXingXi = string.Format(@"insert into ChengJi values('{0}','{1}','{2}','{3}','{4}',{5},{6})",
            //   ShiJuanName, KeChengName, UserIDandUserName.UserName, KaiShiShiJian, JieShuShiJian, ShiJuanZongFenShu, UserIDandUserName.UserID);
            //DBHepler.ZSG(InsertChengJiXingXi);
            #endregion

            //提前交卷
            JiaoJuan();
        }

        private void KaiShiKaoShi_MouseEnter(object sender, EventArgs e)
        {
            ShiJuanShuaXin();
            KeChengShuaXin();
        }

        DataTable DanTiTa;
        DataTable PanTiTa;
        DataTable TianTiTa;
        int ComKeChengID;
        Label[] labXuan;//选择题方块
        Label[] labPan;//判断题方块
        Label[] labTian;//填空题方块
        int tiHaoIndex = 0;

        void TianJiaLab(Label[] lab, string TiStrName, int TiMuSum, GroupBox groupName)
        {
            int x = 0;
            int y = 0;
            for (int i = 0; i < TiMuSum; i++)
            {
                lab[i] = new Label();
                lab[i].Name = TiStrName + i.ToString();
                lab[i].Text = (i + 1).ToString();
                lab[i].Width = 20;
                lab[i].Height = 20;
                lab[i].TextAlign = ContentAlignment.MiddleCenter;
                lab[i].BackColor = Color.LimeGreen;
                lab[i].ForeColor = Color.White;
                lab[i].Location = new Point(30 *x, 20 + y);
                x++;
                if (x==10)//如果前一行排满后
                {
                    x = 0;//把x坐标变为0
                    y += 30;//累加y坐标，十个换一行
                }
                groupName.Controls.Add(lab[i]);//把所有的label控件加载到group容器里面去
            }
        }
        private void ButKaiShiKaoShi_Click(object sender, EventArgs e)
        {
           
            try
            {
                ShiJuanName = ((DataRowView)comShiJuanName.SelectedItem)["ShiJuanName"].ToString();
                ComKeChengID = (int)comKeCheng.SelectedValue;
            }
            catch
            {
                return;
            }

            string SqlDanTiXingName = string.Format(@"select * from TiXing,ShiJuanDanXuanTi where 
                TiXing.TiXingID=ShiJuanDanXuanTi.TiXingID and ShiJuanDanXuanTi.ShiJuanName='{0}' and KeChengID={1}",
                ShiJuanName/*, UserIDandUserName.UserID*/, ComKeChengID);
            DanTiTa = DBHepler.Cha(SqlDanTiXingName);

            string SqlPanTiXingName = string.Format(@"select * from TiXing,ShiJuanPanDuanTi where 
             TiXing.TiXingID=ShiJuanPanDuanTi.TiXingID and ShiJuanPanDuanTi.ShiJuanName='{0}' and KeChengID={1}",
             ShiJuanName/*, UserIDandUserName.UserID*/, ComKeChengID);
            PanTiTa = DBHepler.Cha(SqlPanTiXingName);

            string SqlTianTiXingName = string.Format(@"select * from TiXing,ShiJuanTianKongTi where 
             TiXing.TiXingID=ShiJuanTianKongTi.TiXingID and ShiJuanTianKongTi.ShiJuanName='{0}'  and KeChengID={1}",
                ShiJuanName/*, UserIDandUserName.UserID*/, ComKeChengID);
            TianTiTa = DBHepler.Cha(SqlTianTiXingName);

            string SjName = "";//考试总时长
            if (DanTiTa.Rows.Count > 0)
            {
                labShiJuanName.Text = DanTiTa.Rows[0]["ShiJuanName"].ToString();//显示试卷名称
                labUserName.Text = DanTiTa.Rows[0]["YongHuName"].ToString();//显示用户名称
                SjName = DanTiTa.Rows[0]["KaoShiShiChang"].ToString();
            }
            else if (PanTiTa.Rows.Count > 0)
            {
                labShiJuanName.Text = PanTiTa.Rows[0]["ShiJuanName"].ToString();//显示试卷名称
                labUserName.Text = PanTiTa.Rows[0]["YongHuName"].ToString();//显示用户名称
                SjName = PanTiTa.Rows[0]["KaoShiShiChang"].ToString();
            }
            else if (TianTiTa.Rows.Count > 0)
            {
                labShiJuanName.Text = TianTiTa.Rows[0]["ShiJuanName"].ToString();//显示试卷名称
                labUserName.Text = TianTiTa.Rows[0]["YongHuName"].ToString();//显示用户名称
                SjName = TianTiTa.Rows[0]["KaoShiShiChang"].ToString();
            }
            else
            {
                MessageBox.Show("该课程的试卷没有任何题目,请重新选择!");
                return;
            }


            DanDaAn = new string[DanTiTa.Rows.Count];//初始化单选题答案数组
            PanDaAn = new string[PanTiTa.Rows.Count];//初始化判断题答案数组
            TianDaAn = new string[TianTiTa.Rows.Count];//初始化填空题答案数组

            DanXuanTiDaAnID = new int[DanTiTa.Rows.Count];//初始化单选题ID数组
            PanDuanTiDaAnID = new int[PanTiTa.Rows.Count];
            TianKongTiDaAnID = new int[TianTiTa.Rows.Count];

            panelKaoShiJieMian.Visible = true;//显示考试界面


            ///加载答题卡
            int tiMuZongHe = PanTiTa.Rows.Count + TianTiTa.Rows.Count;//题目总和

            labXuan = new Label[DanTiTa.Rows.Count];
            TianJiaLab(labXuan, "DaTiKa", DanTiTa.Rows.Count, groupXuanZeDaTiKa);

            labPan = new Label[PanTiTa.Rows.Count];
            TianJiaLab(labPan, "PanDaTiKa", PanTiTa.Rows.Count, groupPanDuanDaTiKa);

            labTian = new Label[TianTiTa.Rows.Count];
            TianJiaLab(labTian, "TianDaTiKa", TianTiTa.Rows.Count, groupTianKongDaTiKa);


            //如果当前下标的题目的对应的某个答案没有被选择，那么lab控件颜色变为红色
            if (intShiFouXuanZhong_XuanZeTi == 0)
            {
                labXuan[DanID].BackColor = Color.Red;
            }
            else
            {
                labXuan[DanID].BackColor = Color.LimeGreen;
            }

            //获取考试总时长
            Shi = int.Parse(SjName.Substring(0, SjName.IndexOf("小时")));//获取数字小时
            string DuoYueZF = SjName.Replace(Shi + "小时", "");//去掉小时前面的数字小时
            Fen = int.Parse(DuoYueZF.Substring(0, DuoYueZF.IndexOf("分")));//获取数字分钟


            double ShiJianZongFen = 0;
            //计算当前试卷的总题目的分数
            if (DanTiTa.Rows.Count > 0)
            {
                ShiJianZongFen += Double.Parse(DanTiTa.Rows[0]["MeiTiFenShu"].ToString()) * DanTiTa.Rows.Count;
            }
            if (PanTiTa.Rows.Count > 0)
            {
                ShiJianZongFen += Double.Parse(PanTiTa.Rows[0]["MeiTiFenShu"].ToString()) * PanTiTa.Rows.Count;
            }
            if (TianTiTa.Rows.Count > 0)
            {
                ShiJianZongFen += Double.Parse(TianTiTa.Rows[0]["MeiTiFenShu"].ToString()) * TianTiTa.Rows.Count;
            }
            labZongFen.Text = ShiJianZongFen.ToString();//显示试卷总分
            ShiJuanZongFenShu = double.Parse(labZongFen.Text);


            //显示倒计时 时间
            labShi.Text = Shi.ToString();
            labFen.Text = Fen.ToString();
            labMiao.Text = Miao.ToString();

            //开启倒计时
            timerDoJiShi.Enabled = true;
            KaiShiShiJian = DateTime.Now.ToString("yyyy/MM/dd/HH:mm:ss");//开始考试时间

            butShangYiTi.Enabled = false;//试卷名称下拉框索引变换时禁用上一题按钮

            for (int i = 0; i < 3; i++)
            {
                IndexJiaZaiDanXuanTi();
                IndexJiaZaiPanDuanTi();
                IndexJiaZaiTianKongTi();
            }
        }
    }
}
