﻿namespace KaoShiXiTong
{
    partial class KaiShiKaoShi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.butKaiShiKaoShi = new System.Windows.Forms.Button();
            this.comKeCheng = new System.Windows.Forms.ComboBox();
            this.comShiJuanName = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panelKaoShiJieMian = new System.Windows.Forms.Panel();
            this.groupTianKongDaTiKa = new System.Windows.Forms.GroupBox();
            this.groupPanDuanDaTiKa = new System.Windows.Forms.GroupBox();
            this.groupXuanZeDaTiKa = new System.Windows.Forms.GroupBox();
            this.labTiQianJiaoJuan = new System.Windows.Forms.Label();
            this.labFanHui = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.labMiao = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.labFen = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.labShi = new System.Windows.Forms.Label();
            this.labZongFen = new System.Windows.Forms.Label();
            this.labShiJuanName = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.labYouWuTiMu = new System.Windows.Forms.Label();
            this.labDanTiMuIndex = new System.Windows.Forms.Label();
            this.radD = new System.Windows.Forms.RadioButton();
            this.radA = new System.Windows.Forms.RadioButton();
            this.txtDanIndex = new System.Windows.Forms.TextBox();
            this.radC = new System.Windows.Forms.RadioButton();
            this.radB = new System.Windows.Forms.RadioButton();
            this.butShangYiTi = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.butXiaYiTi = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.richTiMu = new System.Windows.Forms.RichTextBox();
            this.butDanTiaoZhuan = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.labPanYouWu = new System.Windows.Forms.Label();
            this.labPanTiMuIndex = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtPanTiIndex = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.butPanTiaoZhuan = new System.Windows.Forms.Button();
            this.butPanXia = new System.Windows.Forms.Button();
            this.butPanShang = new System.Windows.Forms.Button();
            this.radPanCuo = new System.Windows.Forms.RadioButton();
            this.radPanDui = new System.Windows.Forms.RadioButton();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.richtxtPanTiMu = new System.Windows.Forms.RichTextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.txtTianDaAn = new System.Windows.Forms.TextBox();
            this.labTianYouWu = new System.Windows.Forms.Label();
            this.labTianTiMuIndex = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtTianIndex = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.butTianTiaoZhuan = new System.Windows.Forms.Button();
            this.butTianXia = new System.Windows.Forms.Button();
            this.butTianShang = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.richtxtTianTiMu = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labUserName = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.timerDoJiShi = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.panelKaoShiJieMian.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.butKaiShiKaoShi);
            this.panel1.Controls.Add(this.comKeCheng);
            this.panel1.Controls.Add(this.comShiJuanName);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(308, 61);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(285, 159);
            this.panel1.TabIndex = 0;
            // 
            // butKaiShiKaoShi
            // 
            this.butKaiShiKaoShi.BackColor = System.Drawing.Color.Gainsboro;
            this.butKaiShiKaoShi.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butKaiShiKaoShi.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butKaiShiKaoShi.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.butKaiShiKaoShi.Location = new System.Drawing.Point(144, 122);
            this.butKaiShiKaoShi.Name = "butKaiShiKaoShi";
            this.butKaiShiKaoShi.Size = new System.Drawing.Size(75, 23);
            this.butKaiShiKaoShi.TabIndex = 4;
            this.butKaiShiKaoShi.Text = "开始考试";
            this.butKaiShiKaoShi.UseVisualStyleBackColor = false;
            this.butKaiShiKaoShi.Click += new System.EventHandler(this.ButKaiShiKaoShi_Click);
            // 
            // comKeCheng
            // 
            this.comKeCheng.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comKeCheng.FormattingEnabled = true;
            this.comKeCheng.Location = new System.Drawing.Point(123, 85);
            this.comKeCheng.Name = "comKeCheng";
            this.comKeCheng.Size = new System.Drawing.Size(121, 25);
            this.comKeCheng.TabIndex = 3;
            // 
            // comShiJuanName
            // 
            this.comShiJuanName.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comShiJuanName.FormattingEnabled = true;
            this.comShiJuanName.Location = new System.Drawing.Point(123, 34);
            this.comShiJuanName.Name = "comShiJuanName";
            this.comShiJuanName.Size = new System.Drawing.Size(121, 25);
            this.comShiJuanName.TabIndex = 2;
            this.comShiJuanName.SelectionChangeCommitted += new System.EventHandler(this.ComShiJuanName_SelectionChangeCommitted);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label14.Location = new System.Drawing.Point(50, 90);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 17);
            this.label14.TabIndex = 1;
            this.label14.Text = "选择课程";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(50, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "选择试卷";
            // 
            // panelKaoShiJieMian
            // 
            this.panelKaoShiJieMian.BackColor = System.Drawing.Color.Transparent;
            this.panelKaoShiJieMian.Controls.Add(this.groupTianKongDaTiKa);
            this.panelKaoShiJieMian.Controls.Add(this.groupPanDuanDaTiKa);
            this.panelKaoShiJieMian.Controls.Add(this.groupXuanZeDaTiKa);
            this.panelKaoShiJieMian.Controls.Add(this.labTiQianJiaoJuan);
            this.panelKaoShiJieMian.Controls.Add(this.labFanHui);
            this.panelKaoShiJieMian.Controls.Add(this.label23);
            this.panelKaoShiJieMian.Controls.Add(this.labMiao);
            this.panelKaoShiJieMian.Controls.Add(this.label21);
            this.panelKaoShiJieMian.Controls.Add(this.labFen);
            this.panelKaoShiJieMian.Controls.Add(this.label19);
            this.panelKaoShiJieMian.Controls.Add(this.labShi);
            this.panelKaoShiJieMian.Controls.Add(this.labZongFen);
            this.panelKaoShiJieMian.Controls.Add(this.labShiJuanName);
            this.panelKaoShiJieMian.Controls.Add(this.tabControl1);
            this.panelKaoShiJieMian.Controls.Add(this.label6);
            this.panelKaoShiJieMian.Controls.Add(this.label5);
            this.panelKaoShiJieMian.Controls.Add(this.label4);
            this.panelKaoShiJieMian.Controls.Add(this.labUserName);
            this.panelKaoShiJieMian.Controls.Add(this.label2);
            this.panelKaoShiJieMian.Location = new System.Drawing.Point(11, 15);
            this.panelKaoShiJieMian.Name = "panelKaoShiJieMian";
            this.panelKaoShiJieMian.Size = new System.Drawing.Size(1178, 510);
            this.panelKaoShiJieMian.TabIndex = 1;
            // 
            // groupTianKongDaTiKa
            // 
            this.groupTianKongDaTiKa.Location = new System.Drawing.Point(886, 374);
            this.groupTianKongDaTiKa.Name = "groupTianKongDaTiKa";
            this.groupTianKongDaTiKa.Size = new System.Drawing.Size(289, 133);
            this.groupTianKongDaTiKa.TabIndex = 19;
            this.groupTianKongDaTiKa.TabStop = false;
            this.groupTianKongDaTiKa.Text = "填空题答题卡";
            // 
            // groupPanDuanDaTiKa
            // 
            this.groupPanDuanDaTiKa.Location = new System.Drawing.Point(886, 222);
            this.groupPanDuanDaTiKa.Name = "groupPanDuanDaTiKa";
            this.groupPanDuanDaTiKa.Size = new System.Drawing.Size(289, 136);
            this.groupPanDuanDaTiKa.TabIndex = 19;
            this.groupPanDuanDaTiKa.TabStop = false;
            this.groupPanDuanDaTiKa.Text = "判断题答题卡";
            // 
            // groupXuanZeDaTiKa
            // 
            this.groupXuanZeDaTiKa.Location = new System.Drawing.Point(885, 80);
            this.groupXuanZeDaTiKa.Name = "groupXuanZeDaTiKa";
            this.groupXuanZeDaTiKa.Size = new System.Drawing.Size(289, 125);
            this.groupXuanZeDaTiKa.TabIndex = 19;
            this.groupXuanZeDaTiKa.TabStop = false;
            this.groupXuanZeDaTiKa.Text = "选择题答题卡";
            // 
            // labTiQianJiaoJuan
            // 
            this.labTiQianJiaoJuan.AutoSize = true;
            this.labTiQianJiaoJuan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labTiQianJiaoJuan.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labTiQianJiaoJuan.ForeColor = System.Drawing.Color.Red;
            this.labTiQianJiaoJuan.Location = new System.Drawing.Point(828, 49);
            this.labTiQianJiaoJuan.Name = "labTiQianJiaoJuan";
            this.labTiQianJiaoJuan.Size = new System.Drawing.Size(56, 17);
            this.labTiQianJiaoJuan.TabIndex = 18;
            this.labTiQianJiaoJuan.Text = "提前交卷";
            this.labTiQianJiaoJuan.Click += new System.EventHandler(this.LabTiQianJiaoJuan_Click);
            // 
            // labFanHui
            // 
            this.labFanHui.AutoSize = true;
            this.labFanHui.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labFanHui.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labFanHui.ForeColor = System.Drawing.Color.Blue;
            this.labFanHui.Location = new System.Drawing.Point(753, 49);
            this.labFanHui.Name = "labFanHui";
            this.labFanHui.Size = new System.Drawing.Size(56, 17);
            this.labFanHui.TabIndex = 18;
            this.labFanHui.Text = "退出考试";
            this.labFanHui.Click += new System.EventHandler(this.LabFanHui_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label23.Location = new System.Drawing.Point(874, 20);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(17, 12);
            this.label23.TabIndex = 17;
            this.label23.Text = "秒";
            // 
            // labMiao
            // 
            this.labMiao.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labMiao.ForeColor = System.Drawing.Color.Red;
            this.labMiao.Location = new System.Drawing.Point(845, 17);
            this.labMiao.Name = "labMiao";
            this.labMiao.Size = new System.Drawing.Size(27, 19);
            this.labMiao.TabIndex = 17;
            this.labMiao.Text = "00";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label21.Location = new System.Drawing.Point(828, 20);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(17, 12);
            this.label21.TabIndex = 17;
            this.label21.Text = "分";
            // 
            // labFen
            // 
            this.labFen.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labFen.ForeColor = System.Drawing.Color.Red;
            this.labFen.Location = new System.Drawing.Point(801, 17);
            this.labFen.Name = "labFen";
            this.labFen.Size = new System.Drawing.Size(27, 19);
            this.labFen.TabIndex = 17;
            this.labFen.Text = "00";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label19.Location = new System.Drawing.Point(771, 20);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(29, 12);
            this.label19.TabIndex = 17;
            this.label19.Text = "小时";
            // 
            // labShi
            // 
            this.labShi.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labShi.ForeColor = System.Drawing.Color.Red;
            this.labShi.Location = new System.Drawing.Point(745, 17);
            this.labShi.Name = "labShi";
            this.labShi.Size = new System.Drawing.Size(27, 19);
            this.labShi.TabIndex = 17;
            this.labShi.Text = "00";
            // 
            // labZongFen
            // 
            this.labZongFen.AutoSize = true;
            this.labZongFen.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labZongFen.ForeColor = System.Drawing.Color.Blue;
            this.labZongFen.Location = new System.Drawing.Point(552, 15);
            this.labZongFen.Name = "labZongFen";
            this.labZongFen.Size = new System.Drawing.Size(40, 22);
            this.labZongFen.TabIndex = 16;
            this.labZongFen.Text = "000";
            // 
            // labShiJuanName
            // 
            this.labShiJuanName.AutoSize = true;
            this.labShiJuanName.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labShiJuanName.ForeColor = System.Drawing.Color.Red;
            this.labShiJuanName.Location = new System.Drawing.Point(314, 16);
            this.labShiJuanName.Name = "labShiJuanName";
            this.labShiJuanName.Size = new System.Drawing.Size(60, 19);
            this.labShiJuanName.TabIndex = 15;
            this.labShiJuanName.Text = "label19";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(47, 66);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(833, 441);
            this.tabControl1.TabIndex = 14;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.TabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Transparent;
            this.tabPage1.Controls.Add(this.labYouWuTiMu);
            this.tabPage1.Controls.Add(this.labDanTiMuIndex);
            this.tabPage1.Controls.Add(this.radD);
            this.tabPage1.Controls.Add(this.radA);
            this.tabPage1.Controls.Add(this.txtDanIndex);
            this.tabPage1.Controls.Add(this.radC);
            this.tabPage1.Controls.Add(this.radB);
            this.tabPage1.Controls.Add(this.butShangYiTi);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.butXiaYiTi);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.richTiMu);
            this.tabPage1.Controls.Add(this.butDanTiaoZhuan);
            this.tabPage1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(825, 415);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "单选题";
            // 
            // labYouWuTiMu
            // 
            this.labYouWuTiMu.AutoSize = true;
            this.labYouWuTiMu.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labYouWuTiMu.ForeColor = System.Drawing.Color.Red;
            this.labYouWuTiMu.Location = new System.Drawing.Point(582, 65);
            this.labYouWuTiMu.Name = "labYouWuTiMu";
            this.labYouWuTiMu.Size = new System.Drawing.Size(0, 17);
            this.labYouWuTiMu.TabIndex = 14;
            // 
            // labDanTiMuIndex
            // 
            this.labDanTiMuIndex.AutoSize = true;
            this.labDanTiMuIndex.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labDanTiMuIndex.Location = new System.Drawing.Point(292, 378);
            this.labDanTiMuIndex.Name = "labDanTiMuIndex";
            this.labDanTiMuIndex.Size = new System.Drawing.Size(50, 17);
            this.labDanTiMuIndex.TabIndex = 13;
            this.labDanTiMuIndex.Text = "label19";
            // 
            // radD
            // 
            this.radD.Cursor = System.Windows.Forms.Cursors.Hand;
            this.radD.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.radD.Location = new System.Drawing.Point(206, 323);
            this.radD.Name = "radD";
            this.radD.Size = new System.Drawing.Size(357, 33);
            this.radD.TabIndex = 6;
            this.radD.TabStop = true;
            this.radD.Text = "D";
            this.radD.UseVisualStyleBackColor = true;
            this.radD.CheckedChanged += new System.EventHandler(this.RadD_CheckedChanged);
            // 
            // radA
            // 
            this.radA.Cursor = System.Windows.Forms.Cursors.Hand;
            this.radA.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.radA.Location = new System.Drawing.Point(206, 206);
            this.radA.Name = "radA";
            this.radA.Size = new System.Drawing.Size(357, 33);
            this.radA.TabIndex = 6;
            this.radA.TabStop = true;
            this.radA.Text = "A";
            this.radA.UseVisualStyleBackColor = true;
            this.radA.CheckedChanged += new System.EventHandler(this.RadA_CheckedChanged);
            // 
            // txtDanIndex
            // 
            this.txtDanIndex.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtDanIndex.Location = new System.Drawing.Point(484, 375);
            this.txtDanIndex.Name = "txtDanIndex";
            this.txtDanIndex.Size = new System.Drawing.Size(64, 23);
            this.txtDanIndex.TabIndex = 12;
            this.txtDanIndex.TextChanged += new System.EventHandler(this.TxtDanIndex_TextChanged);
            // 
            // radC
            // 
            this.radC.Cursor = System.Windows.Forms.Cursors.Hand;
            this.radC.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.radC.Location = new System.Drawing.Point(206, 284);
            this.radC.Name = "radC";
            this.radC.Size = new System.Drawing.Size(357, 33);
            this.radC.TabIndex = 6;
            this.radC.TabStop = true;
            this.radC.Text = "C";
            this.radC.UseVisualStyleBackColor = true;
            this.radC.CheckedChanged += new System.EventHandler(this.RadC_CheckedChanged);
            // 
            // radB
            // 
            this.radB.Cursor = System.Windows.Forms.Cursors.Hand;
            this.radB.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.radB.Location = new System.Drawing.Point(206, 245);
            this.radB.Name = "radB";
            this.radB.Size = new System.Drawing.Size(357, 33);
            this.radB.TabIndex = 6;
            this.radB.TabStop = true;
            this.radB.Text = "B";
            this.radB.UseVisualStyleBackColor = true;
            this.radB.CheckedChanged += new System.EventHandler(this.RadB_CheckedChanged);
            // 
            // butShangYiTi
            // 
            this.butShangYiTi.BackColor = System.Drawing.Color.Gainsboro;
            this.butShangYiTi.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butShangYiTi.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butShangYiTi.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.butShangYiTi.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.butShangYiTi.Location = new System.Drawing.Point(195, 373);
            this.butShangYiTi.Name = "butShangYiTi";
            this.butShangYiTi.Size = new System.Drawing.Size(75, 23);
            this.butShangYiTi.TabIndex = 10;
            this.butShangYiTi.Text = "上一题";
            this.butShangYiTi.UseVisualStyleBackColor = false;
            this.butShangYiTi.Click += new System.EventHandler(this.ButShangYiTi_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.Location = new System.Drawing.Point(560, 379);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(20, 17);
            this.label9.TabIndex = 11;
            this.label9.Text = "题";
            // 
            // butXiaYiTi
            // 
            this.butXiaYiTi.BackColor = System.Drawing.Color.Gainsboro;
            this.butXiaYiTi.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butXiaYiTi.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butXiaYiTi.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.butXiaYiTi.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.butXiaYiTi.Location = new System.Drawing.Point(358, 373);
            this.butXiaYiTi.Name = "butXiaYiTi";
            this.butXiaYiTi.Size = new System.Drawing.Size(75, 23);
            this.butXiaYiTi.TabIndex = 10;
            this.butXiaYiTi.Text = "下一题";
            this.butXiaYiTi.UseVisualStyleBackColor = false;
            this.butXiaYiTi.Click += new System.EventHandler(this.ButXiaYiTi_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(442, 379);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 17);
            this.label8.TabIndex = 11;
            this.label8.Text = "转到";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(169, 182);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "答案：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(169, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 17);
            this.label7.TabIndex = 4;
            this.label7.Text = "题目：";
            // 
            // richTiMu
            // 
            this.richTiMu.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTiMu.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.richTiMu.Location = new System.Drawing.Point(206, 65);
            this.richTiMu.Name = "richTiMu";
            this.richTiMu.Size = new System.Drawing.Size(357, 109);
            this.richTiMu.TabIndex = 5;
            this.richTiMu.Text = "";
            // 
            // butDanTiaoZhuan
            // 
            this.butDanTiaoZhuan.BackColor = System.Drawing.Color.Gainsboro;
            this.butDanTiaoZhuan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butDanTiaoZhuan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butDanTiaoZhuan.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.butDanTiaoZhuan.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.butDanTiaoZhuan.Location = new System.Drawing.Point(598, 375);
            this.butDanTiaoZhuan.Name = "butDanTiaoZhuan";
            this.butDanTiaoZhuan.Size = new System.Drawing.Size(75, 23);
            this.butDanTiaoZhuan.TabIndex = 10;
            this.butDanTiaoZhuan.Text = "跳转";
            this.butDanTiaoZhuan.UseVisualStyleBackColor = false;
            this.butDanTiaoZhuan.Click += new System.EventHandler(this.ButDanTiaoZhuan_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.Transparent;
            this.tabPage3.Controls.Add(this.labPanYouWu);
            this.tabPage3.Controls.Add(this.labPanTiMuIndex);
            this.tabPage3.Controls.Add(this.label15);
            this.tabPage3.Controls.Add(this.txtPanTiIndex);
            this.tabPage3.Controls.Add(this.label16);
            this.tabPage3.Controls.Add(this.butPanTiaoZhuan);
            this.tabPage3.Controls.Add(this.butPanXia);
            this.tabPage3.Controls.Add(this.butPanShang);
            this.tabPage3.Controls.Add(this.radPanCuo);
            this.tabPage3.Controls.Add(this.radPanDui);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.richtxtPanTiMu);
            this.tabPage3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(825, 415);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "判断题";
            // 
            // labPanYouWu
            // 
            this.labPanYouWu.AutoSize = true;
            this.labPanYouWu.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labPanYouWu.ForeColor = System.Drawing.Color.Red;
            this.labPanYouWu.Location = new System.Drawing.Point(626, 74);
            this.labPanYouWu.Name = "labPanYouWu";
            this.labPanYouWu.Size = new System.Drawing.Size(0, 25);
            this.labPanYouWu.TabIndex = 19;
            // 
            // labPanTiMuIndex
            // 
            this.labPanTiMuIndex.AutoSize = true;
            this.labPanTiMuIndex.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labPanTiMuIndex.Location = new System.Drawing.Point(277, 343);
            this.labPanTiMuIndex.Name = "labPanTiMuIndex";
            this.labPanTiMuIndex.Size = new System.Drawing.Size(50, 17);
            this.labPanTiMuIndex.TabIndex = 18;
            this.labPanTiMuIndex.Text = "label19";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label15.Location = new System.Drawing.Point(541, 345);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(20, 17);
            this.label15.TabIndex = 17;
            this.label15.Text = "题";
            // 
            // txtPanTiIndex
            // 
            this.txtPanTiIndex.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtPanTiIndex.Location = new System.Drawing.Point(460, 340);
            this.txtPanTiIndex.Name = "txtPanTiIndex";
            this.txtPanTiIndex.Size = new System.Drawing.Size(70, 23);
            this.txtPanTiIndex.TabIndex = 16;
            this.txtPanTiIndex.TextChanged += new System.EventHandler(this.TxtPanTiIndex_TextChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label16.Location = new System.Drawing.Point(425, 345);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(32, 17);
            this.label16.TabIndex = 15;
            this.label16.Text = "转到";
            // 
            // butPanTiaoZhuan
            // 
            this.butPanTiaoZhuan.BackColor = System.Drawing.Color.Gainsboro;
            this.butPanTiaoZhuan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butPanTiaoZhuan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butPanTiaoZhuan.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.butPanTiaoZhuan.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.butPanTiaoZhuan.Location = new System.Drawing.Point(571, 340);
            this.butPanTiaoZhuan.Name = "butPanTiaoZhuan";
            this.butPanTiaoZhuan.Size = new System.Drawing.Size(75, 23);
            this.butPanTiaoZhuan.TabIndex = 12;
            this.butPanTiaoZhuan.Text = "跳转";
            this.butPanTiaoZhuan.UseVisualStyleBackColor = false;
            this.butPanTiaoZhuan.Click += new System.EventHandler(this.ButPanTiaoZhuan_Click);
            // 
            // butPanXia
            // 
            this.butPanXia.BackColor = System.Drawing.Color.Gainsboro;
            this.butPanXia.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butPanXia.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butPanXia.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.butPanXia.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.butPanXia.Location = new System.Drawing.Point(338, 340);
            this.butPanXia.Name = "butPanXia";
            this.butPanXia.Size = new System.Drawing.Size(75, 23);
            this.butPanXia.TabIndex = 13;
            this.butPanXia.Text = "下一题";
            this.butPanXia.UseVisualStyleBackColor = false;
            this.butPanXia.Click += new System.EventHandler(this.ButPanXia_Click);
            // 
            // butPanShang
            // 
            this.butPanShang.BackColor = System.Drawing.Color.Gainsboro;
            this.butPanShang.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butPanShang.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butPanShang.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.butPanShang.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.butPanShang.Location = new System.Drawing.Point(199, 340);
            this.butPanShang.Name = "butPanShang";
            this.butPanShang.Size = new System.Drawing.Size(75, 23);
            this.butPanShang.TabIndex = 14;
            this.butPanShang.Text = "上一题";
            this.butPanShang.UseVisualStyleBackColor = false;
            this.butPanShang.Click += new System.EventHandler(this.ButPanShang_Click);
            // 
            // radPanCuo
            // 
            this.radPanCuo.AutoSize = true;
            this.radPanCuo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.radPanCuo.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.radPanCuo.Location = new System.Drawing.Point(326, 239);
            this.radPanCuo.Name = "radPanCuo";
            this.radPanCuo.Size = new System.Drawing.Size(38, 21);
            this.radPanCuo.TabIndex = 0;
            this.radPanCuo.Text = "错";
            this.radPanCuo.UseVisualStyleBackColor = true;
            this.radPanCuo.CheckedChanged += new System.EventHandler(this.RadPanCuo_CheckedChanged);
            // 
            // radPanDui
            // 
            this.radPanDui.AutoSize = true;
            this.radPanDui.Cursor = System.Windows.Forms.Cursors.Hand;
            this.radPanDui.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.radPanDui.Location = new System.Drawing.Point(249, 239);
            this.radPanDui.Name = "radPanDui";
            this.radPanDui.Size = new System.Drawing.Size(38, 21);
            this.radPanDui.TabIndex = 0;
            this.radPanDui.Text = "对";
            this.radPanDui.UseVisualStyleBackColor = true;
            this.radPanDui.CheckedChanged += new System.EventHandler(this.RadPanDui_CheckedChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.Location = new System.Drawing.Point(208, 204);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 17);
            this.label10.TabIndex = 8;
            this.label10.Text = "答案：";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label11.Location = new System.Drawing.Point(208, 49);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 17);
            this.label11.TabIndex = 8;
            this.label11.Text = "题目：";
            // 
            // richtxtPanTiMu
            // 
            this.richtxtPanTiMu.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richtxtPanTiMu.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.richtxtPanTiMu.Location = new System.Drawing.Point(245, 72);
            this.richtxtPanTiMu.Name = "richtxtPanTiMu";
            this.richtxtPanTiMu.Size = new System.Drawing.Size(357, 112);
            this.richtxtPanTiMu.TabIndex = 9;
            this.richtxtPanTiMu.Text = "";
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.Transparent;
            this.tabPage4.Controls.Add(this.txtTianDaAn);
            this.tabPage4.Controls.Add(this.labTianYouWu);
            this.tabPage4.Controls.Add(this.labTianTiMuIndex);
            this.tabPage4.Controls.Add(this.label17);
            this.tabPage4.Controls.Add(this.txtTianIndex);
            this.tabPage4.Controls.Add(this.label18);
            this.tabPage4.Controls.Add(this.butTianTiaoZhuan);
            this.tabPage4.Controls.Add(this.butTianXia);
            this.tabPage4.Controls.Add(this.butTianShang);
            this.tabPage4.Controls.Add(this.label13);
            this.tabPage4.Controls.Add(this.label12);
            this.tabPage4.Controls.Add(this.richtxtTianTiMu);
            this.tabPage4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(825, 415);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "填空题";
            // 
            // txtTianDaAn
            // 
            this.txtTianDaAn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTianDaAn.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtTianDaAn.Location = new System.Drawing.Point(226, 219);
            this.txtTianDaAn.Multiline = true;
            this.txtTianDaAn.Name = "txtTianDaAn";
            this.txtTianDaAn.Size = new System.Drawing.Size(357, 73);
            this.txtTianDaAn.TabIndex = 26;
            this.txtTianDaAn.TextChanged += new System.EventHandler(this.RichtxtTianDaAn_TextChanged);
            // 
            // labTianYouWu
            // 
            this.labTianYouWu.AutoSize = true;
            this.labTianYouWu.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labTianYouWu.ForeColor = System.Drawing.Color.Red;
            this.labTianYouWu.Location = new System.Drawing.Point(605, 47);
            this.labTianYouWu.Name = "labTianYouWu";
            this.labTianYouWu.Size = new System.Drawing.Size(0, 17);
            this.labTianYouWu.TabIndex = 25;
            // 
            // labTianTiMuIndex
            // 
            this.labTianTiMuIndex.AutoSize = true;
            this.labTianTiMuIndex.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labTianTiMuIndex.Location = new System.Drawing.Point(264, 354);
            this.labTianTiMuIndex.Name = "labTianTiMuIndex";
            this.labTianTiMuIndex.Size = new System.Drawing.Size(50, 17);
            this.labTianTiMuIndex.TabIndex = 24;
            this.labTianTiMuIndex.Text = "label19";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label17.Location = new System.Drawing.Point(528, 354);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(20, 17);
            this.label17.TabIndex = 23;
            this.label17.Text = "题";
            // 
            // txtTianIndex
            // 
            this.txtTianIndex.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtTianIndex.Location = new System.Drawing.Point(447, 351);
            this.txtTianIndex.Name = "txtTianIndex";
            this.txtTianIndex.Size = new System.Drawing.Size(70, 23);
            this.txtTianIndex.TabIndex = 22;
            this.txtTianIndex.TextChanged += new System.EventHandler(this.TxtTianIndex_TextChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label18.Location = new System.Drawing.Point(408, 355);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(32, 17);
            this.label18.TabIndex = 21;
            this.label18.Text = "转到";
            // 
            // butTianTiaoZhuan
            // 
            this.butTianTiaoZhuan.BackColor = System.Drawing.Color.Gainsboro;
            this.butTianTiaoZhuan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butTianTiaoZhuan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butTianTiaoZhuan.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.butTianTiaoZhuan.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.butTianTiaoZhuan.Location = new System.Drawing.Point(558, 351);
            this.butTianTiaoZhuan.Name = "butTianTiaoZhuan";
            this.butTianTiaoZhuan.Size = new System.Drawing.Size(75, 23);
            this.butTianTiaoZhuan.TabIndex = 18;
            this.butTianTiaoZhuan.Text = "跳转";
            this.butTianTiaoZhuan.UseVisualStyleBackColor = false;
            this.butTianTiaoZhuan.Click += new System.EventHandler(this.ButTianTiaoZhuan_Click);
            // 
            // butTianXia
            // 
            this.butTianXia.BackColor = System.Drawing.Color.Gainsboro;
            this.butTianXia.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butTianXia.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butTianXia.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.butTianXia.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.butTianXia.Location = new System.Drawing.Point(323, 351);
            this.butTianXia.Name = "butTianXia";
            this.butTianXia.Size = new System.Drawing.Size(75, 23);
            this.butTianXia.TabIndex = 19;
            this.butTianXia.Text = "下一题";
            this.butTianXia.UseVisualStyleBackColor = false;
            this.butTianXia.Click += new System.EventHandler(this.ButTianXia_Click);
            // 
            // butTianShang
            // 
            this.butTianShang.BackColor = System.Drawing.Color.Gainsboro;
            this.butTianShang.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butTianShang.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butTianShang.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.butTianShang.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.butTianShang.Location = new System.Drawing.Point(185, 351);
            this.butTianShang.Name = "butTianShang";
            this.butTianShang.Size = new System.Drawing.Size(75, 23);
            this.butTianShang.TabIndex = 20;
            this.butTianShang.Text = "上一题";
            this.butTianShang.UseVisualStyleBackColor = false;
            this.butTianShang.Click += new System.EventHandler(this.ButTianShang_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label13.Location = new System.Drawing.Point(189, 186);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 17);
            this.label13.TabIndex = 10;
            this.label13.Text = "答案：";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label12.Location = new System.Drawing.Point(189, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 17);
            this.label12.TabIndex = 10;
            this.label12.Text = "题目：";
            // 
            // richtxtTianTiMu
            // 
            this.richtxtTianTiMu.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richtxtTianTiMu.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.richtxtTianTiMu.Location = new System.Drawing.Point(226, 45);
            this.richtxtTianTiMu.Name = "richtxtTianTiMu";
            this.richtxtTianTiMu.Size = new System.Drawing.Size(357, 134);
            this.richtxtTianTiMu.TabIndex = 11;
            this.richtxtTianTiMu.Text = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(491, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 12);
            this.label6.TabIndex = 3;
            this.label6.Text = "试卷总分：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(246, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 2;
            this.label5.Text = "试卷名称：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(661, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 12);
            this.label4.TabIndex = 1;
            this.label4.Text = "考试剩余时长：";
            // 
            // labUserName
            // 
            this.labUserName.AutoSize = true;
            this.labUserName.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labUserName.ForeColor = System.Drawing.Color.Red;
            this.labUserName.Location = new System.Drawing.Point(107, 16);
            this.labUserName.Name = "labUserName";
            this.labUserName.Size = new System.Drawing.Size(51, 19);
            this.labUserName.TabIndex = 0;
            this.labUserName.Text = "某某某";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(49, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "用户名：";
            // 
            // timerDoJiShi
            // 
            this.timerDoJiShi.Interval = 1000;
            this.timerDoJiShi.Tick += new System.EventHandler(this.TimerDoJiShi_Tick);
            // 
            // KaiShiKaoShi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(236)))), ((int)(((byte)(236)))));
            this.ClientSize = new System.Drawing.Size(1201, 554);
            this.Controls.Add(this.panelKaoShiJieMian);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "KaiShiKaoShi";
            this.Text = "ZaiXianKaoShi";
            this.Load += new System.EventHandler(this.KaiShiKaoShi_Load);
            this.MouseEnter += new System.EventHandler(this.KaiShiKaoShi_MouseEnter);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelKaoShiJieMian.ResumeLayout(false);
            this.panelKaoShiJieMian.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox comShiJuanName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelKaoShiJieMian;
        private System.Windows.Forms.RadioButton radD;
        private System.Windows.Forms.RadioButton radC;
        private System.Windows.Forms.RadioButton radB;
        private System.Windows.Forms.RadioButton radA;
        private System.Windows.Forms.RichTextBox richTiMu;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labUserName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton radPanCuo;
        private System.Windows.Forms.RadioButton radPanDui;
        private System.Windows.Forms.Button butShangYiTi;
        private System.Windows.Forms.TextBox txtDanIndex;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button butDanTiaoZhuan;
        private System.Windows.Forms.Button butXiaYiTi;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.RichTextBox richtxtPanTiMu;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.RichTextBox richtxtTianTiMu;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtPanTiIndex;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button butPanTiaoZhuan;
        private System.Windows.Forms.Button butPanXia;
        private System.Windows.Forms.Button butPanShang;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtTianIndex;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button butTianTiaoZhuan;
        private System.Windows.Forms.Button butTianXia;
        private System.Windows.Forms.Button butTianShang;
        private System.Windows.Forms.Label labDanTiMuIndex;
        private System.Windows.Forms.Label labPanTiMuIndex;
        private System.Windows.Forms.Label labTianTiMuIndex;
        private System.Windows.Forms.Label labYouWuTiMu;
        private System.Windows.Forms.Label labPanYouWu;
        private System.Windows.Forms.Label labTianYouWu;
        private System.Windows.Forms.Label labShiJuanName;
        private System.Windows.Forms.Label labZongFen;
        private System.Windows.Forms.Timer timerDoJiShi;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label labMiao;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label labFen;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label labShi;
        private System.Windows.Forms.TextBox txtTianDaAn;
        private System.Windows.Forms.Label labTiQianJiaoJuan;
        private System.Windows.Forms.Label labFanHui;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button butKaiShiKaoShi;
        private System.Windows.Forms.ComboBox comKeCheng;
        private System.Windows.Forms.GroupBox groupXuanZeDaTiKa;
        private System.Windows.Forms.GroupBox groupTianKongDaTiKa;
        private System.Windows.Forms.GroupBox groupPanDuanDaTiKa;
    }
}