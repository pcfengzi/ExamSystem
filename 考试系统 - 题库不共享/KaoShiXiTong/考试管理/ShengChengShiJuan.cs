﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using Microsoft.Office.Interop.Word;
using Microsoft.Office;
using MSWord = Microsoft.Office.Interop.Word;
using System.IO;
using System.Reflection;
using DataTable = System.Data.DataTable;
using Application = Microsoft.Office.Interop.Word.Application;

namespace KaoShiXiTong
{
    public partial class ShengChengShiJuan : Form
    {
        public ShengChengShiJuan()
        {
            InitializeComponent();
        }


        bool bo = false;
        bool bo_3 = false;
        bool bo_4 = false;


        /// <summary>
        /// 验证数据类型是否为double类型
        /// </summary>
        /// <param name="StringGeShi"></param>
        /// <returns></returns>
        private bool DoubleGeShi(string StringGeShi)
        {
            try
            {
                double SFZ = Convert.ToDouble(StringGeShi);
            }
            catch
            {
                return false;
            }
            return true;
        }


        /// <summary>
        /// 验证数据类型是否为int类型
        /// </summary>
        /// <param name="StringGeShi"></param>
        /// <returns></returns>
        private bool IntGeShi(string StringGeShi)
        {
            try
            {
                int SFZ = Convert.ToInt32(StringGeShi);
            }
            catch
            {
                return false;
            }
            return true;
        }

        string ZongShiChang;
        string ShiJuanName;
        int DanXuanTiShuLiang = 0;
        double DanXuanTiFenShu = 0;
        int PanDuanTiShuLiang = 0;
        double PanDuanTiFenShu = 0;
        int TianKongTiShuLiang = 0;
        double TianKongTiFenShu = 0;

        private void ButShengChengShiJuan_Click(object sender, EventArgs e)
        {
            int XiaoShi = 0;
            int FenZhong = 0;
            ShiJuanName = txtShiJuanName.Text;

            if (ShiJuanName == "")
            {
                MessageBox.Show("请输入试卷名称！");
                return;
            }

            //判断数据格式，以防抛异常
            if (IntGeShi(txtDanXuanTiShuLiang.Text))
            {
                DanXuanTiShuLiang = int.Parse(txtDanXuanTiShuLiang.Text);
            }
            else
            {
                DanXuanTiShuLiang = 0;
            }

            if (DoubleGeShi(txtDanXuanTiFenShu.Text))
            {
                DanXuanTiFenShu = Double.Parse(txtDanXuanTiFenShu.Text);
            }
            else
            {
                DanXuanTiFenShu = 0;
            }

            if (IntGeShi(txtPanDuanTiShuLiang.Text))
            {
                PanDuanTiShuLiang = int.Parse(txtPanDuanTiShuLiang.Text);
            }
            else
            {
                PanDuanTiShuLiang = 0;

            }

            if (DoubleGeShi(txtPanDuanTiFenShu.Text))
            {
                PanDuanTiFenShu = Double.Parse(txtPanDuanTiFenShu.Text);
            }
            else
            {
                PanDuanTiFenShu = 0;
            }

            if (IntGeShi(txtTianKongTiShuLiang.Text))
            {
                TianKongTiShuLiang = int.Parse(txtTianKongTiShuLiang.Text);
            }
            else
            {
                TianKongTiShuLiang = 0;
            }

            if (DoubleGeShi(txtTianKongTiFenShu.Text))
            {
                TianKongTiFenShu = Double.Parse(txtTianKongTiFenShu.Text);
            }
            else
            {
                TianKongTiFenShu = 0;
            }

            if (IntGeShi(txtXiaoShi.Text))
            {
                XiaoShi = int.Parse(txtXiaoShi.Text);
            }
            else
            {
                XiaoShi = 0;
            }

            if (IntGeShi(txtFenZhong.Text))
            {
                FenZhong = int.Parse(txtFenZhong.Text);
            }
            else
            {
                FenZhong = 0;
            }

            ZongShiChang = XiaoShi + "小时" + FenZhong + "分钟";

            int KeChengID = (int)comKeChengName.SelectedValue;

            ChaDanXuanTiBiao(KeChengID);
            ChaTianKongTiBiao(KeChengID);
            ChaPanDuanTiBiao(KeChengID);


            //判断添加的题目数量是否在题库中充足
            if (DanXuanTiShuLiang > DanXuanTiTa.Rows.Count)
            {
                MessageBox.Show("单选题 题库数量不足！");
                return;
            }
            else if (PanDuanTiShuLiang > PanDuanTiTa.Rows.Count)
            {
                MessageBox.Show("判断题 题库数量不足！");
                return;
            }
            else if (TianKongTiShuLiang > TianKongTiTa.Rows.Count)
            {
                MessageBox.Show("填空题 题库数量不足！");
                return;
            }

            //获取题号
            DanSuiJiTiHao(DanXuanTiShuLiang, KeChengID);
            PanSuiJiTiHao(PanDuanTiShuLiang, KeChengID);
            TianSuiJiTiHao(TianKongTiShuLiang, KeChengID);

            //想生成试卷就必须至少有一个题型有题目与分数
            if ((DanXuanTiShuLiang != 0 && DanXuanTiFenShu != 0) || (PanDuanTiShuLiang != 0 && PanDuanTiFenShu != 0) || (TianKongTiShuLiang != 0 && TianKongTiFenShu != 0))
            {
                if (XiaoShi != 0 || FenZhong != 0)//考试时长也必须要输入
                {
                    string SqlChaShiJuanName = string.Format(@"select count(*) from ShiJuanName where ShijuanName='{0}'and UserID={1} and KeChengID={2}",
                        ShiJuanName, UserIDandUserName.UserID, KeChengID);

                    int ShiJuanNameCount = (int)DBHepler.JuHe(SqlChaShiJuanName);

                    if (ShiJuanNameCount == 0)//查看试卷是否在数据库中存在，存在就没必要在次生成
                    {
                        try
                        {
                            string SqlShiJuanName = string.Format(@"insert into ShiJuanName values('{0}',{1},{2})",
                                ShiJuanName, UserIDandUserName.UserID, KeChengID);

                            if (DBHepler.ZSG(SqlShiJuanName))//试卷名称添加成功后在给对应的试卷添加试题
                            {
                                if (DanXuanTiShuLiang > 0)
                                {
                                    ShengChengDanXuanTi(DanXuanTiShuLiang, UserIDandUserName.UserName, DanXuanTiFenShu, ZongShiChang, KeChengID, ShiJuanName);
                                }

                                if (PanDuanTiShuLiang > 0)
                                {
                                    ShengChengPanDuanTi(PanDuanTiShuLiang, UserIDandUserName.UserName, PanDuanTiFenShu, ZongShiChang, KeChengID, ShiJuanName);
                                }


                                if (TianKongTiShuLiang > 0)
                                {
                                    ShengChengTianKongTi(TianKongTiShuLiang, UserIDandUserName.UserName, TianKongTiFenShu, ZongShiChang, KeChengID, ShiJuanName);
                                }

                                if (bo == true || bo_3 == true || bo_4 == true)
                                {
                                    MessageBox.Show("生成成功");
                                    ShiJuanShengChengBo = true;
                                }
                                else
                                {
                                    MessageBox.Show("生成失败");
                                }
                            }
                        }
                        catch
                        {
                            MessageBox.Show("该名称已存在!");
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("该试卷以存在");
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("请填写考试时长！");
                }
            }
            else
            {
                MessageBox.Show("请填写题目数量与分数！");
            }
        }

        bool ShiJuanShengChengBo = false;

        /// <summary>
        /// 添加选择题到对应的试卷
        /// </summary>
        void ShengChengDanXuanTi(int TiMuShuLiang, string UserName, double DanXuanTiFenShu, string ZongShiChang, int KeChengID, string ShiJuanName)
        {
            for (int i = 0; i < TiMuShuLiang; i++)
            {
                int Index = DanTiSuiJiShu[i];
                string TiMuName = DanXuanTiTa.Rows[Index]["TiMu"].ToString();
                string XuanXiangYi = DanXuanTiTa.Rows[Index]["XuanXiangYi"].ToString();
                string XuanXiangEr = DanXuanTiTa.Rows[Index]["XuanXiangEr"].ToString();
                string XuanXiangSan = DanXuanTiTa.Rows[Index]["XuanXiangSan"].ToString();
                string XuanXiangSi = DanXuanTiTa.Rows[Index]["XuanXiangSi"].ToString();
                string ZhengQueDaAn = DanXuanTiTa.Rows[Index]["ZhengQueDaAn"].ToString();
                //int KeChengID = (int)DanXuanTiTa.Rows[Index]["KeChengID"];
                int TiXingID = (int)DanXuanTiTa.Rows[Index]["TiXingID"];

                string InsertSql = string.Format(@"insert into ShiJuanDanXuanTi
                       (TiMu,XuanXiangYi,XuanXiangEr,XuanXiangSan,XuanXiangSi,ZhengQueDaAn,YongHuName,MeiTiFenShu,KaoShiShiChang,
                        KeChengID,TiXingID,ShiJuanName,UserID) 
                        values('{0}','{1}','{2}','{3}','{4}','{5}','{6}',{7},'{8}',{9},{10},'{11}',{12})",
                TiMuName, XuanXiangYi, XuanXiangEr, XuanXiangSan, XuanXiangSi, ZhengQueDaAn, UserName, DanXuanTiFenShu,
                ZongShiChang, KeChengID, TiXingID, ShiJuanName, UserIDandUserName.UserID);
                DBHepler.ZSG(InsertSql);
                bo = true;
            }
        }

        /// <summary>
        /// 添加判断题到对应的试卷
        /// </summary>
        void ShengChengPanDuanTi(int PanDuanTiShuLiang, string UserName, double PanDuanTiFenShu, string ZongShiChang, int KeChengID, string ShiJuanName)
        {
            for (int i = 0; i < PanDuanTiShuLiang; i++)
            {
                int Index = PanSuiJiShu[i];
                string TiMuName = PanDuanTiTa.Rows[Index]["TiMu"].ToString();
                string ZhengQueDaAn = PanDuanTiTa.Rows[Index]["ZhengQueDaAn"].ToString();
                int TiXingID = (int)PanDuanTiTa.Rows[Index]["TiXingID"];

                string InsertSql_3 = string.Format(@"insert into ShiJuanPanDuanTi (TiMu,ZhengQueDaAn,YongHuName,MeiTiFenShu,
                    KaoShiShiChang,KeChengID,TiXingID,ShiJuanName,UserID)
                    values('{0}','{1}','{2}','{3}','{4}',{5},{6},'{7}',{8})",
                TiMuName, ZhengQueDaAn, UserName, PanDuanTiFenShu, ZongShiChang, KeChengID, TiXingID, ShiJuanName, UserIDandUserName.UserID);
                DBHepler.ZSG(InsertSql_3);
                bo_3 = true;
            }
        }

        /// <summary>
        /// 添加填空题到对应的试卷
        /// </summary>
        void ShengChengTianKongTi(int TianKongTiShuLiang, string UserName, double TianKongTiFenShu, string ZongShiChang, int KeChengID, string ShiJuanName)
        {
            for (int i = 0; i < TianKongTiShuLiang; i++)
            {
                int Index = TianSuiJiShu[i];
                string TiMuName = TianKongTiTa.Rows[Index]["TiMu"].ToString();
                string ZhengQueDaAn = TianKongTiTa.Rows[Index]["ZhengQueDaAn"].ToString();
                int TiXingID = (int)TianKongTiTa.Rows[Index]["TiXingID"];

                string InsertSql_4 = string.Format(@"insert into ShiJuanTianKongTi (TiMu,ZhengQueDaAn,YongHuName,MeiTiFenShu,
                    KaoShiShiChang,KeChengID,TiXingID,ShiJuanName,UserID)
                    values('{0}','{1}','{2}','{3}','{4}',{5},{6},'{7}',{8})",
                TiMuName, ZhengQueDaAn, UserName, TianKongTiFenShu, ZongShiChang, KeChengID, TiXingID, ShiJuanName, UserIDandUserName.UserID);
                DBHepler.ZSG(InsertSql_4);
                bo_4 = true;
            }
        }

        /// <summary>
        /// 获取国定数量的随机题号
        /// </summary>
        /// <param name="TiMuShuLiang"></param>
        void DanSuiJiTiHao(int TiMuShuLiang, int KeChengID)
        {
            string DanXuanTiCount = string.Format(@"select count(*) from DanXuanTi where KeChengID={0} and UserID={1}",
                KeChengID, UserIDandUserName.UserID);
            int DXCount = (int)DBHepler.JuHe(DanXuanTiCount);
            if (TiMuShuLiang > 0)
            {
                DanTiSuiJiShu = SuiJiTiMu(DXCount, TiMuShuLiang);
            }
        }

        void PanSuiJiTiHao(int TiMuShuLiang, int KeChengID)
        {
            string PanDuanTiCount = string.Format(@"select count(*) from PanDuanTi where KeChengID={0} and UserID={1}",
                KeChengID, UserIDandUserName.UserID);
            int PanCount = (int)DBHepler.JuHe(PanDuanTiCount);
            if (TiMuShuLiang > 0)
            {
                PanSuiJiShu = SuiJiTiMu(PanCount, TiMuShuLiang);
            }
        }

        void TianSuiJiTiHao(int TiMuShuLiang, int KeChengID)
        {
            string TianKongTiCount = string.Format(@"select count(*) from TianKongTi where KeChengID={0} and UserID={1}",
                KeChengID, UserIDandUserName.UserID);
            int TianCount = (int)DBHepler.JuHe(TianKongTiCount);
            if (TiMuShuLiang > 0)
            {
                TianSuiJiShu = SuiJiTiMu(TianCount, TiMuShuLiang);
            }
        }

        int[] DanTiSuiJiShu;
        int[] PanSuiJiShu;
        int[] TianSuiJiShu;

        System.Data.DataTable DanXuanTiTa;
        void ChaDanXuanTiBiao(int KeChengID)
        {
            string DanXuanTiSqlTa = string.Format(@"select * from DanXuanTi where KeChengID={0} and UserID={1}",
                KeChengID, UserIDandUserName.UserID);
            DanXuanTiTa = DBHepler.Cha(DanXuanTiSqlTa);
        }
        System.Data.DataTable TianKongTiTa;
        void ChaTianKongTiBiao(int KeChengID)
        {
            string TianKongTiSqlTa = string.Format(@"select * from TianKongTi where KeChengID={0} and UserID={1}",
                KeChengID, UserIDandUserName.UserID);
            TianKongTiTa = DBHepler.Cha(TianKongTiSqlTa);
        }
        System.Data.DataTable PanDuanTiTa;
        void ChaPanDuanTiBiao(int KeChengID)
        {
            string PanDuanTiSqlTa = string.Format(@"select * from PanDuanTi where KeChengID={0} and UserID={1}",
                KeChengID, UserIDandUserName.UserID);
            PanDuanTiTa = DBHepler.Cha(PanDuanTiSqlTa);
        }

        /// <summary>
        /// 生成不重复随机题号
        /// </summary>
        private int[] SuiJiTiMu(int TaRowCount, int TiMuGeShu)
        {
            Random SuiJiShu = new Random();
            int sj = 0;
            int[] CFSuiJiShu = new int[TaRowCount];
            int[] ShangYiTiIndex = new int[TiMuGeShu];
            int sytIndex = 0;
            for (; ; )
            {
                //用当前随机数与当前不重复数组的下标中的数字相比较，如果相同那么就重新生成一个随机数
                //直到产生不重复随机数的个数与数据表中的行数相等时，才停止生成随机数
                if (sj != CFSuiJiShu[sj])
                {
                    CFSuiJiShu[sj] = sj;
                    ShangYiTiIndex[sytIndex] = sj;
                    sytIndex++;
                }
                else
                {
                    sj = SuiJiShu.Next(0, TaRowCount);
                }
                if (sytIndex == TiMuGeShu - 1)
                {
                    return ShangYiTiIndex;
                }
            }
        }


        private void ShengChengShiJuan_Load(object sender, EventArgs e)
        {
            string SqlKeCheng = string.Format(@"select * from KeCheng where UserID={0}", UserIDandUserName.UserID);
            System.Data.DataTable KeChengTa = DBHepler.Cha(SqlKeCheng);
            comKeChengName.DataSource = KeChengTa;
            comKeChengName.ValueMember = "KeChengID";
            comKeChengName.DisplayMember = "KeChengName";
        }

        string KuaiShiJuanName;
        bool KuaiSuShengChengBo = false;
        private void ButKuaiSuShengCheng_Click(object sender, EventArgs e)
        {
            KuaiShiJuanName = txtKuaiSuShiJuanName.Text;
            if (KuaiShiJuanName == "")
            {
                MessageBox.Show("请输入试卷名称！");
                return;
            }
            int KuaiSuKeChengID = (int)comKuaiSuKeCheng.SelectedValue;

            ChaDanXuanTiBiao(KuaiSuKeChengID);
            ChaTianKongTiBiao(KuaiSuKeChengID);
            ChaPanDuanTiBiao(KuaiSuKeChengID);


            if (DanXuanTiTa.Rows.Count > 30 && PanDuanTiTa.Rows.Count > 20 && TianKongTiTa.Rows.Count > 10)
            {
                DanSuiJiTiHao(30, KuaiSuKeChengID);
                PanSuiJiTiHao(20, KuaiSuKeChengID);
                TianSuiJiTiHao(10, KuaiSuKeChengID);
            }
            else
            {
                MessageBox.Show("题库题目过少不能进行快速生成！");
                return;
            }

            try
            {
                string SqlChaShiJuanName = string.Format(@"select count(*) from ShiJuanName where ShijuanName='{0}' and KeChengID={1} and UserID={1}", 
                    KuaiShiJuanName, KuaiSuKeChengID, UserIDandUserName.UserID);
                int ShiJuanNameCount = (int)DBHepler.JuHe(SqlChaShiJuanName);
                if (ShiJuanNameCount == 0)
                {
                    string SqlShiJuanName = string.Format(@"insert into ShiJuanName values('{0}',{1},{2})", KuaiShiJuanName, UserIDandUserName.UserID, KuaiSuKeChengID);
                    if (DBHepler.ZSG(SqlShiJuanName))
                    {
                        ShengChengDanXuanTi(30, UserIDandUserName.UserName, 2, "1小时30分", KuaiSuKeChengID, KuaiShiJuanName);
                        ShengChengPanDuanTi(20, UserIDandUserName.UserName, 1, "1小时30分", KuaiSuKeChengID, KuaiShiJuanName);
                        ShengChengTianKongTi(10, UserIDandUserName.UserName, 2, "1小时30分", KuaiSuKeChengID, KuaiShiJuanName);
                        if (bo == true || bo_3 == true || bo_4 == true)
                        {
                            MessageBox.Show("快速生成成功");
                            KuaiSuShengChengBo = true;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("该试卷以存在");
                }
            }
            catch
            {
                MessageBox.Show("该试卷以存在");
            }
        }

        private void TabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int TabIndex = tabControl1.SelectedIndex;
            if (TabIndex == 0)
            {
                string SqlKeCheng = string.Format(@"select * from KeCheng where UserID={0}", UserIDandUserName.UserID);
                DataTable KeChengTa = DBHepler.Cha(SqlKeCheng);
                comKeChengName.DataSource = KeChengTa;
                comKeChengName.ValueMember = "KeChengID";
                comKeChengName.DisplayMember = "KeChengName";
            }
            else if (TabIndex == 1)
            {
                string SqlKeCheng = string.Format(@"select * from KeCheng where UserID={0}", UserIDandUserName.UserID);
                DataTable KeChengTa = DBHepler.Cha(SqlKeCheng);
                comKuaiSuKeCheng.DataSource = KeChengTa;
                comKuaiSuKeCheng.ValueMember = "KeChengID";
                comKuaiSuKeCheng.DisplayMember = "KeChengName";
            }
        }

        #region 创建word试卷(直接导出为word)
        /// <summary>
        /// 创建word试卷
        /// </summary>
        /// <param name="PathName"></param>
        public void CreateWordFile(string PathName, string ShiJuanName, string DanChaSQL, string PanChaSQL, string TianChaSQL, int DanTiShuLiang, int PanTiShuLiang, int TianTiShuLiang, double XuanZeTiFenShu, double PanDuanFenShu, double TianFenShu, double ZongFen, string ZongShiChang, int KeChengID)
        {
            try
            {
                object filename = PathName;  //文件保存路径  
                object count1;
                object WdLine1;//换一行;
                Object Nothing = Missing.Value;

                //创建Word文档    
                Application WordApp = new Application();
                Document WordDoc = WordApp.Documents.Add(ref Nothing, ref Nothing, ref Nothing, ref Nothing);

                //1.页面设置
                WordDoc.PageSetup.PaperSize = MSWord.WdPaperSize.wdPaperA4;
                WordDoc.PageSetup.Orientation = MSWord.WdOrientation.wdOrientPortrait;
                WordDoc.PageSetup.TopMargin = 10f;
                WordDoc.PageSetup.BottomMargin = 10f;
                WordDoc.PageSetup.LeftMargin = 10f;
                WordDoc.PageSetup.RightMargin = 10f;
                WordDoc.PageSetup.HeaderDistance = 10f;

                #region   添加页眉
                //2.添加页眉
                //WordApp.ActiveWindow.View.Type = Microsoft.Office.Interop.Word.WdViewType.wdOutlineView;//视图样式
                //WordApp.ActiveWindow.View.SeekView = Microsoft.Office.Interop.Word.WdSeekView.wdSeekPrimaryHeader;//进入页眉设置，其中页眉边距在页面设置中已完成
                ////WordApp.ActiveWindow.ActivePane.Selection.InsertAfter("考试试卷");
                //WordApp.Selection.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphRight;//设置右对齐
                ////WordApp.ActiveWindow.View.SeekView = WdSeekView.wdSeekMainDocument;//跳出页眉设置
                //WordApp.Selection.ParagraphFormat.LineSpacing = 1f;//设置文档的行间距

                //去掉页眉的横线
                //WordApp.ActiveWindow.ActivePane.Selection.ParagraphFormat.Borders[Microsoft.Office.Interop.Word.WdBorderType.wdBorderBottom].LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleNone;
                //WordApp.ActiveWindow.ActivePane.Selection.Borders[Microsoft.Office.Interop.Word.WdBorderType.wdBorderBottom].Visible = false;
                //WordApp.ActiveWindow.ActivePane.View.SeekView = Microsoft.Office.Interop.Word.WdSeekView.wdSeekMainDocument;//退出页眉设置
                #endregion


                WordApp.Selection.Font.Size = 20;
                WordApp.Selection.Font.Bold = 1; // 黑体
                WordApp.Selection.TypeText("                        " + ShiJuanName);
                count1 = 30;
                WdLine1 = WdUnits.wdLine;//换一行;
                WordApp.Selection.MoveDown(ref WdLine1, ref count1, ref Nothing);//移动焦点
                WordApp.Selection.TypeParagraph();//插入段落

                WordApp.Selection.TypeParagraph();//插入段落


                WordApp.Selection.Font.Size = 15;
                WordApp.Selection.Font.Bold = 1; // 黑体
                WordApp.Selection.TypeText("姓名：  " + "总分： " + ZongFen + "   考试时长：" + ZongShiChang);


                WordApp.Selection.TypeParagraph();//插入段落

                //选择题
                DataTable DanXuanTiDaTb = DBHepler.Cha(DanChaSQL);
                if (DanXuanTiDaTb.Rows.Count > 0 && DanTiShuLiang > 0)
                {
                    WordApp.Selection.Font.Size = 20;
                    WordApp.Selection.Font.Bold = 1; // 黑体

                    WordApp.Selection.TypeText("一.选择题");

                    count1 = 30;
                    WdLine1 = WdUnits.wdLine;//换一行;
                    WordApp.Selection.MoveDown(ref WdLine1, ref count1, ref Nothing);//移动焦点
                    WordApp.Selection.TypeParagraph();//插入段落

                    DanShiJuanSuiJiTiHao(DanTiShuLiang, KeChengID, ShiJuanName);
                    for (int i = 0; i < DanTiShuLiang; i++)
                    {
                        int Index = DanShiJuanTiHaoSuiJiShu[i];
                        int a = i + 1;


                        WordApp.Selection.Font.Size = 15;
                        WordApp.Selection.Font.Bold = 1; // 黑体

                        string TiMu = DanXuanTiDaTb.Rows[Index]["TiMu"].ToString();
                        WordApp.Selection.TypeText(a + "." + TiMu + " (" + XuanZeTiFenShu + ")分");

                        count1 = 30;
                        WdLine1 = WdUnits.wdLine;//换一行;
                        WordApp.Selection.MoveDown(ref WdLine1, ref count1, ref Nothing);//移动焦点
                        WordApp.Selection.TypeParagraph();//插入段落



                        WordApp.Selection.Font.Size = 15;
                        WordApp.Selection.Font.Bold = 1; // 黑体

                        string DaAnA = DanXuanTiDaTb.Rows[Index]["XuanXiangYi"].ToString();
                        WordApp.Selection.TypeText("  " + " (" + 1 + ")" + "." + DaAnA);

                        count1 = 30;
                        WdLine1 = WdUnits.wdLine;//换一行;
                        WordApp.Selection.MoveDown(ref WdLine1, ref count1, ref Nothing);//移动焦点
                        WordApp.Selection.TypeParagraph();//插入段落



                        WordApp.Selection.Font.Size = 15;
                        WordApp.Selection.Font.Bold = 1; // 黑体

                        string DaAnB = DanXuanTiDaTb.Rows[Index]["XuanXiangEr"].ToString();
                        WordApp.Selection.TypeText("  " + " (" + 2 + ")" + "." + DaAnB);

                        count1 = 30;
                        WdLine1 = WdUnits.wdLine;//换一行;
                        WordApp.Selection.MoveDown(ref WdLine1, ref count1, ref Nothing);//移动焦点
                        WordApp.Selection.TypeParagraph();//插入段落


                        WordApp.Selection.Font.Size = 15;
                        WordApp.Selection.Font.Bold = 1; // 黑体

                        string DaAnC = DanXuanTiDaTb.Rows[Index]["XuanXiangSan"].ToString();
                        WordApp.Selection.TypeText("  " + " (" + 3 + ")" + "." + DaAnC);

                        count1 = 30;
                        WdLine1 = WdUnits.wdLine;//换一行;
                        WordApp.Selection.MoveDown(ref WdLine1, ref count1, ref Nothing);//移动焦点
                        WordApp.Selection.TypeParagraph();//插入段落


                        WordApp.Selection.Font.Size = 15;
                        WordApp.Selection.Font.Bold = 1; // 黑体

                        string DaAnD = DanXuanTiDaTb.Rows[Index]["XuanXiangSi"].ToString();
                        if (DaAnD != "")//如果没有选项四那么就不打印
                        {
                            WordApp.Selection.TypeText("  " + " (" + 4 + ")" + "." + DaAnD);

                            count1 = 30;
                            WdLine1 = WdUnits.wdLine;//换一行;
                            WordApp.Selection.MoveDown(ref WdLine1, ref count1, ref Nothing);//移动焦点
                            WordApp.Selection.TypeParagraph();//插入段落
                        }
                        WordApp.Selection.TypeParagraph();//插入段落

                    }
                }


                //判断题
                DataTable PanDuanTiDaTb = DBHepler.Cha(PanChaSQL);
                if (PanDuanTiDaTb.Rows.Count > 0 && PanTiShuLiang > 0)
                {

                    WordApp.Selection.Font.Size = 20;
                    WordApp.Selection.Font.Bold = 1; // 黑体

                    WordApp.Selection.TypeText("二.判断题");

                    count1 = 30;
                    WdLine1 = WdUnits.wdLine;//换一行;
                    WordApp.Selection.MoveDown(ref WdLine1, ref count1, ref Nothing);//移动焦点
                    WordApp.Selection.TypeParagraph();//插入段落

                    PanShiJuanSuiJiTiHao(PanTiShuLiang, KeChengID, ShiJuanName);
                    for (int i = 0; i < PanTiShuLiang; i++)
                    {
                        int PanIndex = PanShiJuanTiHaoSuiJiShu[i];
                        string PanDuanTiMu = PanDuanTiDaTb.Rows[PanIndex]["TiMu"].ToString();
                        int TiHao = i + 1;

                        WordApp.Selection.Font.Size = 15;
                        WordApp.Selection.Font.Bold = 1; // 黑体

                        WordApp.Selection.TypeText(TiHao + "." + PanDuanTiMu + " (" + PanDuanFenShu + ")分");


                        count1 = 30;
                        WdLine1 = WdUnits.wdLine;//换一行;
                        WordApp.Selection.MoveDown(ref WdLine1, ref count1, ref Nothing);//移动焦点
                        WordApp.Selection.TypeParagraph();//插入段落
                    }
                    WordApp.Selection.TypeParagraph();//插入段落
                }


                //填空题
                DataTable TianKongTiDatb = DBHepler.Cha(TianChaSQL);
                if (TianKongTiDatb.Rows.Count > 0 && TianTiShuLiang > 0)
                {

                    WordApp.Selection.Font.Size = 20;
                    WordApp.Selection.Font.Bold = 1; // 黑体

                    WordApp.Selection.TypeText("三.填空题");

                    count1 = 30;
                    WdLine1 = WdUnits.wdLine;//换一行;
                    WordApp.Selection.MoveDown(ref WdLine1, ref count1, ref Nothing);//移动焦点
                    WordApp.Selection.TypeParagraph();//插入段落

                    TianShiJuanSuiJiTiHao(TianTiShuLiang, KeChengID, ShiJuanName);
                    for (int i = 0; i < TianTiShuLiang; i++)
                    {
                        int TianIndex = TianShiJuanTiHaoSuiJiShu[i];
                        string TianKongTiMu = TianKongTiDatb.Rows[TianIndex]["TiMu"].ToString();
                        int TiHao = i + 1;


                        WordApp.Selection.Font.Size = 15;
                        WordApp.Selection.Font.Bold = 1; // 黑体

                        WordApp.Selection.TypeText(TiHao + "." + TianKongTiMu + " (" + TianFenShu + ")分");

                        count1 = 30;
                        WdLine1 = WdUnits.wdLine;//换一行;
                        WordApp.Selection.MoveDown(ref WdLine1, ref count1, ref Nothing);//移动焦点
                        WordApp.Selection.TypeParagraph();//插入段落
                    }
                    WordApp.Selection.TypeParagraph();//插入段落
                }

                WordDoc.Paragraphs.Last.Range.Text = "试卷创建时间：" + DateTime.Now.ToString();//“落款”
                WordDoc.Paragraphs.Last.Alignment = WdParagraphAlignment.wdAlignParagraphRight;

                //试卷保存
                WordDoc.SaveAs(ref filename, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing); MessageBox.Show("导出成功!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                WordDoc.Close(ref Nothing, ref Nothing, ref Nothing);
                WordApp.Quit(ref Nothing, ref Nothing, ref Nothing);
            }
            catch (Exception ex)
            {
                MessageBox.Show("导出失败!" + "\n" + ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        int[] DanShiJuanTiHaoSuiJiShu;
        int[] PanShiJuanTiHaoSuiJiShu;
        int[] TianShiJuanTiHaoSuiJiShu;

        /// <summary>
        /// 获取试卷中国定数量的题目随机数
        /// </summary>
        /// <param name="TiMuShuLiang"></param>
        void DanShiJuanSuiJiTiHao(int TiMuShuLiang, int KeChengID, string ShiJuanName)
        {
            string DanXuanTiCount = string.Format(@"select count(*) from ShiJuanDanXuanTi where UserID={0} and KeChengID={1} and ShiJuanName='{2}'", UserIDandUserName.UserID, KeChengID, ShiJuanName);
            int DXCount = (int)DBHepler.JuHe(DanXuanTiCount);
            if (TiMuShuLiang > 0)
            {
                DanShiJuanTiHaoSuiJiShu = SuiJiTiMu(DXCount, TiMuShuLiang);
            }
        }

        void PanShiJuanSuiJiTiHao(int TiMuShuLiang, int KeChengID, string ShiJuanName)
        {
            string DanXuanTiCount = string.Format(@"select count(*) from ShiJuanPanDuanTi where UserID={0} and KeChengID={1} and ShiJuanName='{2}'", UserIDandUserName.UserID, KeChengID, ShiJuanName);
            int DXCount = (int)DBHepler.JuHe(DanXuanTiCount);
            if (TiMuShuLiang > 0)
            {
                PanShiJuanTiHaoSuiJiShu = SuiJiTiMu(DXCount, TiMuShuLiang);
            }
        }

        void TianShiJuanSuiJiTiHao(int TiMuShuLiang, int KeChengID, string ShiJuanName)
        {
            string DanXuanTiCount = string.Format(@"select count(*) from ShiJuanTianKongTi where UserID={0} and KeChengID={1} and ShiJuanName='{2}'", UserIDandUserName.UserID, KeChengID, ShiJuanName);
            int DXCount = (int)DBHepler.JuHe(DanXuanTiCount);
            if (TiMuShuLiang > 0)
            {
                TianShiJuanTiHaoSuiJiShu = SuiJiTiMu(DXCount, TiMuShuLiang);
            }
        }

        private void ButDaoRuWord_Click(object sender, EventArgs e)
        {
            if (ShiJuanShengChengBo == true)
            {
                int KeChengID = (int)comKeChengName.SelectedValue;//课程ID

                double ZongFen = 0;
                if (DanXuanTiShuLiang != 0)
                {
                    ZongFen += DanXuanTiShuLiang * DanXuanTiFenShu;
                }
                if (PanDuanTiFenShu != 0)
                {
                    ZongFen += PanDuanTiShuLiang * PanDuanTiFenShu;
                }
                if (TianKongTiShuLiang != 0)
                {
                    ZongFen += TianKongTiShuLiang * TianKongTiFenShu;
                }

                string LuJing = "D:\\" + ShiJuanName + ".doc";
                string SqlDanXuanTi = string.Format(@"select * from ShiJuanDanXuanTi where UserID={0} and KeChengID={1} and ShiJuanName='{2}'", UserIDandUserName.UserID, KeChengID, ShiJuanName);
                string SqlPanDuanTi = string.Format(@"select * from ShiJuanPanDuanTi where UserID={0} and KeChengID={1} and ShiJuanName='{2}'", UserIDandUserName.UserID, KeChengID, ShiJuanName);
                string SqlTianKongTi = string.Format(@"select * from ShiJuanTianKongTi where UserID={0} and KeChengID={1} and ShiJuanName='{2}'", UserIDandUserName.UserID, KeChengID, ShiJuanName);
                CreateWordFile(LuJing, ShiJuanName, SqlDanXuanTi, SqlPanDuanTi, SqlTianKongTi, DanXuanTiShuLiang, PanDuanTiShuLiang, TianKongTiShuLiang, DanXuanTiFenShu, PanDuanTiFenShu, TianKongTiFenShu, ZongFen, ZongShiChang, KeChengID);
            }
            else
            {
                MessageBox.Show("请先 生成试卷在对其操作！");
            }
        }

        /// <summary>
        /// 快速导入固定类型试卷
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButKuaiSuDaoRuWord_Click(object sender, EventArgs e)
        {
            if (KuaiSuShengChengBo == true)
            {
                int KuaiSuKeChengID = (int)comKuaiSuKeCheng.SelectedValue;

                string LuJing = "D:\\" + KuaiShiJuanName + ".doc";
                string SqlDanXuanTi = string.Format(@"select * from ShiJuanDanXuanTi where UserID={0} and KeChengID={1} and ShiJuanName='{2}'", UserIDandUserName.UserID, KuaiSuKeChengID, KuaiShiJuanName);
                string SqlPanDuanTi = string.Format(@"select * from ShiJuanPanDuanTi where UserID={0} and KeChengID={1} and ShiJuanName='{2}'", UserIDandUserName.UserID, KuaiSuKeChengID, KuaiShiJuanName);
                string SqlTianKongTi = string.Format(@"select * from ShiJuanTianKongTi where UserID={0} and KeChengID={1} and ShiJuanName='{2}'", UserIDandUserName.UserID, KuaiSuKeChengID, KuaiShiJuanName);
                CreateWordFile(LuJing, KuaiShiJuanName, SqlDanXuanTi, SqlPanDuanTi, SqlTianKongTi, 30, 20, 10, 2, 1, 2, 100, "1小时30分钟", KuaiSuKeChengID);
            }
            else
            {
                MessageBox.Show("请先快速生成试卷在对其进行操作！");
            }
        }

        private void ButQuXiao_Click(object sender, EventArgs e)
        {
            foreach (Control Ct in tabShengChengShiJuan.Controls)
            {
                if (Ct is TextBox)
                {
                    Ct.Text = "";
                }
            }
        }
    }
}
