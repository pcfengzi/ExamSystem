﻿namespace KaoShiXiTong
{
    partial class ShengChengShiJuan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtShiJuanName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comKeChengName = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDanXuanTiShuLiang = new System.Windows.Forms.TextBox();
            this.txtPanDuanTiShuLiang = new System.Windows.Forms.TextBox();
            this.txtTianKongTiShuLiang = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDanXuanTiFenShu = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPanDuanTiFenShu = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTianKongTiFenShu = new System.Windows.Forms.TextBox();
            this.butQuXiao = new System.Windows.Forms.Button();
            this.butShengChengShiJuan = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.txtXiaoShi = new System.Windows.Forms.TextBox();
            this.txtFenZhong = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabShengChengShiJuan = new System.Windows.Forms.TabPage();
            this.butDaoRuWord = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.butKuaiSuDaoRuWord = new System.Windows.Forms.Button();
            this.butKuaiSuShengCheng = new System.Windows.Forms.Button();
            this.comKuaiSuKeCheng = new System.Windows.Forms.ComboBox();
            this.txtKuaiSuShiJuanName = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabShengChengShiJuan.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(231, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "试卷名称";
            // 
            // txtShiJuanName
            // 
            this.txtShiJuanName.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtShiJuanName.Location = new System.Drawing.Point(320, 35);
            this.txtShiJuanName.Name = "txtShiJuanName";
            this.txtShiJuanName.Size = new System.Drawing.Size(121, 23);
            this.txtShiJuanName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(231, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "所属课程";
            // 
            // comKeChengName
            // 
            this.comKeChengName.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comKeChengName.FormattingEnabled = true;
            this.comKeChengName.Location = new System.Drawing.Point(320, 79);
            this.comKeChengName.Name = "comKeChengName";
            this.comKeChengName.Size = new System.Drawing.Size(121, 25);
            this.comKeChengName.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(158, 139);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "单选题数量";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(158, 193);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "判断题数量";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(158, 246);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "填空题数量";
            // 
            // txtDanXuanTiShuLiang
            // 
            this.txtDanXuanTiShuLiang.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtDanXuanTiShuLiang.Location = new System.Drawing.Point(240, 136);
            this.txtDanXuanTiShuLiang.Name = "txtDanXuanTiShuLiang";
            this.txtDanXuanTiShuLiang.Size = new System.Drawing.Size(100, 23);
            this.txtDanXuanTiShuLiang.TabIndex = 1;
            // 
            // txtPanDuanTiShuLiang
            // 
            this.txtPanDuanTiShuLiang.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtPanDuanTiShuLiang.Location = new System.Drawing.Point(240, 190);
            this.txtPanDuanTiShuLiang.Name = "txtPanDuanTiShuLiang";
            this.txtPanDuanTiShuLiang.Size = new System.Drawing.Size(100, 23);
            this.txtPanDuanTiShuLiang.TabIndex = 1;
            // 
            // txtTianKongTiShuLiang
            // 
            this.txtTianKongTiShuLiang.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtTianKongTiShuLiang.Location = new System.Drawing.Point(240, 243);
            this.txtTianKongTiShuLiang.Name = "txtTianKongTiShuLiang";
            this.txtTianKongTiShuLiang.Size = new System.Drawing.Size(100, 23);
            this.txtTianKongTiShuLiang.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(391, 139);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "每题分数";
            // 
            // txtDanXuanTiFenShu
            // 
            this.txtDanXuanTiFenShu.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtDanXuanTiFenShu.Location = new System.Drawing.Point(458, 136);
            this.txtDanXuanTiFenShu.Name = "txtDanXuanTiFenShu";
            this.txtDanXuanTiFenShu.Size = new System.Drawing.Size(100, 23);
            this.txtDanXuanTiFenShu.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(391, 193);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 17);
            this.label7.TabIndex = 0;
            this.label7.Text = "每题分数";
            // 
            // txtPanDuanTiFenShu
            // 
            this.txtPanDuanTiFenShu.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtPanDuanTiFenShu.Location = new System.Drawing.Point(460, 190);
            this.txtPanDuanTiFenShu.Name = "txtPanDuanTiFenShu";
            this.txtPanDuanTiFenShu.Size = new System.Drawing.Size(100, 23);
            this.txtPanDuanTiFenShu.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(391, 246);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 17);
            this.label8.TabIndex = 0;
            this.label8.Text = "每题分数";
            // 
            // txtTianKongTiFenShu
            // 
            this.txtTianKongTiFenShu.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtTianKongTiFenShu.Location = new System.Drawing.Point(460, 243);
            this.txtTianKongTiFenShu.Name = "txtTianKongTiFenShu";
            this.txtTianKongTiFenShu.Size = new System.Drawing.Size(100, 23);
            this.txtTianKongTiFenShu.TabIndex = 1;
            // 
            // butQuXiao
            // 
            this.butQuXiao.BackColor = System.Drawing.Color.Gainsboro;
            this.butQuXiao.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butQuXiao.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butQuXiao.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.butQuXiao.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.butQuXiao.Location = new System.Drawing.Point(485, 356);
            this.butQuXiao.Name = "butQuXiao";
            this.butQuXiao.Size = new System.Drawing.Size(75, 23);
            this.butQuXiao.TabIndex = 3;
            this.butQuXiao.Text = "清空";
            this.butQuXiao.UseVisualStyleBackColor = false;
            this.butQuXiao.Click += new System.EventHandler(this.ButQuXiao_Click);
            // 
            // butShengChengShiJuan
            // 
            this.butShengChengShiJuan.BackColor = System.Drawing.Color.Gainsboro;
            this.butShengChengShiJuan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butShengChengShiJuan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butShengChengShiJuan.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.butShengChengShiJuan.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.butShengChengShiJuan.Location = new System.Drawing.Point(197, 356);
            this.butShengChengShiJuan.Name = "butShengChengShiJuan";
            this.butShengChengShiJuan.Size = new System.Drawing.Size(87, 23);
            this.butShengChengShiJuan.TabIndex = 3;
            this.butShengChengShiJuan.Text = "生成试卷";
            this.butShengChengShiJuan.UseVisualStyleBackColor = false;
            this.butShengChengShiJuan.Click += new System.EventHandler(this.ButShengChengShiJuan_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.Location = new System.Drawing.Point(266, 298);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 17);
            this.label9.TabIndex = 4;
            this.label9.Text = "考试时长";
            // 
            // txtXiaoShi
            // 
            this.txtXiaoShi.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtXiaoShi.Location = new System.Drawing.Point(325, 293);
            this.txtXiaoShi.Name = "txtXiaoShi";
            this.txtXiaoShi.Size = new System.Drawing.Size(38, 23);
            this.txtXiaoShi.TabIndex = 1;
            // 
            // txtFenZhong
            // 
            this.txtFenZhong.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtFenZhong.Location = new System.Drawing.Point(413, 293);
            this.txtFenZhong.Name = "txtFenZhong";
            this.txtFenZhong.Size = new System.Drawing.Size(38, 23);
            this.txtFenZhong.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.Location = new System.Drawing.Point(373, 298);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 17);
            this.label10.TabIndex = 5;
            this.label10.Text = "小时";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label11.Location = new System.Drawing.Point(461, 298);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(32, 17);
            this.label11.TabIndex = 5;
            this.label11.Text = "分钟";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabShengChengShiJuan);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(18, 15);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(706, 476);
            this.tabControl1.TabIndex = 7;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.TabControl1_SelectedIndexChanged);
            // 
            // tabShengChengShiJuan
            // 
            this.tabShengChengShiJuan.BackColor = System.Drawing.Color.Transparent;
            this.tabShengChengShiJuan.Controls.Add(this.butDaoRuWord);
            this.tabShengChengShiJuan.Controls.Add(this.comKeChengName);
            this.tabShengChengShiJuan.Controls.Add(this.label1);
            this.tabShengChengShiJuan.Controls.Add(this.label11);
            this.tabShengChengShiJuan.Controls.Add(this.label2);
            this.tabShengChengShiJuan.Controls.Add(this.label10);
            this.tabShengChengShiJuan.Controls.Add(this.label3);
            this.tabShengChengShiJuan.Controls.Add(this.label9);
            this.tabShengChengShiJuan.Controls.Add(this.label6);
            this.tabShengChengShiJuan.Controls.Add(this.butShengChengShiJuan);
            this.tabShengChengShiJuan.Controls.Add(this.butQuXiao);
            this.tabShengChengShiJuan.Controls.Add(this.label4);
            this.tabShengChengShiJuan.Controls.Add(this.txtFenZhong);
            this.tabShengChengShiJuan.Controls.Add(this.label7);
            this.tabShengChengShiJuan.Controls.Add(this.txtXiaoShi);
            this.tabShengChengShiJuan.Controls.Add(this.label5);
            this.tabShengChengShiJuan.Controls.Add(this.txtTianKongTiShuLiang);
            this.tabShengChengShiJuan.Controls.Add(this.label8);
            this.tabShengChengShiJuan.Controls.Add(this.txtPanDuanTiShuLiang);
            this.tabShengChengShiJuan.Controls.Add(this.txtShiJuanName);
            this.tabShengChengShiJuan.Controls.Add(this.txtDanXuanTiFenShu);
            this.tabShengChengShiJuan.Controls.Add(this.txtDanXuanTiShuLiang);
            this.tabShengChengShiJuan.Controls.Add(this.txtTianKongTiFenShu);
            this.tabShengChengShiJuan.Controls.Add(this.txtPanDuanTiFenShu);
            this.tabShengChengShiJuan.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabShengChengShiJuan.Location = new System.Drawing.Point(4, 22);
            this.tabShengChengShiJuan.Name = "tabShengChengShiJuan";
            this.tabShengChengShiJuan.Padding = new System.Windows.Forms.Padding(3);
            this.tabShengChengShiJuan.Size = new System.Drawing.Size(698, 450);
            this.tabShengChengShiJuan.TabIndex = 0;
            this.tabShengChengShiJuan.Text = "生成试卷";
            // 
            // butDaoRuWord
            // 
            this.butDaoRuWord.BackColor = System.Drawing.Color.Gainsboro;
            this.butDaoRuWord.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butDaoRuWord.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butDaoRuWord.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.butDaoRuWord.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.butDaoRuWord.Location = new System.Drawing.Point(334, 356);
            this.butDaoRuWord.Name = "butDaoRuWord";
            this.butDaoRuWord.Size = new System.Drawing.Size(107, 23);
            this.butDaoRuWord.TabIndex = 6;
            this.butDaoRuWord.Text = "导出word文档";
            this.butDaoRuWord.UseVisualStyleBackColor = false;
            this.butDaoRuWord.Click += new System.EventHandler(this.ButDaoRuWord_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Transparent;
            this.tabPage2.Controls.Add(this.butKuaiSuDaoRuWord);
            this.tabPage2.Controls.Add(this.butKuaiSuShengCheng);
            this.tabPage2.Controls.Add(this.comKuaiSuKeCheng);
            this.tabPage2.Controls.Add(this.txtKuaiSuShiJuanName);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(698, 450);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "快速生成试卷";
            // 
            // butKuaiSuDaoRuWord
            // 
            this.butKuaiSuDaoRuWord.BackColor = System.Drawing.Color.Gainsboro;
            this.butKuaiSuDaoRuWord.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butKuaiSuDaoRuWord.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.butKuaiSuDaoRuWord.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.butKuaiSuDaoRuWord.Location = new System.Drawing.Point(357, 265);
            this.butKuaiSuDaoRuWord.Name = "butKuaiSuDaoRuWord";
            this.butKuaiSuDaoRuWord.Size = new System.Drawing.Size(95, 23);
            this.butKuaiSuDaoRuWord.TabIndex = 4;
            this.butKuaiSuDaoRuWord.Text = "导出word文档";
            this.butKuaiSuDaoRuWord.UseVisualStyleBackColor = false;
            this.butKuaiSuDaoRuWord.Click += new System.EventHandler(this.ButKuaiSuDaoRuWord_Click);
            // 
            // butKuaiSuShengCheng
            // 
            this.butKuaiSuShengCheng.BackColor = System.Drawing.Color.Gainsboro;
            this.butKuaiSuShengCheng.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butKuaiSuShengCheng.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.butKuaiSuShengCheng.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.butKuaiSuShengCheng.Location = new System.Drawing.Point(219, 265);
            this.butKuaiSuShengCheng.Name = "butKuaiSuShengCheng";
            this.butKuaiSuShengCheng.Size = new System.Drawing.Size(75, 23);
            this.butKuaiSuShengCheng.TabIndex = 3;
            this.butKuaiSuShengCheng.Text = "快速生成";
            this.butKuaiSuShengCheng.UseVisualStyleBackColor = false;
            this.butKuaiSuShengCheng.Click += new System.EventHandler(this.ButKuaiSuShengCheng_Click);
            // 
            // comKuaiSuKeCheng
            // 
            this.comKuaiSuKeCheng.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.comKuaiSuKeCheng.FormattingEnabled = true;
            this.comKuaiSuKeCheng.Location = new System.Drawing.Point(301, 186);
            this.comKuaiSuKeCheng.Name = "comKuaiSuKeCheng";
            this.comKuaiSuKeCheng.Size = new System.Drawing.Size(114, 25);
            this.comKuaiSuKeCheng.TabIndex = 2;
            // 
            // txtKuaiSuShiJuanName
            // 
            this.txtKuaiSuShiJuanName.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtKuaiSuShiJuanName.Location = new System.Drawing.Point(301, 120);
            this.txtKuaiSuShiJuanName.Name = "txtKuaiSuShiJuanName";
            this.txtKuaiSuShiJuanName.Size = new System.Drawing.Size(114, 23);
            this.txtKuaiSuShiJuanName.TabIndex = 1;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label15.Location = new System.Drawing.Point(231, 189);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(56, 17);
            this.label15.TabIndex = 0;
            this.label15.Text = "所属课程";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label14.Location = new System.Drawing.Point(231, 125);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 17);
            this.label14.TabIndex = 0;
            this.label14.Text = "试卷名称";
            // 
            // ShengChengShiJuan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(236)))), ((int)(((byte)(236)))));
            this.ClientSize = new System.Drawing.Size(737, 503);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ShengChengShiJuan";
            this.Text = "ShengChengShiJuan";
            this.Load += new System.EventHandler(this.ShengChengShiJuan_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabShengChengShiJuan.ResumeLayout(false);
            this.tabShengChengShiJuan.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtShiJuanName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comKeChengName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtDanXuanTiShuLiang;
        private System.Windows.Forms.TextBox txtPanDuanTiShuLiang;
        private System.Windows.Forms.TextBox txtTianKongTiShuLiang;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtDanXuanTiFenShu;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPanDuanTiFenShu;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtTianKongTiFenShu;
        private System.Windows.Forms.Button butQuXiao;
        private System.Windows.Forms.Button butShengChengShiJuan;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtXiaoShi;
        private System.Windows.Forms.TextBox txtFenZhong;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabShengChengShiJuan;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtKuaiSuShiJuanName;
        private System.Windows.Forms.ComboBox comKuaiSuKeCheng;
        private System.Windows.Forms.Button butKuaiSuShengCheng;
        private System.Windows.Forms.Button butDaoRuWord;
        private System.Windows.Forms.Button butKuaiSuDaoRuWord;
    }
}