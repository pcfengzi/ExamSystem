﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Word;
using Microsoft.Office;
using MSWord = Microsoft.Office.Interop.Word;
using System.IO;
using System.Reflection;

namespace KaoShiXiTong
{
    public static class WordCaoZuo
    {
        public static void CreateWord(string ShiJuanName)
        {
            object path;//文件路径
            //string strContent;//文件内容
            MSWord.Application wordApp;//Word应用程序变量
            MSWord.Document wordDoc;//Word文档变量
            path = "D:\\" + ShiJuanName + ".doc";//保存为Word2003文档
                                                // path = "d:\\myWord.doc";//保存为Word2007文档
            wordApp = new MSWord.Application();//初始化
            //if (File.Exists((string)path))
            //{
            //    File.Delete((string)path);
            //}
            //由于使用的是COM 库，因此有许多变量需要用Missing.Value 代替
            Object Nothing = Missing.Value;
            //新建一个word对象
            wordDoc = wordApp.Documents.Add(ref Nothing, ref Nothing, ref Nothing, ref Nothing);
            //WdSaveDocument为Word2003文档的保存格式(文档后缀.doc)\wdFormatDocumentDefault为Word2007的保存格式(文档后缀.docx)
            object format = MSWord.WdSaveFormat.wdFormatDocument;
            //将wordDoc 文档对象的内容保存为DOC 文档,并保存到path指定的路径
            wordDoc.SaveAs(ref path, ref format, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing);
            //关闭wordDoc文档
            wordDoc.Close(ref Nothing, ref Nothing, ref Nothing);
            //关闭wordApp组件对象
            wordApp.Quit(ref Nothing, ref Nothing, ref Nothing);
            //MessageBox.Show("Word文档创建完毕!");
            //Response.Write("<script>alert('" + path + ": Word文档创建完毕!');</script>");
        }
        public static void SetWordStyle(string ShiJuanName)
        {
            object path;//文件路径
            //string strContent;//文件内容
            MSWord.Application wordApp;//Word应用程序变量
            MSWord.Document wordDoc;//Word文档变量
            path = "D:\\"+ShiJuanName+".doc";//保存为Word2003文档
            // path = "d:\\myWord.doc";//保存为Word2007文档
            wordApp = new MSWord.Application();//初始化
            //if (File.Exists((string)path))
            //{
            //    File.Delete((string)path);
            //}
            Object Nothing = Missing.Value;
            wordDoc = wordApp.Documents.Add(ref Nothing, ref Nothing, ref Nothing, ref Nothing);

            //页面设置
            wordDoc.PageSetup.PaperSize = Microsoft.Office.Interop.Word.WdPaperSize.wdPaperA4;//设置纸张样式
            wordDoc.PageSetup.Orientation = Microsoft.Office.Interop.Word.WdOrientation.wdOrientPortrait;//排列方式为垂直方向
            wordDoc.PageSetup.TopMargin = 57.0f;
            wordDoc.PageSetup.BottomMargin = 57.0f;
            wordDoc.PageSetup.LeftMargin = 57.0f;
            wordDoc.PageSetup.RightMargin = 57.0f;
            wordDoc.PageSetup.HeaderDistance = 30.0f;//页眉位置

            //设置页眉
            wordApp.ActiveWindow.View.Type = Microsoft.Office.Interop.Word.WdViewType.wdOutlineView;//视图样式
            wordApp.ActiveWindow.View.SeekView = Microsoft.Office.Interop.Word.WdSeekView.wdSeekPrimaryHeader;//进入页眉设置，其中页眉边距在页面设置中已完成
            wordApp.Selection.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphRight;

            //插入页眉图片(测试结果图片未插入成功)
            //wordApp.Selection.ParagraphFormat.Alignment = MSWord.WdParagraphAlignment.wdAlignParagraphCenter;
            //string headerfile = "D:\\header.jpg";
            //Microsoft.Office.Interop.Word.InlineShape shape1 = wordApp.ActiveWindow.ActivePane.Selection.InlineShapes.AddPicture(headerfile, ref Nothing, ref Nothing, ref Nothing);
            //shape1.Height = 20;
            //shape1.Width = 80;
            wordApp.ActiveWindow.ActivePane.Selection.InsertAfter("  文档页眉");
            //去掉页眉的横线
            wordApp.ActiveWindow.ActivePane.Selection.ParagraphFormat.Borders[Microsoft.Office.Interop.Word.WdBorderType.wdBorderBottom].LineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleNone;
            wordApp.ActiveWindow.ActivePane.Selection.Borders[Microsoft.Office.Interop.Word.WdBorderType.wdBorderBottom].Visible = false;
            wordApp.ActiveWindow.ActivePane.View.SeekView = Microsoft.Office.Interop.Word.WdSeekView.wdSeekMainDocument;//退出页眉设置

            //为当前页添加页码
            Microsoft.Office.Interop.Word.PageNumbers pns = wordApp.Selection.Sections[1].Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterEvenPages].PageNumbers;//获取当前页的号码
            pns.NumberStyle = Microsoft.Office.Interop.Word.WdPageNumberStyle.wdPageNumberStyleNumberInDash;
            pns.HeadingLevelForChapter = 0;
            pns.IncludeChapterNumber = false;
            pns.RestartNumberingAtSection = false;
            pns.StartingNumber = 0;
            object pagenmbetal = Microsoft.Office.Interop.Word.WdPageNumberAlignment.wdAlignPageNumberCenter;//将号码设置在中间
            object first = true;
            wordApp.Selection.Sections[1].Footers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterEvenPages].PageNumbers.Add(ref pagenmbetal, ref first);


            object format = MSWord.WdSaveFormat.wdFormatDocument;
            wordDoc.SaveAs(ref path, ref format, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing);
            wordDoc.Close(ref Nothing, ref Nothing, ref Nothing);
            wordApp.Quit(ref Nothing, ref Nothing, ref Nothing);
            //Response.Write("<script>alert('" + path + ": Word文档格式设置完毕!');</script>");
            //MessageBox.Show("Word文档格式设置完毕!");
        }
        public static void AddWordText()
        {
            object path;//文件路径
            string strContent;//文件内容
            MSWord.Application wordApp;//Word应用程序变量
            MSWord.Document wordDoc;//Word文档变量
            path = "D:\\考试试卷.doc";//保存为Word2003文档
            // path = "d:\\myWord.doc";//保存为Word2007文档
            wordApp = new MSWord.Application();//初始化
            //if (File.Exists((string)path))
            //{
            //    File.Delete((string)path);
            //}
            Object Nothing = Missing.Value;
            wordDoc = wordApp.Documents.Add(ref Nothing, ref Nothing, ref Nothing, ref Nothing);

            wordApp.Selection.ParagraphFormat.LineSpacing = 10f;//设置文档的行间距
            //写入普通文本
            wordApp.Selection.ParagraphFormat.FirstLineIndent = 30;//首行缩进的长度
            strContent = "c#向Word写入文本   普通文本:\n";
            wordDoc.Paragraphs.Last.Range.Text = strContent;

            //将文档的前三个字替换成"asdfasdf"，并将其颜色设为蓝色
            object start = 0;
            object end = 3;
            Microsoft.Office.Interop.Word.Range rang = wordDoc.Range(ref start, ref end);
            rang.Font.Color = Microsoft.Office.Interop.Word.WdColor.wdColorBlack;
            rang.Text = "我是替换文字";
            wordDoc.Range(ref start, ref end);



            //写入黑体文本
            object unite = Microsoft.Office.Interop.Word.WdUnits.wdStory;
            wordApp.Selection.EndKey(ref unite, ref Nothing);
            wordApp.Selection.ParagraphFormat.FirstLineIndent = 10;//取消首行缩进的长度
            strContent = "黑体文本\n ";//在文本中使用'\n'换行
            wordDoc.Paragraphs.Last.Range.Font.Name = "黑体";
            wordDoc.Paragraphs.Last.Range.Text = strContent;
            // wordApp.Selection.Text = strContent;
            //写入加粗文本
            strContent = "加粗文本\n ";
            wordApp.Selection.EndKey(ref unite, ref Nothing);
            wordDoc.Paragraphs.Last.Range.Font.Bold = 1;//Bold=0为不加粗
            wordDoc.Paragraphs.Last.Range.Text = strContent;
            //  wordApp.Selection.Text = strContent;
            //写入15号字体文本
            strContent = "15号字体文本\n ";
            wordApp.Selection.EndKey(ref unite, ref Nothing);

            wordDoc.Paragraphs.Last.Range.Font.Size = 15;
            wordDoc.Paragraphs.Last.Range.Text = strContent;
            //写入斜体文本
            strContent = "斜体文本\n ";
            wordApp.Selection.EndKey(ref unite, ref Nothing);
            wordDoc.Paragraphs.Last.Range.Font.Italic = 1;
            wordDoc.Paragraphs.Last.Range.Text = strContent;
            //写入蓝色文本
            strContent = "蓝色文本\n ";
            wordApp.Selection.EndKey(ref unite, ref Nothing);
            wordDoc.Paragraphs.Last.Range.Font.Color = MSWord.WdColor.wdColorBlue;
            wordDoc.Paragraphs.Last.Range.Text = strContent;
            //写入下划线文本
            strContent = "下划线文本\n ";
            wordApp.Selection.EndKey(ref unite, ref Nothing);
            wordDoc.Paragraphs.Last.Range.Font.Underline = MSWord.WdUnderline.wdUnderlineThick;
            wordDoc.Paragraphs.Last.Range.Text = strContent;

            object format = MSWord.WdSaveFormat.wdFormatDocument;
            wordDoc.SaveAs(ref path, ref format, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing);
            wordDoc.Close(ref Nothing, ref Nothing, ref Nothing);
            wordApp.Quit(ref Nothing, ref Nothing, ref Nothing);
            //Response.Write("<script> alert('" + path + ": Word文档写入文本完毕!');</script>");
            //MessageBox.Show("Word文档格式设置完毕!");
        }
        public static void HeiTi( string strContent,string ShiJuanName)
        {
            object path;//文件路径
            //string strContent;//文件内容
            MSWord.Application wordApp;//Word应用程序变量
            MSWord.Document wordDoc;//Word文档变量
            path = "D:\\"+ ShiJuanName + ".doc";//保存为Word2003文档
            // path = "d:\\myWord.doc";//保存为Word2007文档
            wordApp = new MSWord.Application();//初始化
            //if (File.Exists((string)path))
            //{
            //    File.Delete((string)path);
            //}
            Object Nothing = Missing.Value;
            wordDoc = wordApp.Documents.Add(ref Nothing, ref Nothing, ref Nothing, ref Nothing);

            //写入黑体文本
            object unite = Microsoft.Office.Interop.Word.WdUnits.wdStory;
            wordApp.Selection.EndKey(ref unite, ref Nothing);
            wordApp.Selection.ParagraphFormat.FirstLineIndent = 10;//取消首行缩进的长度
            //strContent = "黑体文本\n ";//在文本中使用'\n'换行
            wordDoc.Paragraphs.Last.Range.Font.Name = "黑体";
            wordDoc.Paragraphs.Last.Range.Text = strContent;


            object format = MSWord.WdSaveFormat.wdFormatDocument;
            wordDoc.SaveAs(ref path, ref format, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing, ref Nothing);
            wordDoc.Close(ref Nothing, ref Nothing, ref Nothing);
            wordApp.Quit(ref Nothing, ref Nothing, ref Nothing);
        }
    }
}
