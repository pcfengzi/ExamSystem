USE [master]
GO
/****** Object:  Database [KaoShiXiTong]    Script Date: 2019/9/23 星期一 下午 9:36 ******/
CREATE DATABASE [KaoShiXiTong]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'renlianshibie', FILENAME = N'D:\sql server 文件\renlianshibie.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'renlianshibie_log', FILENAME = N'D:\sql server 文件\renlianshibie_log.ldf' , SIZE = 73728KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [KaoShiXiTong] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [KaoShiXiTong].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [KaoShiXiTong] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [KaoShiXiTong] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [KaoShiXiTong] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [KaoShiXiTong] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [KaoShiXiTong] SET ARITHABORT OFF 
GO
ALTER DATABASE [KaoShiXiTong] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [KaoShiXiTong] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [KaoShiXiTong] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [KaoShiXiTong] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [KaoShiXiTong] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [KaoShiXiTong] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [KaoShiXiTong] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [KaoShiXiTong] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [KaoShiXiTong] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [KaoShiXiTong] SET  DISABLE_BROKER 
GO
ALTER DATABASE [KaoShiXiTong] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [KaoShiXiTong] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [KaoShiXiTong] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [KaoShiXiTong] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [KaoShiXiTong] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [KaoShiXiTong] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [KaoShiXiTong] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [KaoShiXiTong] SET RECOVERY FULL 
GO
ALTER DATABASE [KaoShiXiTong] SET  MULTI_USER 
GO
ALTER DATABASE [KaoShiXiTong] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [KaoShiXiTong] SET DB_CHAINING OFF 
GO
ALTER DATABASE [KaoShiXiTong] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [KaoShiXiTong] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [KaoShiXiTong] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'KaoShiXiTong', N'ON'
GO
ALTER DATABASE [KaoShiXiTong] SET QUERY_STORE = OFF
GO
USE [KaoShiXiTong]
GO
/****** Object:  Table [dbo].[ChengJi]    Script Date: 2019/9/23 星期一 下午 9:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChengJi](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ShiJuanName] [nvarchar](50) NOT NULL,
	[KeChengName] [nvarchar](50) NOT NULL,
	[XueShengName] [nvarchar](50) NOT NULL,
	[KaiShiShiJian] [nvarchar](50) NOT NULL,
	[JieShuShiJian] [nvarchar](50) NOT NULL,
	[ChengJi] [float] NOT NULL,
	[UserID] [int] NOT NULL,
	[ZongChengJi] [float] NOT NULL,
 CONSTRAINT [PK_ChengJi] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DanXuanTi]    Script Date: 2019/9/23 星期一 下午 9:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DanXuanTi](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TiMu] [text] NOT NULL,
	[XuanXiangYi] [text] NOT NULL,
	[XuanXiangEr] [text] NOT NULL,
	[XuanXiangSan] [text] NOT NULL,
	[XuanXiangSi] [text] NOT NULL,
	[ZhengQueDaAn] [nvarchar](4) NOT NULL,
	[KeChengID] [int] NOT NULL,
	[TiXingID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
 CONSTRAINT [PK_DanXuanTi] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DuoXuanTi]    Script Date: 2019/9/23 星期一 下午 9:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DuoXuanTi](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TiMu] [text] NOT NULL,
	[XuanXiangYi] [text] NOT NULL,
	[XuanXiangEr] [text] NOT NULL,
	[XuanXiangSan] [text] NOT NULL,
	[XuanXiangSi] [text] NOT NULL,
	[ZhengQueDaAn] [nvarchar](4) NOT NULL,
	[KeChengID] [int] NOT NULL,
	[TiXingID] [int] NOT NULL,
 CONSTRAINT [PK_DuoXuanTi] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GuanLiYuan]    Script Date: 2019/9/23 星期一 下午 9:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GuanLiYuan](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[phone] [nvarchar](11) NOT NULL,
	[tuPianLuJing] [nvarchar](max) NOT NULL,
	[UserLeiXin] [nvarchar](50) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KeCheng]    Script Date: 2019/9/23 星期一 下午 9:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KeCheng](
	[KeChengID] [int] IDENTITY(1,1) NOT NULL,
	[KeChengName] [nvarchar](50) NOT NULL,
	[UserID] [int] NULL,
 CONSTRAINT [PK_KeCheng] PRIMARY KEY CLUSTERED 
(
	[KeChengID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_KeCheng] UNIQUE NONCLUSTERED 
(
	[KeChengID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LuJin]    Script Date: 2019/9/23 星期一 下午 9:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LuJin](
	[LuJin] [nvarchar](max) NULL,
	[UserName] [nvarchar](50) NULL,
	[UserPhone] [nvarchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PanDuanTi]    Script Date: 2019/9/23 星期一 下午 9:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PanDuanTi](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TiMu] [text] NOT NULL,
	[ZhengQueDaAn] [nvarchar](50) NOT NULL,
	[KeChengID] [int] NOT NULL,
	[TiXingID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
 CONSTRAINT [PK_PanDuanTi] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RenLianUser]    Script Date: 2019/9/23 星期一 下午 9:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RenLianUser](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Phone] [nvarchar](50) NOT NULL,
	[RenLianLuJing] [nvarchar](max) NOT NULL,
	[UserLeiXin] [nvarchar](50) NULL,
 CONSTRAINT [PK_RenLianUser] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ShiJuanDanXuanTi]    Script Date: 2019/9/23 星期一 下午 9:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShiJuanDanXuanTi](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TiMu] [text] NOT NULL,
	[XuanXiangYi] [text] NOT NULL,
	[XuanXiangEr] [text] NOT NULL,
	[XuanXiangSan] [text] NOT NULL,
	[XuanXiangSi] [text] NOT NULL,
	[ZhengQueDaAn] [nvarchar](2) NOT NULL,
	[YongHuDaAn] [nvarchar](2) NULL,
	[YongHuName] [nvarchar](50) NOT NULL,
	[MeiTiFenShu] [float] NOT NULL,
	[KaoShiShiChang] [nvarchar](50) NOT NULL,
	[KeChengID] [int] NOT NULL,
	[TiXingID] [int] NOT NULL,
	[ShiJuanName] [nvarchar](50) NOT NULL,
	[UserID] [int] NOT NULL,
	[TrueFale] [int] NOT NULL,
 CONSTRAINT [PK_ShiJuanDanXuanTi] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ShiJuanDuoXuanTi]    Script Date: 2019/9/23 星期一 下午 9:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShiJuanDuoXuanTi](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TiMu] [text] NOT NULL,
	[XuanXiangYi] [text] NOT NULL,
	[XuanXiangEr] [text] NOT NULL,
	[XuanXiangSan] [text] NOT NULL,
	[XuanXiangSi] [text] NOT NULL,
	[ZhengQueDaAn] [nvarchar](4) NOT NULL,
	[YongHuDaAn] [nvarchar](4) NULL,
	[YongHuName] [nvarchar](50) NOT NULL,
	[MeiTiFenShu] [float] NOT NULL,
	[KaoShiShiChang] [nvarchar](50) NOT NULL,
	[KeChengID] [int] NOT NULL,
	[TiXingID] [int] NOT NULL,
	[ShiJuanName] [nvarchar](50) NOT NULL,
	[UserID] [int] NULL,
 CONSTRAINT [PK_ShiJuanDuoXuanTi] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ShiJuanName]    Script Date: 2019/9/23 星期一 下午 9:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShiJuanName](
	[ShiJuanID] [int] IDENTITY(1,1) NOT NULL,
	[ShiJuanName] [nvarchar](50) NOT NULL,
	[UserID] [int] NOT NULL,
	[KeChengID] [int] NOT NULL,
 CONSTRAINT [PK_ShiJuanName] PRIMARY KEY CLUSTERED 
(
	[ShiJuanName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ShiJuanPanDuanTi]    Script Date: 2019/9/23 星期一 下午 9:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShiJuanPanDuanTi](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TiMu] [text] NOT NULL,
	[ZhengQueDaAn] [nvarchar](2) NOT NULL,
	[YongHuDaAn] [nvarchar](2) NULL,
	[YongHuName] [nvarchar](50) NOT NULL,
	[MeiTiFenShu] [float] NOT NULL,
	[KaoShiShiChang] [nvarchar](50) NOT NULL,
	[KeChengID] [int] NOT NULL,
	[TiXingID] [int] NOT NULL,
	[ShiJuanName] [nvarchar](50) NOT NULL,
	[UserID] [int] NOT NULL,
	[TrueFale] [int] NOT NULL,
 CONSTRAINT [PK_ShiJuanPanDuanTi] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ShiJuanTianKongTi]    Script Date: 2019/9/23 星期一 下午 9:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShiJuanTianKongTi](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TiMu] [text] NOT NULL,
	[ZhengQueDaAn] [text] NOT NULL,
	[YongHuDaAn] [text] NULL,
	[YongHuName] [nvarchar](50) NOT NULL,
	[MeiTiFenShu] [float] NOT NULL,
	[KaoShiShiChang] [nvarchar](50) NOT NULL,
	[KeChengID] [int] NOT NULL,
	[TiXingID] [int] NOT NULL,
	[ShiJuanName] [nvarchar](50) NOT NULL,
	[UserID] [int] NOT NULL,
	[TrueFale] [int] NOT NULL,
 CONSTRAINT [PK_ShiJuanTianKongTi] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TianKongTi]    Script Date: 2019/9/23 星期一 下午 9:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TianKongTi](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TiMu] [text] NOT NULL,
	[ZhengQueDaAn] [nvarchar](50) NOT NULL,
	[KeChengID] [int] NOT NULL,
	[TiXingID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
 CONSTRAINT [PK_TianKongTi] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TiXing]    Script Date: 2019/9/23 星期一 下午 9:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TiXing](
	[TiXingID] [int] IDENTITY(1,1) NOT NULL,
	[TiXingName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_TiXing] PRIMARY KEY CLUSTERED 
(
	[TiXingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_TiXing] UNIQUE NONCLUSTERED 
(
	[TiXingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ZhangHaoUser]    Script Date: 2019/9/23 星期一 下午 9:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ZhangHaoUser](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Phone] [nvarchar](50) NOT NULL,
	[ZhangHao] [nvarchar](50) NOT NULL,
	[Pwd] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ZhangHaoUser] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ShiJuanDanXuanTi] ADD  CONSTRAINT [DF_ShiJuanDanXuanTi_TrueFale]  DEFAULT ((1)) FOR [TrueFale]
GO
ALTER TABLE [dbo].[ShiJuanPanDuanTi] ADD  CONSTRAINT [DF_ShiJuanPanDuanTi_TrueFale]  DEFAULT ((1)) FOR [TrueFale]
GO
ALTER TABLE [dbo].[ShiJuanTianKongTi] ADD  CONSTRAINT [DF_ShiJuanTianKongTi_TrueFale]  DEFAULT ((1)) FOR [TrueFale]
GO
ALTER TABLE [dbo].[ChengJi]  WITH CHECK ADD  CONSTRAINT [FK_ChengJi_RenLianUser] FOREIGN KEY([UserID])
REFERENCES [dbo].[RenLianUser] ([ID])
GO
ALTER TABLE [dbo].[ChengJi] CHECK CONSTRAINT [FK_ChengJi_RenLianUser]
GO
ALTER TABLE [dbo].[DanXuanTi]  WITH CHECK ADD  CONSTRAINT [FK_DanXuanTi_KeCheng] FOREIGN KEY([KeChengID])
REFERENCES [dbo].[KeCheng] ([KeChengID])
GO
ALTER TABLE [dbo].[DanXuanTi] CHECK CONSTRAINT [FK_DanXuanTi_KeCheng]
GO
ALTER TABLE [dbo].[DanXuanTi]  WITH CHECK ADD  CONSTRAINT [FK_DanXuanTi_RenLianUser] FOREIGN KEY([UserID])
REFERENCES [dbo].[RenLianUser] ([ID])
GO
ALTER TABLE [dbo].[DanXuanTi] CHECK CONSTRAINT [FK_DanXuanTi_RenLianUser]
GO
ALTER TABLE [dbo].[DanXuanTi]  WITH CHECK ADD  CONSTRAINT [FK_DanXuanTi_TiXing] FOREIGN KEY([TiXingID])
REFERENCES [dbo].[TiXing] ([TiXingID])
GO
ALTER TABLE [dbo].[DanXuanTi] CHECK CONSTRAINT [FK_DanXuanTi_TiXing]
GO
ALTER TABLE [dbo].[DuoXuanTi]  WITH CHECK ADD  CONSTRAINT [FK_DuoXuanTi_KeCheng] FOREIGN KEY([KeChengID])
REFERENCES [dbo].[KeCheng] ([KeChengID])
GO
ALTER TABLE [dbo].[DuoXuanTi] CHECK CONSTRAINT [FK_DuoXuanTi_KeCheng]
GO
ALTER TABLE [dbo].[DuoXuanTi]  WITH CHECK ADD  CONSTRAINT [FK_DuoXuanTi_TiXing] FOREIGN KEY([TiXingID])
REFERENCES [dbo].[TiXing] ([TiXingID])
GO
ALTER TABLE [dbo].[DuoXuanTi] CHECK CONSTRAINT [FK_DuoXuanTi_TiXing]
GO
ALTER TABLE [dbo].[KeCheng]  WITH CHECK ADD  CONSTRAINT [FK_KeCheng_RenLianUser] FOREIGN KEY([UserID])
REFERENCES [dbo].[RenLianUser] ([ID])
GO
ALTER TABLE [dbo].[KeCheng] CHECK CONSTRAINT [FK_KeCheng_RenLianUser]
GO
ALTER TABLE [dbo].[PanDuanTi]  WITH CHECK ADD  CONSTRAINT [FK_PanDuanTi_KeCheng] FOREIGN KEY([KeChengID])
REFERENCES [dbo].[KeCheng] ([KeChengID])
GO
ALTER TABLE [dbo].[PanDuanTi] CHECK CONSTRAINT [FK_PanDuanTi_KeCheng]
GO
ALTER TABLE [dbo].[PanDuanTi]  WITH CHECK ADD  CONSTRAINT [FK_PanDuanTi_RenLianUser] FOREIGN KEY([UserID])
REFERENCES [dbo].[RenLianUser] ([ID])
GO
ALTER TABLE [dbo].[PanDuanTi] CHECK CONSTRAINT [FK_PanDuanTi_RenLianUser]
GO
ALTER TABLE [dbo].[PanDuanTi]  WITH CHECK ADD  CONSTRAINT [FK_PanDuanTi_TiXing] FOREIGN KEY([TiXingID])
REFERENCES [dbo].[TiXing] ([TiXingID])
GO
ALTER TABLE [dbo].[PanDuanTi] CHECK CONSTRAINT [FK_PanDuanTi_TiXing]
GO
ALTER TABLE [dbo].[ShiJuanDanXuanTi]  WITH CHECK ADD  CONSTRAINT [FK_ShiJuanDanXuanTi_KeCheng] FOREIGN KEY([KeChengID])
REFERENCES [dbo].[KeCheng] ([KeChengID])
GO
ALTER TABLE [dbo].[ShiJuanDanXuanTi] CHECK CONSTRAINT [FK_ShiJuanDanXuanTi_KeCheng]
GO
ALTER TABLE [dbo].[ShiJuanDanXuanTi]  WITH CHECK ADD  CONSTRAINT [FK_ShiJuanDanXuanTi_RenLianUser] FOREIGN KEY([UserID])
REFERENCES [dbo].[RenLianUser] ([ID])
GO
ALTER TABLE [dbo].[ShiJuanDanXuanTi] CHECK CONSTRAINT [FK_ShiJuanDanXuanTi_RenLianUser]
GO
ALTER TABLE [dbo].[ShiJuanDanXuanTi]  WITH CHECK ADD  CONSTRAINT [FK_ShiJuanDanXuanTi_ShiJuanDanXuanTi] FOREIGN KEY([ID])
REFERENCES [dbo].[ShiJuanDanXuanTi] ([ID])
GO
ALTER TABLE [dbo].[ShiJuanDanXuanTi] CHECK CONSTRAINT [FK_ShiJuanDanXuanTi_ShiJuanDanXuanTi]
GO
ALTER TABLE [dbo].[ShiJuanDanXuanTi]  WITH CHECK ADD  CONSTRAINT [FK_ShiJuanDanXuanTi_ShiJuanName] FOREIGN KEY([ShiJuanName])
REFERENCES [dbo].[ShiJuanName] ([ShiJuanName])
GO
ALTER TABLE [dbo].[ShiJuanDanXuanTi] CHECK CONSTRAINT [FK_ShiJuanDanXuanTi_ShiJuanName]
GO
ALTER TABLE [dbo].[ShiJuanDanXuanTi]  WITH CHECK ADD  CONSTRAINT [FK_ShiJuanDanXuanTi_TiXing] FOREIGN KEY([TiXingID])
REFERENCES [dbo].[TiXing] ([TiXingID])
GO
ALTER TABLE [dbo].[ShiJuanDanXuanTi] CHECK CONSTRAINT [FK_ShiJuanDanXuanTi_TiXing]
GO
ALTER TABLE [dbo].[ShiJuanDuoXuanTi]  WITH CHECK ADD  CONSTRAINT [FK_ShiJuanDuoXuanTi_KeCheng] FOREIGN KEY([KeChengID])
REFERENCES [dbo].[KeCheng] ([KeChengID])
GO
ALTER TABLE [dbo].[ShiJuanDuoXuanTi] CHECK CONSTRAINT [FK_ShiJuanDuoXuanTi_KeCheng]
GO
ALTER TABLE [dbo].[ShiJuanDuoXuanTi]  WITH CHECK ADD  CONSTRAINT [FK_ShiJuanDuoXuanTi_RenLianUser] FOREIGN KEY([UserID])
REFERENCES [dbo].[RenLianUser] ([ID])
GO
ALTER TABLE [dbo].[ShiJuanDuoXuanTi] CHECK CONSTRAINT [FK_ShiJuanDuoXuanTi_RenLianUser]
GO
ALTER TABLE [dbo].[ShiJuanDuoXuanTi]  WITH CHECK ADD  CONSTRAINT [FK_ShiJuanDuoXuanTi_ShiJuanName] FOREIGN KEY([ShiJuanName])
REFERENCES [dbo].[ShiJuanName] ([ShiJuanName])
GO
ALTER TABLE [dbo].[ShiJuanDuoXuanTi] CHECK CONSTRAINT [FK_ShiJuanDuoXuanTi_ShiJuanName]
GO
ALTER TABLE [dbo].[ShiJuanDuoXuanTi]  WITH CHECK ADD  CONSTRAINT [FK_ShiJuanDuoXuanTi_TiXing] FOREIGN KEY([TiXingID])
REFERENCES [dbo].[TiXing] ([TiXingID])
GO
ALTER TABLE [dbo].[ShiJuanDuoXuanTi] CHECK CONSTRAINT [FK_ShiJuanDuoXuanTi_TiXing]
GO
ALTER TABLE [dbo].[ShiJuanName]  WITH CHECK ADD  CONSTRAINT [FK_ShiJuanName_KeCheng] FOREIGN KEY([KeChengID])
REFERENCES [dbo].[KeCheng] ([KeChengID])
GO
ALTER TABLE [dbo].[ShiJuanName] CHECK CONSTRAINT [FK_ShiJuanName_KeCheng]
GO
ALTER TABLE [dbo].[ShiJuanName]  WITH CHECK ADD  CONSTRAINT [FK_ShiJuanName_RenLianUser] FOREIGN KEY([UserID])
REFERENCES [dbo].[RenLianUser] ([ID])
GO
ALTER TABLE [dbo].[ShiJuanName] CHECK CONSTRAINT [FK_ShiJuanName_RenLianUser]
GO
ALTER TABLE [dbo].[ShiJuanPanDuanTi]  WITH CHECK ADD  CONSTRAINT [FK_ShiJuanPanDuanTi_KeCheng] FOREIGN KEY([KeChengID])
REFERENCES [dbo].[KeCheng] ([KeChengID])
GO
ALTER TABLE [dbo].[ShiJuanPanDuanTi] CHECK CONSTRAINT [FK_ShiJuanPanDuanTi_KeCheng]
GO
ALTER TABLE [dbo].[ShiJuanPanDuanTi]  WITH CHECK ADD  CONSTRAINT [FK_ShiJuanPanDuanTi_RenLianUser] FOREIGN KEY([UserID])
REFERENCES [dbo].[RenLianUser] ([ID])
GO
ALTER TABLE [dbo].[ShiJuanPanDuanTi] CHECK CONSTRAINT [FK_ShiJuanPanDuanTi_RenLianUser]
GO
ALTER TABLE [dbo].[ShiJuanPanDuanTi]  WITH CHECK ADD  CONSTRAINT [FK_ShiJuanPanDuanTi_ShiJuanName] FOREIGN KEY([ShiJuanName])
REFERENCES [dbo].[ShiJuanName] ([ShiJuanName])
GO
ALTER TABLE [dbo].[ShiJuanPanDuanTi] CHECK CONSTRAINT [FK_ShiJuanPanDuanTi_ShiJuanName]
GO
ALTER TABLE [dbo].[ShiJuanPanDuanTi]  WITH CHECK ADD  CONSTRAINT [FK_ShiJuanPanDuanTi_TiXing] FOREIGN KEY([TiXingID])
REFERENCES [dbo].[TiXing] ([TiXingID])
GO
ALTER TABLE [dbo].[ShiJuanPanDuanTi] CHECK CONSTRAINT [FK_ShiJuanPanDuanTi_TiXing]
GO
ALTER TABLE [dbo].[ShiJuanTianKongTi]  WITH CHECK ADD  CONSTRAINT [FK_ShiJuanTianKongTi_KeCheng] FOREIGN KEY([KeChengID])
REFERENCES [dbo].[KeCheng] ([KeChengID])
GO
ALTER TABLE [dbo].[ShiJuanTianKongTi] CHECK CONSTRAINT [FK_ShiJuanTianKongTi_KeCheng]
GO
ALTER TABLE [dbo].[ShiJuanTianKongTi]  WITH CHECK ADD  CONSTRAINT [FK_ShiJuanTianKongTi_RenLianUser] FOREIGN KEY([UserID])
REFERENCES [dbo].[RenLianUser] ([ID])
GO
ALTER TABLE [dbo].[ShiJuanTianKongTi] CHECK CONSTRAINT [FK_ShiJuanTianKongTi_RenLianUser]
GO
ALTER TABLE [dbo].[ShiJuanTianKongTi]  WITH CHECK ADD  CONSTRAINT [FK_ShiJuanTianKongTi_ShiJuanName] FOREIGN KEY([ShiJuanName])
REFERENCES [dbo].[ShiJuanName] ([ShiJuanName])
GO
ALTER TABLE [dbo].[ShiJuanTianKongTi] CHECK CONSTRAINT [FK_ShiJuanTianKongTi_ShiJuanName]
GO
ALTER TABLE [dbo].[ShiJuanTianKongTi]  WITH CHECK ADD  CONSTRAINT [FK_ShiJuanTianKongTi_TiXing] FOREIGN KEY([TiXingID])
REFERENCES [dbo].[TiXing] ([TiXingID])
GO
ALTER TABLE [dbo].[ShiJuanTianKongTi] CHECK CONSTRAINT [FK_ShiJuanTianKongTi_TiXing]
GO
ALTER TABLE [dbo].[TianKongTi]  WITH CHECK ADD  CONSTRAINT [FK_TianKongTi_KeCheng] FOREIGN KEY([KeChengID])
REFERENCES [dbo].[KeCheng] ([KeChengID])
GO
ALTER TABLE [dbo].[TianKongTi] CHECK CONSTRAINT [FK_TianKongTi_KeCheng]
GO
ALTER TABLE [dbo].[TianKongTi]  WITH CHECK ADD  CONSTRAINT [FK_TianKongTi_RenLianUser] FOREIGN KEY([UserID])
REFERENCES [dbo].[RenLianUser] ([ID])
GO
ALTER TABLE [dbo].[TianKongTi] CHECK CONSTRAINT [FK_TianKongTi_RenLianUser]
GO
ALTER TABLE [dbo].[TianKongTi]  WITH CHECK ADD  CONSTRAINT [FK_TianKongTi_TiXing] FOREIGN KEY([TiXingID])
REFERENCES [dbo].[TiXing] ([TiXingID])
GO
ALTER TABLE [dbo].[TianKongTi] CHECK CONSTRAINT [FK_TianKongTi_TiXing]
GO
/****** Object:  StoredProcedure [dbo].[P_Test]    Script Date: 2019/9/23 星期一 下午 9:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[P_Test]--创建存储过程P_Test

@pageSize int,--每页数据条数

@pageIndex int,--当前页数（页码）

@pageCount int output--总的页数，因为需要显示页数，因此是个输出参数

as

declare @datacount int--总数据条数

select @datacount=count(*) from DanXuanTi--获得总数据条数值并赋给参数

set @pageCount=ceiling(1.0*@datacount/@pageSize)--获得总页数，并赋给参数

--接下来是获得指定页数据

select * from

(select *,row_number() over(order by id) as num from DanXuanTi) as temp

where num between @pageSize*(@pageIndex-1)+1 and @pageSize*@pageIndex
GO
/****** Object:  StoredProcedure [dbo].[UP_DanXuanTi]    Script Date: 2019/9/23 星期一 下午 9:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[UP_DanXuanTi]--创建存储过程UP_DanXuanTi

@pageSize int,--每页数据条数
@pageIndex int,--当前页数（页码）
@TiXingID int,--题型ID
@KeChengID int,--课程ID
@UserID int,--用户ID

@pageCount int output--总的页数，因为需要显示页数，因此是个输出参数

as

declare @datacount int--总数据条数

select @datacount=count(*) from PanDuanTi where TiXingID=@TiXingID and KeChengID=@KeChengID and UserID=@UserID --获得总数据条数值并赋给参数

set @pageCount=ceiling(1.0*@datacount/@pageSize)--获得总页数，并赋给参数

--接下来是获得指定页数据

select * from

(select *,row_number() over(order by id) as num from PanDuanTi  where TiXingID=@TiXingID and KeChengID=@KeChengID and UserID=@UserID) as temp

where num between @pageSize*(@pageIndex-1)+1 and @pageSize*@pageIndex
GO
/****** Object:  StoredProcedure [dbo].[UP_TianKongTi]    Script Date: 2019/9/23 星期一 下午 9:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[UP_TianKongTi]--创建存储过程UP_DanXuanTi

@pageSize int,--每页数据条数
@pageIndex int,--当前页数（页码）
@TiXingID int,--题型ID
@KeChengID int,--课程ID
@UserID int,--用户ID

@pageCount int output--总的页数，因为需要显示页数，因此是个输出参数

as

declare @datacount int--总数据条数

select @datacount=count(*) from TianKongTi where TiXingID=@TiXingID and KeChengID=@KeChengID and UserID=@UserID --获得总数据条数值并赋给参数

set @pageCount=ceiling(1.0*@datacount/@pageSize)--获得总页数，并赋给参数

--接下来是获得指定页数据

select * from

(select *,row_number() over(order by id) as num from TianKongTi  where TiXingID=@TiXingID and KeChengID=@KeChengID and UserID=@UserID) as temp

where num between @pageSize*(@pageIndex-1)+1 and @pageSize*@pageIndex
GO
/****** Object:  StoredProcedure [dbo].[UP_XuanZeTi]    Script Date: 2019/9/23 星期一 下午 9:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[UP_XuanZeTi]--创建存储过程UP_DanXuanTi

@pageSize int,--每页数据条数
@pageIndex int,--当前页数（页码）
@TiXingID int,--题型ID
@KeChengID int,--课程ID
@UserID int,--用户ID

@pageCount int output--总的页数，因为需要显示页数，因此是个输出参数

as

declare @datacount int--总数据条数

select @datacount=count(*) from DanXuanTi where TiXingID=@TiXingID and KeChengID=@KeChengID and UserID=@UserID --获得总数据条数值并赋给参数

set @pageCount=ceiling(1.0*@datacount/@pageSize)--获得总页数，并赋给参数

--接下来是获得指定页数据

select * from

(select *,row_number() over(order by id) as num from DanXuanTi  where TiXingID=@TiXingID and KeChengID=@KeChengID and UserID=@UserID) as temp

where num between @pageSize*(@pageIndex-1)+1 and @pageSize*@pageIndex
GO
USE [master]
GO
ALTER DATABASE [KaoShiXiTong] SET  READ_WRITE 
GO
