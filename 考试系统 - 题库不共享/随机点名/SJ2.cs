﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 随机点名
{
    public partial class SJ2 : Form
    {
        public SJ2()
        {
            InitializeComponent();
        }
        int a = 0;
        string[] cishu = new string[39];
        int[] XH;
        string[] XB;
        public void DaoJiShi()
        {
            a++;
            label41.Text = "第" + a.ToString() + "位同学被点到";

            XH = XueHao();
            XB = XingBie();
            xm = name();
            xm_2 = TextShuzu();
            //循环给点到过的label控件变为白色
            for (int i = 0; i < 39; i++)
            {
                xm[i].BackColor = Color.Transparent;
                xm[i].ForeColor = Color.FromArgb(44, 76, 133);
            }

            //防止姓名被重复点到
            for (int i = 0; i < xm.Length; i++)
            {
                if (cishu[count] != xm_2[count])//判断存储当前随机到姓名的数组里面是否有当前随机到的姓名
                {
                    cishu[count] = xm_2[count];//如果没有就把当前随机到的姓名赋给存储姓名的数组中，方便下次判断
                    xm[count].BackColor = Color.Transparent;//如果没有重复的就把当前的控件变个颜色
                    xm[count].ForeColor = Color.Red;
                    label40.Text = xm_2[count];//把点到的label控件相应的文本赋给label40控件
                    label42.Text = XH[count].ToString() + "号";
                    label43.Text = XB[count];
                    return;//跳出当前方法
                }
                else if (i == xm.Length - 1)
                {
                    xm[count].BackColor = Color.Black;
                }
                else
                {
                    count = suijishu.Next(0, 39);//如果有存在重复的姓名，就重新new一个随机数，回去重新判断
                    continue;

                }
            }
        }
        private void Button1_Click(object sender, EventArgs e)
        {
            if (timer1.Enabled == true)
            {
                //Bool.GuanBiJiShi = true;
                timer1.Enabled = false;
                button1.Text = "开始";
                button1.BackColor = Color.Transparent;
                button1.ForeColor = Color.FromArgb(44, 76, 133);
                DaoJiShi();

                if (a==39)
                {
                    cishu = new string[39];
                    a = 0;
                }
            }
            else
            {
                //Bool.GuanBiJiShi = false;

                timer1.Enabled = true;
                button1.Text = "暂停";
                label40.Location = new Point(310, 370);
                button1.BackColor = Color.BurlyWood;
                button1.ForeColor = Color.White;

                if (a == 39)
                {
                    cishu = new string[39];
                    a = 0;
                }
            }
        }

        /// <summary>
        /// 存储所有label控件，随机给某个label控件设置颜色
        /// </summary>
        /// <returns></returns>
        public Label[] name()
        {
            Label[] nameSZ = { label1, label2,label3,
                label4, label5, label6, label7,
                label8, label9, label10, label11,
                label12, label13, label14, label15,
                label16, label17, label18, label19,
                label20,label21, label22, label23,
                label24, label25,label26, label27,
                label28, label29, label30, label31,
                label32, label33, label34,label35,
                label36, label37, label38, label39 };
            return nameSZ;
        }

        public string[] TextShuzu()
        {
            string[] nameSZ = { "向汝星", "彭程","戴皓宇",
                "龙晓波", "张德华", "曾琦峰", "黄卓群",
                "王攀峰", "曾浩", "皱亚雄", "姚超",
                "邓星宇", "何佳炜", "曾煜城", "方小龙",
                "肖海荣", "阳润泽", "左成", "陈军林",
                "朱逸风", "王滨", "陈波", "张志宏",
                "蒋春玲", "张井", "周敏龙", "向晓明",
                "刘玲", "王耀", "龙洋", "王澳彬",
                "董旭阳", "孙敏", "陈宇", "谢艳",
                "卢思玉", "魏俞东", "吴冬庭", "刘杰" };
            return nameSZ;

        }
        /// <summary>
        /// 存储每个对应label控件的同学的学号
        /// </summary>
        /// <returns></returns>
        public int[] XueHao()
        {
            int[] XHhao = { 1,2,3,4,5,6,7,8,9,10,11,12,13,
            14,15,16,17,18,19,22,23,24,25,26,28,29,30,31,32,33,34,
            35,36,37,38,39,20,21,40};
            return XHhao;
        }

        /// <summary>
        /// 存储每个对应label控件的同学的性别
        /// </summary>
        /// <returns></returns>
        public string[] XingBie()
        {
            string[] xingbie = { "男","男","男","男","男","男","男",
                "男","男","男","男","男","男",
            "男","男","男","男","男","男","男",
                "男","男","男","女","男","男","男",
                "女","男","男","男",
            "男","女","男","女","女","男","男","男"};
            return xingbie;
        }

        Random suijishu = new Random();
        int count = 0;
        Label[] xm;
        string[] xm_2;
        /// <summary>
        /// 时间控件，每0.2秒new一个随机数给label数组中的下标
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer1_Tick(object sender, EventArgs e)
        {
           
            xm = name();
            xm_2 = TextShuzu();
            xm[count].ForeColor = Color.FromArgb(44, 76, 133);
            xm[count].BackColor = Color.Transparent;//循环回来之后把上次的颜色变回来
            count = suijishu.Next(0, 39);
            xm[count].BackColor = Color.Transparent;//把点到的姓名变成红色
            xm[count].ForeColor = Color.Red;
            label40.Text = xm_2[count];//把点到的label控件相应的文本赋给label40控件
            #region
            //try
            //{
            //    pictureBox1.Image = Image.FromFile(tp[count]);//0.2秒换一张图片
            //}
            //catch
            //{
            //}
            #endregion
            //if (Bool.GuanBiJiShi == true)
            //{
            //    timer2.Enabled = false;
            //    timer1.Enabled = false;
            //    Bool.GuanBiJiShi = false;
            //}
        }
        //string[] tp = Directory.GetFiles(@"D:\\ps素材");//存储所有文件夹的图片路径
        //string[] tp = Application.StartupPath + @"\\ps素材";//存储所有文件夹的图片路径
        private void SJ2_Load(object sender, EventArgs e)
        {
            toolStripTextBox2.Text = DJiShi.ToString();
            //pictureBox1.Image = Image.FromFile(@"D:\\ps素材\3.jpg");//界面初始化的时候把某一张图片给 pictureBox1控件
            button1.BackColor = Color.White;
            button1.ForeColor = Color.Red;

            xm = name();
            for (int i = 0; i < xm.Length; i++)//窗体初始化的时候把全部label控件都变为透明
            {
                xm[i].BackColor = Color.Transparent;
            }
            label41.BackColor = Color.Transparent;
            label40.BackColor = Color.Transparent;
            label42.BackColor = Color.Transparent;
            label43.BackColor = Color.Transparent;

        }
        int DJiShi;
        private void 开始倒计时ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                DJiShi = Convert.ToInt32(toolStripTextBox1.Text);
            }
            catch
            {
                MessageBox.Show("请输入倒计时 时长/秒");
            }
            if (DJiShi > 0)
            {
                timer2.Enabled = true;
                timer1.Enabled = true;
                button1.Visible = false;
                label40.Location = new Point(310, 370);
            }
        }

        private void Timer2_Tick(object sender, EventArgs e)
        {
            DJiShi--;
            toolStripTextBox2.Text = DJiShi.ToString();
            if (DJiShi == 0)
            {
                timer1.Enabled = false;
                timer2.Enabled = false;
                DaoJiShi();
                button1.Visible = true;
            }

            //if (Bool.GuanBiJiShi == true)
            //{
            //    timer2.Enabled = false;
            //    timer1.Enabled = false;
            //    Bool.GuanBiJiShi = false;
            //}
        }

        private void 停止倒计时ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            timer2.Enabled = false;
            DaoJiShi();
            button1.Visible = true;
        }

        private void 重置系统ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cishu = new string[39];
        }
    }
}
